/**
 * 此类是地图API的核心类，用来实例化一个地图。
 *
 * <p>
 * 在指定的容器内创建地图实例，之后需要调用Map.centerAndZoom()方法对地图进行初始化。未进行初始化的地图将不能进行任何操作。
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.Map
 * @param {BMap.String|HTMLElement}
 *            container
 * @param {BMap.MapOptions}
 *            [opts]
 *
 */
BMap.Map = function(container, opts) {
    /**
     * @event click 左键单击地图时触发此事件。 当双击时，产生的事件序列为： click click dblclick (自 1.1 更新)
     * @param {type}
     * @param {target}
     * @param {point}
     * @param {pixel}
     * @param {overlay}
     * @version 1.1
     */
    /**
     * @event dblclick 鼠标双击地图时会触发此事件。
     * @param {type}
     * @param {target}
     * @param {pixel}
     * @param {point}
     *
     */
    /**
     * @event rightclick 右键单击地图时触发此事件。 当双击时，产生的事件序列为： rightclick rightclick
     *        rightdblclick (自 1.1 更新)
     * @param {type}
     * @param {target}
     * @param {point}
     * @param {pixel}
     * @param {overlay}
     * @version 1.1
     */
    /**
     * @event rightdblclick 右键双击地图时触发此事件。 (自 1.1 新增)
     * @param {type}
     * @param {target}
     * @param {point}
     * @param {pixel}
     * @param {overlay}
     * @since 1.1
     */
    /**
     * @event maptypechange 地图类型发生变化时触发此事件。 (自 1.1 新增)
     * @param {type}
     * @param {target}
     * @since 1.1
     */
    /**
     * @event mousemove 鼠标在地图区域移动过程中触发此事件。 (自 1.1 新增)
     * @param {type}
     * @param {target}
     * @param {point}
     * @param {pixel}
     * @param {overlay}
     * @since 1.1
     */
    /**
     * @event mouseover 鼠标移入地图区域时触发此事件。 (自 1.2 新增)
     * @param {type}
     * @param {target}
     * @since 1.2
     */
    /**
     * @event mouseout 鼠标移出地图区域时触发此事件。 (自 1.2 新增)
     * @param {type}
     * @param {target}
     * @since 1.2
     */
    /**
     * @event movestart 地图移动开始时触发此事件。
     * @param {type}
     * @param {target}
     *
     */
    /**
     * @event moving 地图移动过程中触发此事件。
     * @param {type}
     * @param {target}
     *
     */
    /**
     * @event moveend 地图移动结束时触发此事件。
     * @param {type}
     * @param {target}
     *
     */
    /**
     * @event zoomstart 地图更改缩放级别开始时触发触发此事件。
     * @param {type}
     * @param {target}
     *
     */
    /**
     * @event zoomend 地图更改缩放级别结束时触发触发此事件。
     * @param {type}
     * @param {target}
     *
     */
    /**
     * @event addoverlay 当使用Map.addOverlay()方法向地图中添加单个覆盖物时会触发此事件。
     * @param {type}
     * @param {target}
     *
     */
    /**
     * @event addcontrol 当使用Map.addControl()方法向地图中添加单个控件时会触发此事件。
     * @param {type}
     * @param {target}
     *
     */
    /**
     * @event removecontrol 当使用Map.removeControl()方法移除单个控件时会触发此事件。
     * @param {type}
     * @param {target}
     *
     */
    /**
     * @event removeoverlay 当使用Map.removeOverlay()方法移除单个覆盖物时会触发此事件。
     * @param {type}
     * @param {target}
     *
     */
    /**
     * @event clearoverlays 当使用Map.clearOverlays()方法一次性移除全部覆盖物时会触发此事件。
     * @param {type}
     * @param {target}
     *
     */
    /**
     * @event dragstart 开始拖拽地图时触发。
     * @param {type}
     * @param {target}
     * @param {pixel}
     * @param {point}
     *
     */
    /**
     * @event dragging 拖拽地图过程中触发。
     * @param {type}
     * @param {target}
     * @param {pixel}
     * @param {point}
     *
     */
    /**
     * @event dragend 停止拖拽地图时触发。
     * @param {type}
     * @param {target}
     * @param {pixel}
     * @param {point}
     *
     */
    /**
     * @event addtilelayer 添加一个自定义地图图层时触发此事件。
     * @param {type}
     * @param {target}
     *
     */
    /**
     * @event removetilelayer 移除一个自定义地图图层时触发此事件。
     * @param {type}
     * @param {target}
     *
     */
    /**
     * @event load 调用Map.centerAndZoom()方法时会触发此事件。这表示位置、缩放层级已经确定，但可能还在载入地图图块。
     * @param {type}
     * @param {target}
     * @param {pixel}
     * @param {point}
     * @param {zoom}
     *
     */
    /**
     * @event resize 地图可视区域大小发生变化时会触发此事件。
     * @param {type}
     * @param {target}
     * @param {size}
     *
     */
    /**
     * @event hotspotclick 点击热区时触发此事件。(自 1.2 新增)
     * @param {type}
     * @param {target}
     * @param {hotspots}
     * @since 1.2
     */
    /**
     * @event hotspotover 鼠标移至热区时触发此事件。(自 1.2 新增)
     * @param {type}
     * @param {target}
     * @param {hotspots}
     * @since 1.2
     */
    /**
     * @event hotspotout 鼠标移出热区时触发此事件。(自 1.2 新增)
     * @param {type}
     * @param {target}
     * @param {hotspots}
     * @since 1.2
     */
    /**
     * @event tilesloaded 当地图所有图块完成加载时触发此事件。(自 1.2 新增)
     * @param {type}
     * @param {target}
     * @since 1.2
     */
    /**
     * @event touchstart 触摸开始时触发此事件，仅适用移动设备。(自 1.5新增)
     * @param {type}
     * @param {target}
     * @param {point}
     * @param {pixel}
     * @since 1.5
     */
    /**
     * @event touchmove 触摸移动时触发此事件，仅适用移动设备。(自 1.5新增)
     * @param {type}
     * @param {target}
     * @param {point}
     * @param {pixel}
     * @since 1.5
     */
    /**
     * @event touchend 触摸结束时触发此事件，仅适用移动设备。(自 1.5新增)
     * @param {type}
     * @param {target}
     * @param {point}
     * @param {pixel}
     * @since 1.5
     */
    /**
     * @event longpress 长按事件，仅适用移动设备。(自 1.5新增)
     * @param {type}
     * @param {target}
     * @param {point}
     * @param {pixel}
     * @since 1.5
     */
};
/*
 * ================================================================================
 *
 * 配置方法
 *
 * ================================================================================
 */
/**
 * 启用地图拖拽，默认启用。
 *
 *
 *
 */
BMap.Map.prototype.enableDragging = function() {
};
/**
 * 禁用地图拖拽。
 *
 *
 *
 */
BMap.Map.prototype.disableDragging = function() {
};
/**
 * 启用滚轮放大缩小，默认禁用。
 *
 *
 *
 */
BMap.Map.prototype.enableScrollWheelZoom = function() {
};
/**
 * 禁用滚轮放大缩小。
 *
 *
 *
 */
BMap.Map.prototype.disableScrollWheelZoom = function() {
};
/**
 * 启用双击放大，默认启用。
 *
 *
 *
 */
BMap.Map.prototype.enableDoubleClickZoom = function() {
};
/**
 * 禁用双击放大。
 *
 *
 *
 */
BMap.Map.prototype.disableDoubleClickZoom = function() {
};
/**
 * 启用键盘操作，默认禁用。键盘的上、下、左、右键可连续移动地图。同时按下其中两个键可使地图进行对角移动。PgUp、PgDn、Home和End键会使地图平移其1/2的大小。+、-键会使地图放大或缩小一级。
 *
 *
 *
 */
BMap.Map.prototype.enableKeyboard = function() {
};
/**
 * 禁用键盘操作。
 *
 *
 *
 */
BMap.Map.prototype.disableKeyboard = function() {
};
/**
 * 启用地图惯性拖拽，默认禁用。 (自 1.1 新增)
 *
 *
 * @since 1.1
 */
BMap.Map.prototype.enableInertialDragging = function() {
};
/**
 * 禁用地图惯性拖拽。 (自 1.1 新增)
 *
 *
 * @since 1.1
 */
BMap.Map.prototype.disableInertialDragging = function() {
};
/**
 * 启用连续缩放效果，默认禁用。 (自 1.1 新增)
 *
 *
 * @since 1.1
 */
BMap.Map.prototype.enableContinuousZoom = function() {
};
/**
 * 禁用连续缩放效果。 (自 1.1 新增)
 *
 *
 * @since 1.1
 */
BMap.Map.prototype.disableContinuousZoom = function() {
};
/**
 * 启用双指操作缩放，默认启用。 (自 1.1 新增)
 *
 *
 * @since 1.1
 */
BMap.Map.prototype.enablePinchToZoom = function() {
};
/**
 * 禁用双指操作缩放。 (自 1.1 新增)
 *
 *
 * @since 1.1
 */
BMap.Map.prototype.disablePinchToZoom = function() {
};
/**
 * 启用自动适应容器尺寸变化，默认启用。 (自 1.2 新增)
 *
 *
 * @since 1.2
 */
BMap.Map.prototype.enableAutoResize = function() {
};
/**
 * 禁用自动适应容器尺寸变化。 (自 1.2 新增)
 *
 *
 * @since 1.2
 */
BMap.Map.prototype.disableAutoResize = function() {
};
/**
 * 设置地图默认的鼠标指针样式。参数cursor应符合CSS的cursor属性规范。 (自 1.1 新增)
 *
 *
 * @param {BMap.String}
 *            cursor
 * @since 1.1
 */
BMap.Map.prototype.setDefaultCursor = function(cursor) {
};
/**
 * 返回地图默认的鼠标指针样式。 (自 1.1 新增)
 *
 *
 * @return {BMap.String}
 * @since 1.1
 */
BMap.Map.prototype.getDefaultCursor = function() {
};
/**
 * 设置拖拽地图时的鼠标指针样式。参数cursor应符合CSS的cursor属性规范。 (自 1.1 新增)
 *
 *
 * @param {BMap.String}
 *            cursor
 * @since 1.1
 */
BMap.Map.prototype.setDraggingCursor = function(cursor) {
};
/**
 * 返回拖拽地图时的鼠标指针样式。 (自 1.1 新增)
 *
 *
 * @return {BMap.String}
 * @since 1.1
 */
BMap.Map.prototype.getDraggingCursor = function() {
};
/**
 * 设置地图允许的最小级别。取值不得小于地图类型所允许的最小级别。 (自 1.2 新增)
 *
 *
 * @param {Number}
 *            zoom
 * @since 1.2
 */
BMap.Map.prototype.setMinZoom = function(zoom) {
};
/**
 * 设置地图允许的最大级别。取值不得大于地图类型所允许的最大级别。 (自 1.2 新增)
 *
 *
 * @param {Number}
 *            zoom
 * @since 1.2
 */
BMap.Map.prototype.setMaxZoom = function(zoom) {
};
/*
 * ================================================================================
 *
 * 地图状态方法
 *
 * ================================================================================
 */
/**
 * 返回地图可视区域，以地理坐标表示。
 *
 *
 * @return {BMap.Bounds}
 *
 */
BMap.Map.prototype.getBounds = function() {
};
/**
 * 返回地图当前中心点。
 *
 *
 * @return {BMap.Point}
 *
 */
BMap.Map.prototype.getCenter = function() {
};
/**
 * 返回两点之间的距离，单位是米。(自 1.1 新增)
 *
 *
 * @param {BMap.Point}
 *            start
 * @param {BMap.Point}
 *            end
 * @return {Number}
 * @since 1.1
 */
BMap.Map.prototype.getDistance = function(start, end) {
};
/**
 * 返回地图类型。(自 1.1 新增)
 *
 *
 * @return {BMap.MapType}
 * @since 1.1
 */
BMap.Map.prototype.getMapType = function() {
};
/**
 * 返回地图视图的大小，以像素表示。
 *
 *
 * @return {BMap.Size}
 *
 */
BMap.Map.prototype.getSize = function() {
};
/**
 * 根据提供的地理区域或坐标获得最佳的地图视野，返回的对象中包含center和zoom属性，分别表示地图的中心点和级别。此方法仅返回视野信息，不会将新的中心点和级别做用到当前地图上。(自
 * 1.1 新增)
 *
 *
 * @param {BMap.Point[]}
 *            view
 * @param {BMap.ViewportOptions}
 *            [viewportOptions]
 * @return {BMap.Viewport}
 * @since 1.1
 */
BMap.Map.prototype.getViewport = function(view, viewportOptions) {
};
/**
 * 返回地图当前缩放级别。
 *
 *
 * @return {Number}
 *
 */
BMap.Map.prototype.getZoom = function() {
};
/*
 * ================================================================================
 *
 * 修改地图状态方法
 *
 * ================================================================================
 */
/**
 * 设初始化地图。
 * 如果center类型为Point时，zoom必须赋值，范围3-19级，若调用高清底图（针对移动端开发）时，zoom可赋值范围为3-18级。如果center类型为字符串时，比如“北京”，zoom可以忽略，地图将自动根据center适配最佳zoom级别。
 *
 *
 * @param {BMap.Point}
 *            center
 * @param {Number}
 *            zoom
 *
 */
BMap.Map.prototype.centerAndZoom = function(center, zoom) {
};
/**
 * 将地图的中心点更改为给定的点。如果该点在当前的地图视图中已经可见，则会以平滑动画的方式移动到中心点位置。可以通过配置强制移动过程不使用动画效果。
 *
 *
 * @param {BMap.Point}
 *            center
 * @param {BMap.PanOptions}
 *            [opts]
 *
 */
BMap.Map.prototype.panTo = function(center, opts) {
};
/**
 * 将地图在水平位置上移动x像素，垂直位置上移动y像素。如果指定的像素大于可视区域范围或者在配置中指定没有动画效果，则不执行滑动效果。
 *
 *
 * @param {Number}
 *            x
 * @param {Number}
 *            y
 * @param {BMap.PanOptions}
 *            [opts]
 *
 */
BMap.Map.prototype.panBy = function(x, y, opts) {
};
/**
 * 重新设置地图，恢复地图初始化时的中心点和级别。
 *
 *
 *
 */
BMap.Map.prototype.reset = function() {
};
/**
 * 设置地图中心点。center除了可以为坐标点以外，还支持城市名。注：使用城市名进行设置时该方法是异步执行，使用坐标点设置时该方法不是异步执行。
 *
 *
 * @param {BMap.Point|String}
 *            center
 *
 */
BMap.Map.prototype.setCenter = function(center) {
};
/**
 * 设置地图城市，注意当地图初始化时的类型设置为BMAP_PERSPECTIVE_MAP时，需要在调用centerAndZoom之前调用此方法设置地图所在城市。例如：
 * var map = new BMap.Map(“container”, {mapType: BMAP_PERSPECTIVE_MAP});
 * map.setCurrentCity(“北京市”); map.centerAndZoom(new BMap.Point(116.404, 39.915),
 * 18);
 * 注意：初始化的坐标应与您设置的城市对应，否则地图将无法正常显示。如果地图初始化为BMAP_NORMAL_MAP类型，则在调用setMapType切换地图类型时也要调用此方法。
 * (自 1.1 新增)
 *
 *
 * @param {BMap.String}
 *            city
 * @since 1.1
 */
BMap.Map.prototype.setCurrentCity = function(city) {
};
/**
 * 设置地图类型。注意，当设置地图类型为BMAP_PERSPECTIVE_MAP时，需要调用map.setCurrentCity方法设置城市。 (自 1.1
 * 新增)
 *
 *
 * @param {BMap.MapTypes}
 *            mapType
 * @since 1.1
 */
BMap.Map.prototype.setMapType = function(mapType) {
};
/**
 * 根据提供的坐标点数组、地理区域或视野对象设置地图视野，调整后的视野会保证包含提供的坐标点或地理区域。 (自 1.1 新增)
 *
 *
 * @param {BMap.Point[]|Bounds|Viewport}
 *            view
 * @param {BMap.ViewportOptions}
 *            [viewportOptions]
 * @since 1.1
 */
BMap.Map.prototype.setViewport = function(view, viewportOptions) {
};
/**
 * 将视图切换到指定的缩放等级，中心点坐标不变。注意：当有信息窗口在地图上打开时，地图缩放将保证信息窗口所在的坐标位置不动。 (自 1.2 废弃)
 *
 *
 * @param {Number}
 *            zoom
 * @deprecated 1.2
 */
BMap.Map.prototype.zoomTo = function(zoom) {
};
/**
 * 将视图切换到指定的缩放等级，中心点坐标不变。注意：当有信息窗口在地图上打开时，地图缩放将保证信息窗口所在的坐标位置不动。 (自 1.2 新增)
 *
 *
 * @param {Number}
 *            zoom
 * @since 1.2
 */
BMap.Map.prototype.setZoom = function(zoom) {
};
/**
 * 是否使用高分辨率底图。仅当mapOptions.enableHighResolution属性为true且设备支持高分辨率时返回true。
 *
 *
 * @return {BMap.Boolean}
 *
 */
BMap.Map.prototype.highResolutionEnabled = function() {
};
/**
 * 放大一级视图。
 *
 *
 *
 */
BMap.Map.prototype.zoomIn = function() {
};
/**
 * 缩小一级视图。
 *
 *
 *
 */
BMap.Map.prototype.zoomOut = function() {
};
/**
 * 为地图添加热区。 (自 1.2 新增)
 *
 *
 * @param {BMap.Hotspot}
 *            hotspot
 * @since 1.2
 */
BMap.Map.prototype.addHotspot = function(hotspot) {
};
/**
 * 移除某个地图热区。 (自 1.2 新增)
 *
 *
 * @param {BMap.Hotspot}
 *            hotspot
 * @since 1.2
 */
BMap.Map.prototype.removeHotspot = function(hotspot) {
};
/**
 * 清空地图所有热区。 (自 1.2 新增)
 *
 *
 * @since 1.2
 */
BMap.Map.prototype.clearHotspots = function() {
};
/**
 * 通知地图容器尺寸发生变化。默认情况下地图会自动检测容器尺寸是否发生变化并重新加载图块，如果修改了MapOptions的enableAutoResize为false，或者调用了Map的disableAutoResize()方法，那么需要调用此方法来通知地图。
 * (自 1.2 新增)
 *
 *
 * @since 1.2
 */
BMap.Map.prototype.checkResize = function() {
};
/*
 * ================================================================================
 *
 * 控件方法
 *
 * ================================================================================
 */
/**
 * 将控件添加到地图，一个控件实例只能向地图中添加一次。
 *
 *
 * @param {BMap.Control}
 *            control
 *
 */
BMap.Map.prototype.addControl = function(control) {
};
/**
 * 从地图中移除控件。如果控件从未被添加到地图中，则该移除不起任何作用。
 *
 *
 * @param {BMap.Control}
 *            control
 *
 */
BMap.Map.prototype.removeControl = function(control) {
};
/**
 * 返回地图的容器元素。当创建用户自定义控件时，需要自行实现Control.initialize()方法，并将控件的容器元素添加到地图上，通过此方法可获得地图容器。
 *
 *
 * @return {HTMLElement}
 *
 */
BMap.Map.prototype.getContainer = function() {
};
/*
 * ================================================================================
 *
 * 右键菜单方法
 *
 * ================================================================================
 */
/**
 * 添加右键菜单。
 *
 *
 * @param {BMap.ContextMenu}
 *            menu
 *
 */
BMap.Map.prototype.addContextMenu = function(menu) {
};
/**
 * 移除右键菜单。
 *
 *
 * @param {BMap.ContextMenu}
 *            menu
 *
 */
BMap.Map.prototype.removeContextMenu = function(menu) {
};
/*
 * ================================================================================
 *
 * 覆盖物方法
 *
 * ================================================================================
 */
/**
 * 将覆盖物添加到地图中，一个覆盖物实例只能向地图中添加一次。
 *
 *
 * @param {BMap.Overlay}
 *            overlay
 *
 */
BMap.Map.prototype.addOverlay = function(overlay) {
};
/**
 * 从地图中移除覆盖物。如果覆盖物从未被添加到地图中，则该移除不起任何作用。
 *
 *
 * @param {BMap.Overlay}
 *            overlay
 *
 */
BMap.Map.prototype.removeOverlay = function(overlay) {
};
/**
 * 清除地图上所有覆盖物。
 *
 *
 *
 */
BMap.Map.prototype.clearOverlays = function() {
};
/**
 * 在地图上打开信息窗口。
 *
 *
 * @param {BMap.InfoWindow}
 *            infoWnd
 * @param {BMap.Point}
 *            point
 *
 */
BMap.Map.prototype.openInfoWindow = function(infoWnd, point) {
};
/**
 * 关闭在地图上打开的信息窗口。在标注上打开的信息窗口也可通过此方法进行关闭。
 *
 *
 *
 */
BMap.Map.prototype.closeInfoWindow = function() {
};
/**
 * 根据地理坐标获取对应的覆盖物容器的坐标，此方法用于自定义覆盖物。(自 1.1 新增)
 *
 *
 * @param {BMap.Point}
 *            point
 * @return {BMap.Pixel}
 * @since 1.1
 */
BMap.Map.prototype.pointToOverlayPixel = function(point) {
};
/**
 * 根据覆盖物容器的坐标获取对应的地理坐标。(自 1.1 新增)
 *
 *
 * @param {BMap.Pixel}
 *            pixel
 * @return {BMap.Point}
 * @since 1.1
 */
BMap.Map.prototype.overlayPixelToPoint = function(pixel) {
};
/**
 * 返回地图上处于打开状态的信息窗的实例。当地图没有打开的信息窗口时，此方法返回null。(自 1.1 新增)
 *
 *
 * @return {BMap.InfoWindow|Null}
 * @since 1.1
 */
BMap.Map.prototype.getInfoWindow = function() {
};
/**
 * 返回地图上的所有覆盖物。(自 1.1 新增)
 *
 *
 * @return {BMap.Overlay[]}
 * @since 1.1
 */
BMap.Map.prototype.getOverlays = function() {
};
/**
 * 返回地图覆盖物容器列表。(自 1.1 新增)
 *
 *
 * @return {BMap.MapPanes}
 * @since 1.1
 */
BMap.Map.prototype.getPanes = function() {
};
/*
 * ================================================================================
 *
 * 地图图层方法
 *
 * ================================================================================
 */
/**
 * 添加一个自定义地图图层。
 *
 *
 * @param {BMap.TileLayer}
 *            tileLayer
 *
 */
BMap.Map.prototype.addTileLayer = function(tileLayer) {
};
/**
 * 移除一个自定义地图图层。
 *
 *
 * @param {BMap.TileLayer}
 *            tilelayer
 *
 */
BMap.Map.prototype.removeTileLayer = function(tilelayer) {
};
/*
 * ================================================================================
 *
 * 坐标变换
 *
 * ================================================================================
 */
/**
 * 像素坐标转换为经纬度坐标。
 *
 *
 * @param {BMap.Pixel}
 *            pixel
 * @return {BMap.Point}
 *
 */
BMap.Map.prototype.pixelToPoint = function(pixel) {
};
/**
 * 经纬度坐标转换为像素坐标。
 *
 *
 * @param {BMap.Point}
 *            point
 * @return {BMap.Pixel}
 *
 */
BMap.Map.prototype.pointToPixel = function(point) {
};
/**
 * 此类是panBy和panTo方法的可选参数，没有构造函数，通过对象字面量形式表示。
 *
 *
 */
BMap.PanOptions = function() {
};
/**
 *
 * 是否在平移过程中禁止动画。（自1.2新增）
 *
 * @type {BMap.Boolean}
 *
 */
BMap.PanOptions.prototype.noAnimation = false;
/**
 * 此类表示Map构造函数的可选参数。它没有构造函数，但可通过对象字面量形式表示。
 *
 *
 */
BMap.MapOptions = function() {
};
/**
 *
 * 地图允许展示的最小级别。 (自 1.2 废弃)
 *
 * @type {Number}
 * @deprecated 1.2
 */
BMap.MapOptions.prototype.zoomLevelMin = 0;
/**
 *
 * 地图允许展示的最大级别。 (自 1.2 废弃)
 *
 * @type {Number}
 * @deprecated 1.2
 */
BMap.MapOptions.prototype.zoomLevelMax = 0;
/**
 *
 * 地图允许展示的最小级别。 (自 1.2 新增)
 *
 * @type {Number}
 * @since 1.2
 */
BMap.MapOptions.prototype.minZoom = 0;
/**
 *
 * 地图允许展示的最大级别。 (自 1.2 新增)
 *
 * @type {Number}
 * @since 1.2
 */
BMap.MapOptions.prototype.maxZoom = 0;
/**
 *
 * 地图类型，默认为BMAP_NORMAL_MAP。 (自 1.1 新增)
 *
 * @type {BMap.MapType}
 * @since 1.1
 */
BMap.MapOptions.prototype.mapType = null;
/**
 *
 * 是否启用使用高分辨率地图。在iPhone4及其后续设备上，可以通过开启此选项获取更高分辨率的底图，v1.2,v1.3版本默认不开启，v1.4默认为开启状态。
 * (自 1.2 新增)
 *
 * @type {BMap.Boolean}
 * @since 1.2
 */
BMap.MapOptions.prototype.enableHighResolution = false;
/**
 *
 * 是否自动适应地图容器变化，默认启用。 (自 1.2 新增)
 *
 * @type {BMap.Boolean}
 * @since 1.2
 */
BMap.MapOptions.prototype.enableAutoResize = false;
/**
 *
 * 是否开启底图可点功能，默认启用。 (自 1.5 新增)
 *
 * @type {BMap.Boolean}
 * @since 1.5
 */
BMap.MapOptions.prototype.enableMapClick = false;
/**
 * 此类代表视野，不可实例化，通过对象字面量形式表示。(自 1.1 新增)
 *
 *
 */
BMap.Viewport = function() {
};
/**
 *
 * 视野中心点。
 *
 * @type {BMap.Point}
 *
 */
BMap.Viewport.prototype.center = null;
/**
 *
 * 视野级别。
 *
 * @type {Number}
 *
 */
BMap.Viewport.prototype.zoom = 0;
/**
 * 此类作为map.getViewport与map.setViewport方法的可选参数，不可实例化。
 *
 *
 */
BMap.ViewportOptions = function() {
};
/**
 *
 * 是否启用动画效果移动地图，默认为true。当调整后的级别与当前地图级别一致时，将使用动画效果移动地图。(自 1.1 新增)
 *
 * @type {BMap.Boolean}
 * @since 1.1
 */
BMap.ViewportOptions.prototype.enableAnimation = false;
/**
 *
 * 视野调整的预留边距，例如： margins: [30, 20, 0, 20] 表示坐标点会限制在上述区域内。 (自 1.1 新增)
 *
 * @type {BMap.Number[]}
 * @since 1.1
 */
BMap.ViewportOptions.prototype.margins = null;
/**
 *
 * 地图级别的偏移量，您可以在方法得出的结果上增加一个偏移值。例如map.setViewport计算出地图的级别为10，如果zoomFactor为-1，则最终的地图级别为9。
 * (自 1.1 新增)
 *
 * @type {Number}
 * @since 1.1
 */
BMap.ViewportOptions.prototype.zoomFactor = 0;
/**
 *
 * 改变地图视野的延迟执行时间，单位毫秒，默认为200ms。此延时仅针对动画效果有效。 (自 1.1 新增)
 *
 * @type {Number}
 * @since 1.1
 */
BMap.ViewportOptions.prototype.delay = 0;
/**
 *
 * 返回当前API版本。例如，1.2版本返回字符串“1.2”。(自 1.2 新增)
 *
 * @constant
 * @type {BMap.String}
 * @default
 * @since 1.2
 */
var BMAP_API_VERSION = "";
/**
 * 此类表示一个地理坐标点。
 *
 * <p>
 * 以指定的经度和纬度创建一个地理点坐标。
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.Point
 * @param {Number}
 *            lng
 * @param {Number}
 *            lat
 *
 */
BMap.Point = function(lng, lat) {
};
/**
 *
 * 地理经度。
 *
 * @type {Number}
 *
 */
BMap.Point.prototype.lng = 0;
/**
 *
 * 地理纬度。
 *
 * @type {Number}
 *
 */
BMap.Point.prototype.lat = 0;
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 判断坐标点是否相等，当且仅当两点的经度和纬度均相等时返回true。
 *
 *
 * @param {BMap.Point}
 *            other
 * @return {BMap.Boolean}
 *
 */
BMap.Point.prototype.equals = function(other) {
};
/**
 * 此类表示地图上的一点，单位为像素。
 *
 * <p>
 * 创建像素点对象实例。像素坐标的坐标原点为地图区域的左上角。
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.Pixel
 * @param {Number}
 *            x
 * @param {Number}
 *            y
 *
 */
BMap.Pixel = function(x, y) {
};
/**
 *
 * x坐标。
 *
 * @type {Number}
 *
 */
BMap.Pixel.prototype.x = 0;
/**
 *
 * y坐标。
 *
 * @type {Number}
 *
 */
BMap.Pixel.prototype.y = 0;
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 判断坐标点是否相等，当且仅当两点的x坐标和y坐标均相等时返回true。
 *
 *
 * @param {BMap.Pixel}
 *            other
 * @return {BMap.Boolean}
 *
 */
BMap.Pixel.prototype.equals = function(other) {
};
/**
 * 此类表示地理坐标的矩形区域。
 *
 * <p>
 * 创建一个包含所有给定点坐标的矩形区域。 (自 1.2 废弃)
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.Bounds
 * @param {Number}
 *            minX
 * @param {Number}
 *            minY
 * @param {Number}
 *            maxX
 * @param {Number}
 *            maxY
 *
 */
BMap.Bounds = function(minX, minY, maxX, maxY) {
};
/**
 *
 * 矩形左下角的x坐标。 (自1.2废弃)
 *
 * @type {Number}
 * @deprecated 1.2
 */
BMap.Bounds.prototype.minX = 0;
/**
 *
 * 矩形左下角的y坐标。 (自1.2废弃)
 *
 * @type {Number}
 * @deprecated 1.2
 */
BMap.Bounds.prototype.minY = 0;
/**
 *
 * 矩形右上角的x坐标。 (自1.2废弃)
 *
 * @type {Number}
 * @deprecated 1.2
 */
BMap.Bounds.prototype.maxX = 0;
/**
 *
 * 矩形右上角的y坐标。 (自1.2废弃)
 *
 * @type {Number}
 * @deprecated 1.2
 */
BMap.Bounds.prototype.maxY = 0;
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 当且仅当此矩形中的两点参数都等于其他矩形的两点参数时，返回true。
 *
 *
 * @param {BMap.Bounds}
 *            other
 * @return {BMap.Boolean}
 *
 */
BMap.Bounds.prototype.equals = function(other) {
};
/**
 * 如果点的地理坐标位于此矩形内，则返回true。
 *
 *
 * @param {BMap.Point}
 *            point
 * @return {BMap.Boolean}
 *
 */
BMap.Bounds.prototype.containsPoint = function(point) {
};
/**
 * 传入的矩形区域完全包含于此矩形区域中，则返回true。
 *
 *
 * @param {BMap.Bounds}
 *            bounds
 * @return {BMap.Boolean}
 *
 */
BMap.Bounds.prototype.containsBounds = function(bounds) {
};
/**
 * 计算与另一矩形的交集区域。
 *
 *
 * @param {BMap.Bounds}
 *            other
 * @return {BMap.Bounds}
 *
 */
BMap.Bounds.prototype.intersects = function(other) {
};
/**
 * 放大此矩形，使其包含给定的点。
 *
 *
 * @param {BMap.Point}
 *            point
 *
 */
BMap.Bounds.prototype.extend = function(point) {
};
/**
 * 返回矩形的中心点。
 *
 *
 * @return {BMap.Point}
 *
 */
BMap.Bounds.prototype.getCenter = function() {
};
/**
 * 如果矩形为空，则返回true。
 *
 *
 * @return {BMap.Boolean}
 *
 */
BMap.Bounds.prototype.isEmpty = function() {
};
/**
 * 返回矩形区域的西南角。 (自 1.2 新增)
 *
 *
 * @return {BMap.Point}
 * @since 1.2
 */
BMap.Bounds.prototype.getSouthWest = function() {
};
/**
 * 返回矩形区域的东北角。 (自 1.2 新增)
 *
 *
 * @return {BMap.Point}
 * @since 1.2
 */
BMap.Bounds.prototype.getNorthEast = function() {
};
/**
 * 返回矩形区域的跨度。 (自 1.2 新增)
 *
 *
 * @return {BMap.Point}
 * @since 1.2
 */
BMap.Bounds.prototype.toSpan = function() {
};
/**
 * 此类以像素表示一个矩形区域的大小。
 *
 * <p>
 * 以指定的宽度和高度创建一个矩形区域大小对象。
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.Size
 * @param {Number}
 *            width
 * @param {Number}
 *            height
 *
 */
BMap.Size = function(width, height) {
};
/**
 *
 * 水平方向数值。
 *
 * @type {Number}
 *
 */
BMap.Size.prototype.width = 0;
/**
 *
 * 竖直方向的数值。
 *
 * @type {Number}
 *
 */
BMap.Size.prototype.height = 0;
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 当且仅当此矩形中的宽度和高度都等于其他矩形的宽度和高度时，返回true。
 *
 *
 * @param {BMap.Size}
 *            other
 * @return {BMap.Boolean}
 *
 */
BMap.Size.prototype.equals = function(other) {
};
/**
 * 此类是所有控件的基类，您可以通过此类来实现自定义控件。所有控件均包含Control类的属性、方法和事件。通过Map.addControl()方法可将控件添加到地图上。
 *
 * <p>
 * 创建一个控件原型实例，通过该原型实例可创建自定义控件。
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.Control
 *
 */
BMap.Control = function() {
};
/**
 *
 * 控件默认的停靠位置。自定义控件时需要提供此属性，作为控件的默认停靠位置。
 *
 * @type {enum ControlAnchor}
 *
 */
BMap.Control.prototype.defaultAnchor = null;
/**
 *
 * 控件默认的位置偏移值。自定义控件时需要提供此属性，作为控件的默认偏移位置。
 *
 * @type {BMap.Size}
 *
 */
BMap.Control.prototype.defaultOffset = null;
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 抽象方法。调用Map.addControl()方法添加控件时将调用此方法，从而实现该控件的初始化。自定义控件时需要实现此方法，并将元素的DOM元素在该方法中返回。DOM元素需要添加到地图的容器中，使用map.getContainer()方法可获得地图容器元素。
 *
 *
 * @param {BMap.Map}
 *            map
 * @return {BMap.HTMLElement}
 *
 */
BMap.Control.prototype.initialize = function(map) {
};
/**
 * 设置控件停靠的位置
 *
 *
 * @param {enumControlAnchor}
 *            anchor
 *
 */
BMap.Control.prototype.setAnchor = function(anchor) {
};
/**
 * 返回控件停靠的位置
 *
 *
 * @return {enum ControlAnchor}
 *
 */
BMap.Control.prototype.getAnchor = function() {
};
/**
 * 设置控件停靠的偏移量
 *
 *
 * @param {BMap.Size}
 *            offset
 *
 */
BMap.Control.prototype.setOffset = function(offset) {
};
/**
 * 返回控件停靠的偏移量
 *
 *
 * @return {BMap.Size}
 *
 */
BMap.Control.prototype.getOffset = function() {
};
/**
 * 显示控件
 *
 *
 *
 */
BMap.Control.prototype.show = function() {
};
/**
 * 隐藏控件
 *
 *
 *
 */
BMap.Control.prototype.hide = function() {
};
/**
 * 判断控件的可见性
 *
 *
 * @return {BMap.Boolean}
 *
 */
BMap.Control.prototype.isVisible = function() {
};
/**
 * 返回描述类型字符串。如果需要，自定义控件需要自行实现此方法以获得描述自身类型的字符串。
 *
 *
 * @return {BMap.String}
 *
 */
BMap.Control.prototype.toString = function() {
};
/**
 * 此类表示NavigationControl构造函数的可选参数。它没有构造函数，但可通过对象字面量形式表示。
 *
 *
 */
BMap.NavigationControlOptions = function() {
};
/**
 *
 * 控件的停靠位置。
 *
 * @type {enum ControlAnchor}
 *
 */
BMap.NavigationControlOptions.prototype.anchor = null;
/**
 *
 * 控件的水平偏移值。
 *
 * @type {BMap.Size}
 *
 */
BMap.NavigationControlOptions.prototype.offset = null;
/**
 *
 * 平移缩放控件的类型。
 *
 * @type {enum NavigationControlType}
 *
 */
BMap.NavigationControlOptions.prototype.type = null;
/**
 *
 * 是否显示级别提示信息。(自 1.1 新增)
 *
 * @type {BMap.Boolean}
 * @since 1.1
 */
BMap.NavigationControlOptions.prototype.showZoomInfo = false;
/**
 * 此类表示ScaleControl构造函数的可选参数。它没有构造函数，但可通过对象字面量形式表示。
 *
 *
 */
BMap.ScaleControlOptions = function() {
};
/**
 *
 * 控件的停靠位置。
 *
 * @type {enum ControlAnchor}
 *
 */
BMap.ScaleControlOptions.prototype.anchor = null;
/**
 *
 * 控件的偏移值。
 *
 * @type {BMap.Size}
 *
 */
BMap.ScaleControlOptions.prototype.offset = null;
/**
 * 此类表示CopyrightControl构造函数的可选参数。它没有构造函数，但可通过对象字面量形式表示。
 *
 *
 */
BMap.CopyrightControlOptions = function() {
};
/**
 *
 * 控件的停靠位置。
 *
 * @type {enum ControlAnchor}
 *
 */
BMap.CopyrightControlOptions.prototype.anchor = null;
/**
 *
 * 控件的偏移值。
 *
 * @type {BMap.Size}
 *
 */
BMap.CopyrightControlOptions.prototype.offset = null;
/**
 *
 * 控件将定位到地图的左上角。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_ANCHOR_TOP_LEFT = 0;
/**
 *
 * 控件将定位到地图的右上角。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_ANCHOR_TOP_RIGHT = 0;
/**
 *
 * 控件将定位到地图的左下角。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_ANCHOR_BOTTOM_LEFT = 0;
/**
 *
 * 控件将定位到地图的右下角。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_ANCHOR_BOTTOM_RIGHT = 0;
/**
 * 此类表示缩略地图控件。
 *
 * <p>
 * 创建一个缩略地图控件实例。
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.OverviewMapControl
 * @param {BMap.OverviewMapControlOptions}
 *            opts
 *
 */
BMap.OverviewMapControl = function(opts) {
    /**
     * @event viewchanged 缩略地图开合状态发生变化后触发此事件。
     * @param {eventtype}
     * @param {target}
     * @param {isOpen}
     *
     */
    /**
     * @event viewchanging 缩略地图开合状态发生变化过程中触发此事件。
     * @param {eventtype}
     * @param {target}
     *
     */
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 切换缩略地图控件的开合状态。
 *
 *
 *
 */
BMap.OverviewMapControl.prototype.changeView = function() {
};
/**
 * 设置缩略地图的大小。
 *
 *
 * @param {BMap.Size}
 *            size
 *
 */
BMap.OverviewMapControl.prototype.setSize = function(size) {
};
/**
 * 返回缩略地图的大小。
 *
 *
 * @return {BMap.Size}
 *
 */
BMap.OverviewMapControl.prototype.getSize = function() {
};
/**
 *
 * 公制单位。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_UNIT_METRIC = 0;
/**
 *
 * 英制单位。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_UNIT_IMPERIAL = 0;
/**
 * 此类是负责切换地图类型的控件，此类继承Control所有功能。
 *
 * <p>
 * 创建地图类型控件。(自 1.1 新增)
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.MapTypeControl
 * @param {BMap.MapTypeControlOptions}
 *            opts
 *
 */
BMap.MapTypeControl = function(opts) {
};
/**
 * 此类表示地图的平移缩放控件，可以对地图进行上下左右四个方向的平移和缩放操作。
 *
 * <p>
 * 创建一个特定样式的地图平移缩放控件。
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.NavigationControl
 * @param {BMap.NavigationControlOptions}
 *            type
 *
 */
BMap.NavigationControl = function(type) {
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 返回平移缩放控件的类型。
 *
 *
 * @return {enum NavigationControlType}
 *
 */
BMap.NavigationControl.prototype.getType = function() {
};
/**
 * 设置平移缩放控件的类型。
 *
 *
 * @param {enumNavigationControlType}
 *            type
 *
 */
BMap.NavigationControl.prototype.setType = function(type) {
};
/**
 * 此类表示OverviewMapControl构造函数的可选参数。它没有构造函数，但可通过对象字面量形式表示。
 *
 *
 */
BMap.OverviewMapControlOptions = function() {
};
/**
 *
 * 控件的停靠位置。
 *
 * @type {enum ControlAnchor}
 *
 */
BMap.OverviewMapControlOptions.prototype.anchor = null;
/**
 *
 * 控件的偏移值。
 *
 * @type {BMap.Size}
 *
 */
BMap.OverviewMapControlOptions.prototype.offset = null;
/**
 *
 * 缩略地图控件的大小。
 *
 * @type {BMap.Size}
 *
 */
BMap.OverviewMapControlOptions.prototype.size = null;
/**
 *
 * 缩略地图添加到地图后的开合状态，默认为关闭。
 *
 * @type {BMap.Boolean}
 *
 */
BMap.OverviewMapControlOptions.prototype.isOpen = false;
/**
 * 此类表示版权控件，您可以在地图上添加自己的版权信息。每一个版权信息需要包含如下内容：版权的唯一标识、版权内容和其适用的区域范围。
 *
 * <p>
 * 创建一个版权控件实例。
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.CopyrightControl
 * @param {BMap.CopyrightControlOptions}
 *            opts
 *
 */
BMap.CopyrightControl = function(opts) {
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 添加版权信息。
 *
 *
 * @param {BMap.Copyright}
 *            copyright
 *
 */
BMap.CopyrightControl.prototype.addCopyright = function(copyright) {
};
/**
 * 移除版权信息。
 *
 *
 * @param {BMap.String}
 *            id
 *
 */
BMap.CopyrightControl.prototype.removeCopyright = function(id) {
};
/**
 * 返回单个版权信息。
 *
 *
 * @param {BMap.String}
 *            id
 * @return {BMap.Copyright}
 *
 */
BMap.CopyrightControl.prototype.getCopyright = function(id) {
};
/**
 * 返回版权信息列表。
 *
 *
 * @return {BMap.Copyright[]}
 *
 */
BMap.CopyrightControl.prototype.getCopyrightCollection = function() {
};
/**
 * 此类表示MapTypeControl构造函数的可选参数。它没有构造函数，但可通过对象字面量形式表示（自 1.2 新增）。
 *
 *
 */
BMap.MapTypeControlOptions = function() {
};
/**
 *
 * 控件样式。
 *
 * @type {BMap.MapTypeControlType}
 *
 */
BMap.MapTypeControlOptions.prototype.type = null;
/**
 *
 * 控件展示的地图类型，默认为普通图、卫星图、卫星加路网混合图和三维图。通过此属性可配置控件展示的地图类型。
 *
 * @type {BMap.MapType[]}
 *
 */
BMap.MapTypeControlOptions.prototype.mapTypes = null;
/**
 *
 * 标准的平移缩放控件（包括平移、缩放按钮和滑块）。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_NAVIGATION_CONTROL_LARGE = 0;
/**
 *
 * 仅包含平移和缩放按钮。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_NAVIGATION_CONTROL_SMALL = 0;
/**
 *
 * 仅包含平移按钮。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_NAVIGATION_CONTROL_PAN = 0;
/**
 *
 * 仅包含缩放按钮。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_NAVIGATION_CONTROL_ZOOM = 0;
/**
 * 此类表示比例尺控件。
 *
 * <p>
 * 创建一个比例尺控件。
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.ScaleControl
 * @param {BMap.ScaleControlOptions}
 *            opts
 *
 */
BMap.ScaleControl = function(opts) {
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 返回比例尺单位制。
 *
 *
 * @return {enum LengthUnit}
 *
 */
BMap.ScaleControl.prototype.getUnit = function() {
};
/**
 * 设置比例尺单位制。
 *
 *
 * @param {enumLengthUnit}
 *            unit
 *
 */
BMap.ScaleControl.prototype.setUnit = function(unit) {
};
/**
 * 此类表示一条版权信息。可作为CopyrightControl.addCopyright()方法的参数。它没有构造函数，但可通过对象字面量形式表示。
 *
 *
 */
BMap.Copyright = function() {
};
/**
 *
 * 该版权信息的唯一标识符。
 *
 * @type {Number}
 *
 */
BMap.Copyright.prototype.id = 0;
/**
 *
 * 该版权的文本信息，用于显示在地图上，支持HTML内容。
 *
 * @type {BMap.String}
 *
 */
BMap.Copyright.prototype.content = "";
/**
 *
 * 该版权信息所适用的地理区域。
 *
 * @type {BMap.Bounds}
 *
 */
BMap.Copyright.prototype.bounds = null;
/**
 *
 * 按钮水平方式展示，默认采用此类型展示。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_MAPTYPE_CONTROL_HORIZONTAL = 0;
/**
 *
 * 按钮呈下拉列表方式展示。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_MAPTYPE_CONTROL_DROPDOWN = 0;
/**
 * 此类是负责进行地图定位的控件，使用html5浏览器定位功能，此类继承Control所有功能。(自 1.4 新增)
 *
 * <p>
 * 创建一个特定样式的地图定位控件。
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.GeolocationControl
 * @param {BMap.GeolocationControlOptions}
 *            type
 * @since 1.4
 */
BMap.GeolocationControl = function(type) {
    /**
     * @event locationSuccess 定位成功后触发此事件。
     * @param {point}
     * @param {BMap.AddressComponent}
     *
     */
    /**
     * @event locationError 定位失败后触发此事件。
     * @param {BMap.StatusCode}
     *
     */
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 开始进行定位。
 *
 *
 *
 */
BMap.GeolocationControl.prototype.location = function() {
};
/**
 * 返回当前的定位信息。若当前还未定位，则返回null。
 *
 *
 * @return {BMap.AddressComponent}
 *
 */
BMap.GeolocationControl.prototype.getAddressComponent = function() {
};
/**
 * 此类表示GeolocationControl构造函数的可选参数。它没有构造函数，但可通过对象字面量形式表示。(自 1.4 新增)
 *
 *
 */
BMap.GeolocationControlOptions = function() {
};
/**
 *
 * 控件的停靠位置，默认定位到地图的右下角。
 *
 * @type {enum ControlAnchor}
 *
 */
BMap.GeolocationControlOptions.prototype.anchor = null;
/**
 *
 * 控件的水平偏移值。
 *
 * @type {BMap.Size}
 *
 */
BMap.GeolocationControlOptions.prototype.offset = null;
/**
 *
 * 是否显示定位信息面板。默认显示定位信息面板。
 *
 * @type {BMap.Boolean}
 *
 */
BMap.GeolocationControlOptions.prototype.showAddressBar = false;
/**
 *
 * 添加控件时是否进行定位。默认添加控件时不进行定位。
 *
 * @type {BMap.Boolean}
 *
 */
BMap.GeolocationControlOptions.prototype.enableAutoLocation = false;
/**
 *
 * 可自定义定位中心点的Icon样式。
 *
 * @type {BMap.Icon}
 *
 */
BMap.GeolocationControlOptions.prototype.locationIcon = null;
/**
 *
 * 没有权限。对应数值“6”。(自 1.1 新增)
 *
 * @constant
 * @type {Number}
 * @default
 * @since 1.1
 */
var BMAP_STATUS_PERMISSION_DENIED = 0;
/**
 *
 * 服务不可用。对应数值“7”。(自 1.1 新增)
 *
 * @constant
 * @type {Number}
 * @default
 * @since 1.1
 */
var BMAP_STATUS_SERVICE_UNAVAILABLE = 0;
/**
 *
 * 超时。对应数值“8”。(自 1.1 新增)
 *
 * @constant
 * @type {Number}
 * @default
 * @since 1.1
 */
var BMAP_STATUS_TIMEOUT = 0;
/**
 * 覆盖物的抽象基类，所有覆盖物均继承基类的方法。此类不可实例化。
 *
 *
 */
BMap.Overlay = function() {
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 抽象方法，用于初始化覆盖物，当调用map.addOverlay时，API将调用此方法。自定义覆盖物时需要实现此方法。自定义覆盖物时需要将覆盖物对应的HTML元素返回。(自
 * 1.1 新增)
 *
 *
 * @param {BMap.Map}
 *            map
 * @return {BMap.HTMLElement}
 * @since 1.1
 */
BMap.Overlay.prototype.initialize = function(map) {
};
/**
 * 判断覆盖物是否可见。
 *
 *
 * @return {BMap.Boolean}
 *
 */
BMap.Overlay.prototype.isVisible = function() {
};
/**
 * 释放覆盖物对象所占用的内存。可在覆盖物被移除后调用此方法，此后该覆盖物将不能再次添加到地图上。自1.1版本开始API会自动管理内存，您不再需要调用此方法。
 * (自 1.2 废弃)
 *
 *
 * @deprecated 1.2
 */
BMap.Overlay.prototype.dispose = function() {
};
/**
 * 抽象方法，当地图状态发生变化时，由系统调用对覆盖物进行绘制。自定义覆盖物需要实现此方法。 (自 1.1 新增)
 *
 *
 * @since 1.1
 */
BMap.Overlay.prototype.draw = function() {
};
/**
 * 显示覆盖物。对于自定义覆盖物，此方法会自动将initialize方法返回的HTML元素样式的display属性设置为空。 (自 1.1 更新)
 *
 *
 * @version 1.1
 */
BMap.Overlay.prototype.show = function() {
};
/**
 * 隐藏覆盖物。对于自定义覆盖物，此方法会自动将initialize方法返回的HTML元素样式的display属性设置为none。 (自 1.1 更新)
 *
 *
 * @version 1.1
 */
BMap.Overlay.prototype.hide = function() {
};
/**
 * 此类表示Icon构造函数的可选参数。它没有构造函数，但可通过对象字面量形式表示。
 *
 *
 */
BMap.IconOptions = function() {
};
/**
 *
 * 图标的定位锚点。此点用来决定图标与地理位置的关系，是相对于图标左上角的偏移值，默认等于图标宽度和高度的中间值。 (自 1.2 废弃)
 *
 * @type {BMap.Size}
 * @deprecated 1.2
 */
BMap.IconOptions.prototype.offset = null;
/**
 *
 * 图标的定位锚点。此点用来决定图标与地理位置的关系，是相对于图标左上角的偏移值，默认等于图标宽度和高度的中间值。 (自 1.2 新增)
 *
 * @type {BMap.Size}
 * @since 1.2
 */
BMap.IconOptions.prototype.anchor = null;
/**
 *
 * 图片相对于可视区域的偏移值。
 *
 * @type {BMap.Size}
 *
 */
BMap.IconOptions.prototype.imageOffset = null;
/**
 *
 * 信息窗口定位锚点。开启信息窗口时，信息窗口底部尖角相对于图标左上角的位置，默认等于图标的anchor。 (自 1.2 废弃)
 *
 * @type {BMap.Size}
 * @deprecated 1.2
 */
BMap.IconOptions.prototype.infoWindowOffset = null;
/**
 *
 * 信息窗口定位锚点。开启信息窗口时，信息窗口底部尖角相对于图标左上角的位置，默认等于图标的anchor。 (自 1.2 新增)
 *
 * @type {BMap.Size}
 * @since 1.2
 */
BMap.IconOptions.prototype.infoWindowAnchor = null;
/**
 *
 * 用于打印的图片，此属性只适用于IE6，为了解决IE6在包含滤镜的情况下打印样式不正确的问题。 (自 1.1 新增)
 *
 * @type {BMap.String}
 * @since 1.1
 */
BMap.IconOptions.prototype.printImageUrl = "";
/**
 * 使用浏览器的矢量制图工具（如果可用）在地图上绘制折线的地图叠加层。
 *
 * <p>
 * 创建折线覆盖物对象
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.Polyline
 * @param {BMap.Point[]}
 *            points
 * @param {BMap.PolylineOptions}
 *            [opts]
 *
 */
BMap.Polyline = function(points, opts) {
    /**
     * @event click 点击折线后会触发此事件。
     * @param {eventtype}
     * @param {target}
     * @param {point}
     * @param {pixel}
     *
     */
    /**
     * @event dblclick 双击折线后会触发此事件。
     * @param {eventtype}
     * @param {target}
     * @param {point}
     * @param {pixel}
     *
     */
    /**
     * @event mousedown 鼠标在折线上按下触发此事件。
     * @param {eventtype}
     * @param {target}
     * @param {point}
     * @param {pixel}
     *
     */
    /**
     * @event mouseup 鼠标在折线释放触发此事件。
     * @param {eventtype}
     * @param {target}
     * @param {point}
     * @param {pixel}
     *
     */
    /**
     * @event mouseout 鼠标离开折线时触发此事件。
     * @param {eventtype}
     * @param {target}
     * @param {point}
     * @param {pixel}
     *
     */
    /**
     * @event mouseover 当鼠标进入折线区域时会触发此事件。
     * @param {eventtype}
     * @param {target}
     * @param {point}
     * @param {pixel}
     *
     */
    /**
     * @event remove 移除折线时触发。
     * @param {eventtype}
     * @param {target}
     *
     */
    /**
     * @event lineupdate 覆盖物的属性发生变化时触发。(自 1.1 新增)
     * @param {eventtype}
     * @param {target}
     * @since 1.1
     */
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 设置折线的点数组。(自 1.2 废弃)
 *
 *
 * @param {BMap.Point[]}
 *            points
 * @deprecated 1.2
 */
BMap.Polyline.prototype.setPoints = function(points) {
};
/**
 * 返回折线的点数组。(自 1.2 废弃)
 *
 *
 * @return {BMap.Point[]}
 * @deprecated 1.2
 */
BMap.Polyline.prototype.getPoints = function() {
};
/**
 * 设置折线的点数组（自1.2新增）
 *
 *
 * @param {BMap.Point[]}
 *            path
 *
 */
BMap.Polyline.prototype.setPath = function(path) {
};
/**
 * 返回折线的点数组（自1.2新增）
 *
 *
 * @return {BMap.Point[]}
 *
 */
BMap.Polyline.prototype.getPath = function() {
};
/**
 * 设置折线的颜色。
 *
 *
 * @param {BMap.String}
 *            color
 *
 */
BMap.Polyline.prototype.setStrokeColor = function(color) {
};
/**
 * 返回折线的颜色。
 *
 *
 * @return {BMap.String}
 *
 */
BMap.Polyline.prototype.getStrokeColor = function() {
};
/**
 * 设置透明度，取值范围0 - 1。
 *
 *
 * @param {Number}
 *            opacity
 *
 */
BMap.Polyline.prototype.setStrokeOpacity = function(opacity) {
};
/**
 * 返回透明度。
 *
 *
 * @return {Number}
 *
 */
BMap.Polyline.prototype.getStrokeOpacity = function() {
};
/**
 * 设置线的宽度，范围为大于等于1的整数。
 *
 *
 * @param {Number}
 *            weight
 *
 */
BMap.Polyline.prototype.setStrokeWeight = function(weight) {
};
/**
 * 返回线的宽度。
 *
 *
 * @return {Number}
 *
 */
BMap.Polyline.prototype.getStrokeWeight = function() {
};
/**
 * 设置是为实线或虚线，solid或dashed。
 *
 *
 * @param {BMap.String}
 *            style
 *
 */
BMap.Polyline.prototype.setStrokeStyle = function(style) {
};
/**
 * 返回当前线样式状态，实线或者虚线。
 *
 *
 * @return {BMap.String}
 *
 */
BMap.Polyline.prototype.getStrokeStyle = function() {
};
/**
 * 返回覆盖物的地理区域范围。(自 1.1 新增)
 *
 *
 * @return {BMap.Bounds}
 * @since 1.1
 */
BMap.Polyline.prototype.getBounds = function() {
};
/**
 * 开启编辑功能。(自 1.1 新增)
 *
 *
 * @since 1.1
 */
BMap.Polyline.prototype.enableEditing = function() {
};
/**
 * 关闭编辑功能。(自 1.1 新增)
 *
 *
 * @since 1.1
 */
BMap.Polyline.prototype.disableEditing = function() {
};
/**
 * 允许覆盖物在map.clearOverlays方法中被清除。(自 1.1 新增)
 *
 *
 * @since 1.1
 */
BMap.Polyline.prototype.enableMassClear = function() {
};
/**
 * 禁止覆盖物在map.clearOverlays方法中被清除。(自 1.1 新增)
 *
 *
 * @since 1.1
 */
BMap.Polyline.prototype.disableMassClear = function() {
};
/**
 * 修改指定位置的坐标。Number从0开始计数。例如setPointAt(2, point2a)代表将折线的第3个点，坐标设为point2a。(自 1.2
 * 废弃)
 *
 *
 * @param {Number}
 *            index
 * @param {BMap.Point}
 *            point
 * @deprecated 1.2
 */
BMap.Polyline.prototype.setPointAt = function(index, point) {
};
/**
 * 修改指定位置的坐标。索引index从0开始计数。例如setPointAt(2, point)代表将折线的第3个点的坐标设为point(自 1.2 新增)
 *
 *
 * @param {Number}
 *            index
 * @param {BMap.Point}
 *            point
 * @since 1.2
 */
BMap.Polyline.prototype.setPositionAt = function(index, point) {
};
/**
 * 返回覆盖物所在的map对象。（自1.2新增)
 *
 *
 * @return {BMap.Map}
 *
 */
BMap.Polyline.prototype.getMap = function() {
};
/**
 * 添加事件监听函数
 *
 *
 * @param {BMap.String}
 *            event
 * @param {Function}
 *            handler
 *
 */
BMap.Polyline.prototype.addEventListener = function(event, handler) {
};
/**
 * 移除事件监听函数
 *
 *
 * @param {BMap.String}
 *            event
 * @param {Function}
 *            handler
 *
 */
BMap.Polyline.prototype.removeEventListener = function(event, handler) {
};
/**
 * Circle类构造函数的可选参数（1.1 版本新增）。它没有构造函数，但可通过对象字面量形式表示。
 *
 *
 */
BMap.CircleOptions = function() {
};
/**
 *
 * 圆形边线颜色。
 *
 * @type {BMap.String}
 *
 */
BMap.CircleOptions.prototype.strokeColor = "";
/**
 *
 * 圆形填充颜色。当参数为空时，圆形将没有填充效果。
 *
 * @type {BMap.String}
 *
 */
BMap.CircleOptions.prototype.fillColor = "";
/**
 *
 * 圆形边线的宽度，以像素为单位。
 *
 * @type {Number}
 *
 */
BMap.CircleOptions.prototype.strokeWeight = 0;
/**
 *
 * 圆形边线透明度，取值范围0 - 1。
 *
 * @type {Number}
 *
 */
BMap.CircleOptions.prototype.strokeOpacity = 0;
/**
 *
 * 圆形填充的透明度，取值范围0 - 1。
 *
 * @type {Number}
 *
 */
BMap.CircleOptions.prototype.fillOpacity = 0;
/**
 *
 * 圆形边线的样式，solid或dashed。
 *
 * @type {BMap.String}
 *
 */
BMap.CircleOptions.prototype.strokeStyle = "";
/**
 *
 * 是否在调用map.clearOverlays清除此覆盖物，默认为true。
 *
 * @type {BMap.Boolean}
 *
 */
BMap.CircleOptions.prototype.enableMassClear = false;
/**
 *
 * 是否启用线编辑，默认为false。
 *
 * @type {BMap.Boolean}
 *
 */
BMap.CircleOptions.prototype.enableEditing = false;
/**
 *
 * 是否响应点击事件，默认为true。 (自 1.2 新增)
 *
 * @type {BMap.Boolean}
 * @since 1.2
 */
BMap.CircleOptions.prototype.enableClicking = false;
/**
 * 此类表示地图上一个图像标注。
 *
 * <p>
 * 创建一个图像标注实例。point参数指定了图像标注所在的地理位置。
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.Marker
 * @param {BMap.Point}
 *            point
 * @param {BMap.MarkerOptions}
 *            [opts]
 *
 */
BMap.Marker = function(point, opts) {
    /**
     * @event click 点击标注图标后会触发此事件。
     * @param {eventtype}
     * @param {target}
     *
     */
    /**
     * @event dblclick 双击标注图标后会触发此事件。
     * @param {eventtype}
     * @param {target}
     * @param {point}
     * @param {pixel}
     *
     */
    /**
     * @event mousedown 鼠标在标注图上按下触发此事件。
     * @param {eventtype}
     * @param {target}
     * @param {point}
     * @param {pixel}
     *
     */
    /**
     * @event mouseup 鼠标在标注图上释放触发此事件。
     * @param {eventtype}
     * @param {target}
     * @param {point}
     * @param {pixel}
     *
     */
    /**
     * @event mouseout 鼠标离开标注时触发此事件。
     * @param {eventtype}
     * @param {target}
     * @param {point}
     * @param {pixel}
     *
     */
    /**
     * @event mouseover 当鼠标进入标注图标区域时会触发此事件。
     * @param {eventtype}
     * @param {target}
     * @param {point}
     * @param {pixel}
     *
     */
    /**
     * @event remove 移除标注时触发。
     * @param {eventtype}
     * @param {target}
     *
     */
    /**
     * @event infowindowclose 信息窗在此标注上关闭时触发此事件。
     * @param {eventtype}
     * @param {target}
     *
     */
    /**
     * @event infowindowopen 信息窗在此标注上打开时触发此事件。
     * @param {eventtype}
     * @param {target}
     *
     */
    /**
     * @event dragstart 开始拖拽标注时触发此事件。 (自 1.1 新增)
     * @param {eventtype}
     * @param {target}
     * @since 1.1
     */
    /**
     * @event dragging 拖拽标注过程中触发此事件。 (自 1.1 新增)
     * @param {eventtype}
     * @param {target}
     * @param {pixel}
     * @param {point}
     * @since 1.1
     */
    /**
     * @event dragend 拖拽结束时触发此事件。 (自 1.1 新增)
     * @param {eventtype}
     * @param {target}
     * @param {pixel}
     * @param {point}
     * @since 1.1
     */
    /**
     * @event rightclick 右键点击标注时触发此事件。 (自 1.1 新增)
     * @param {eventtype}
     * @param {target}
     * @since 1.1
     */
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 打开信息窗。
 *
 *
 * @param {BMap.InfoWindow}
 *            infoWnd
 *
 */
BMap.Marker.prototype.openInfoWindow = function(infoWnd) {
};
/**
 * 关闭信息窗。
 *
 *
 *
 */
BMap.Marker.prototype.closeInfoWindow = function() {
};
/**
 * 设置标注所用的图标对象。
 *
 *
 * @param {BMap.Icon}
 *            icon
 *
 */
BMap.Marker.prototype.setIcon = function(icon) {
};
/**
 * 返回标注所用的图标对象。
 *
 *
 * @return {BMap.Icon}
 *
 */
BMap.Marker.prototype.getIcon = function() {
};
/**
 * 设置标注的地理坐标。 (自 1.2 废弃)
 *
 *
 * @param {BMap.Point}
 *            point
 * @deprecated 1.2
 */
BMap.Marker.prototype.setPoint = function(point) {
};
/**
 * 返回标注的地理坐标。 (自 1.2 废弃)
 *
 *
 * @return {BMap.Point}
 * @deprecated 1.2
 */
BMap.Marker.prototype.getPoint = function() {
};
/**
 * 设置标注的地理坐标。 (自 1.2 新增)
 *
 *
 * @param {BMap.Point}
 *            position
 * @since 1.2
 */
BMap.Marker.prototype.setPosition = function(position) {
};
/**
 * 返回标注的地理坐标。 (自 1.2 新增)
 *
 *
 * @return {BMap.Point}
 * @since 1.2
 */
BMap.Marker.prototype.getPosition = function() {
};
/**
 * 设置标注的偏移值。
 *
 *
 * @param {BMap.Size}
 *            offset
 *
 */
BMap.Marker.prototype.setOffset = function(offset) {
};
/**
 * 返回标注的偏移值。
 *
 *
 * @return {BMap.Size}
 *
 */
BMap.Marker.prototype.getOffset = function() {
};
/**
 * 返回标注的文本标注。
 *
 *
 * @return {BMap.Label}
 *
 */
BMap.Marker.prototype.getLabel = function() {
};
/**
 * 为标注添加文本标注。
 *
 *
 * @param {BMap.Label}
 *            label
 *
 */
BMap.Marker.prototype.setLabel = function(label) {
};
/**
 * 设置标注的标题，当鼠标移至标注上时显示此标题。
 *
 *
 * @param {BMap.String}
 *            title
 *
 */
BMap.Marker.prototype.setTitle = function(title) {
};
/**
 * 返回标注的标题。
 *
 *
 * @return {BMap.String}
 *
 */
BMap.Marker.prototype.getTitle = function() {
};
/**
 * 将标注置于其他标注之上。默认情况下，纬度较低的标注会覆盖在纬度较高的标注之上，从而形成一种立体效果。通过此方法可使某个标注覆盖在其他所有标注之上。注意：如果在多个标注对象上调用此方法，则这些标注依旧按照纬度产生默认的覆盖效果。
 *
 *
 * @param {BMap.Boolean}
 *            isTop
 *
 */
BMap.Marker.prototype.setTop = function(isTop) {
};
/**
 * 开启标注拖拽功能。 (自 1.1 新增)
 *
 *
 * @since 1.1
 */
BMap.Marker.prototype.enableDragging = function() {
};
/**
 * 关闭标注拖拽功能。 (自 1.1 新增)
 *
 *
 * @since 1.1
 */
BMap.Marker.prototype.disableDragging = function() {
};
/**
 * 允许覆盖物在map.clearOverlays方法中被清除。 (自 1.1 新增)
 *
 *
 * @since 1.1
 */
BMap.Marker.prototype.enableMassClear = function() {
};
/**
 * 禁止覆盖物在map.clearOverlays方法中被清除。 (自 1.1 新增)
 *
 *
 * @since 1.1
 */
BMap.Marker.prototype.disableMassClear = function() {
};
/**
 * 设置覆盖物的zIndex。 (自 1.1 新增)
 *
 *
 * @param {Number}
 *            zIndex
 * @since 1.1
 */
BMap.Marker.prototype.setZIndex = function(zIndex) {
};
/**
 * 返回覆盖物所在的map对象。 (自 1.2 新增)
 *
 *
 * @return {BMap.Map}
 * @since 1.2
 */
BMap.Marker.prototype.getMap = function() {
};
/**
 * 添加右键菜单。 (自 1.2 新增)
 *
 *
 * @param {BMap.ContextMenu}
 *            menu
 * @since 1.2
 */
BMap.Marker.prototype.addContextMenu = function(menu) {
};
/**
 * 移除右键菜单。 (自 1.2 新增)
 *
 *
 * @param {BMap.ContextMenu}
 *            menu
 * @since 1.2
 */
BMap.Marker.prototype.removeContextMenu = function(menu) {
};
/**
 * 设置标注动画效果。如果参数为null，则取消动画效果。该方法需要在addOverlay方法后设置。 (自 1.2 新增)
 *
 *
 * @param {BMap.Animation|Null}
 *            animation
 * @since 1.2
 */
BMap.Marker.prototype.setAnimation = function(animation) {
};
/**
 * 设置标注阴影图标。 (自 1.2 新增)
 *
 *
 * @param {BMap.Icon}
 *            shadow
 * @since 1.2
 */
BMap.Marker.prototype.setShadow = function(shadow) {
};
/**
 * 获取标注阴影图标。 (自 1.2 新增)
 *
 *
 * @return {BMap.Icon}
 * @since 1.2
 */
BMap.Marker.prototype.getShadow = function() {
};
/**
 * 添加事件监听函数
 *
 *
 * @param {BMap.String}
 *            event
 * @param {Function}
 *            handler
 *
 */
BMap.Marker.prototype.addEventListener = function(event, handler) {
};
/**
 * 移除事件监听函数
 *
 *
 * @param {BMap.String}
 *            event
 * @param {Function}
 *            handler
 *
 */
BMap.Marker.prototype.removeEventListener = function(event, handler) {
};
/**
 * 此类表示地图上包含信息的窗口。
 *
 * <p>
 * 创建一个信息窗实例，其中content支持HTML内容。1.2版本开始content参数支持传入DOM结点。
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.InfoWindow
 * @param {BMap.String|HTMLElement}
 *            content
 * @param {BMap.InfoWindowOptions}
 *            [opts]
 *
 */
BMap.InfoWindow = function(content, opts) {
    /**
     * @event close 信息窗口被关闭时触发此事件。
     * @param {eventtype}
     * @param {target}
     * @param {point}
     *
     */
    /**
     * @event open 信息窗口被打开时触发此事件。
     * @param {eventtype}
     * @param {target}
     * @param {point}
     *
     */
    /**
     * @event maximize 信息窗口最大化后触发此事件。(自 1.1 新增)
     * @param {eventtype}
     * @param {target}
     * @since 1.1
     */
    /**
     * @event restore 信息窗口还原时触发此事件。(自 1.1 新增)
     * @param {eventtype}
     * @param {target}
     * @since 1.1
     */
    /**
     * @event clickclose 点击信息窗口的关闭按钮时触发此事件。(自 1.1 新增)
     * @param {eventtype}
     * @param {target}
     * @since 1.1
     */
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 设置信息窗口的宽度，单位像素。取值范围：220 - 730。
 *
 *
 * @param {Number}
 *            width
 *
 */
BMap.InfoWindow.prototype.setWidth = function(width) {
};
/**
 * 设置信息窗口的高度，单位像素。取值范围：60 - 650。
 *
 *
 * @param {Number}
 *            height
 *
 */
BMap.InfoWindow.prototype.setHeight = function(height) {
};
/**
 * 重绘信息窗口，当信息窗口内容发生变化时进行调用。
 *
 *
 *
 */
BMap.InfoWindow.prototype.redraw = function() {
};
/**
 * 设置信息窗口标题。支持HTML内容。1.2版本开始title参数支持传入DOM结点。
 *
 *
 * @param {BMap.String|HTMLElement}
 *            title
 *
 */
BMap.InfoWindow.prototype.setTitle = function(title) {
};
/**
 * 返回信息窗口标题。 (自 1.2 新增)
 *
 *
 * @return {BMap.String|HTMLElement}
 * @since 1.2
 */
BMap.InfoWindow.prototype.getTitle = function() {
};
/**
 * 设置信息窗口内容。支持HTML内容。1.2版本开始content参数支持传入DOM结点。
 *
 *
 * @param {BMap.String|HTMLElement}
 *            content
 *
 */
BMap.InfoWindow.prototype.setContent = function(content) {
};
/**
 * 返回信息窗口内容。 (自 1.2 新增)
 *
 *
 * @return {BMap.String|HTMLElement}
 * @since 1.2
 */
BMap.InfoWindow.prototype.getContent = function() {
};
/**
 * 返回信息窗口的位置。
 *
 *
 * @return {BMap.Point}
 *
 */
BMap.InfoWindow.prototype.getPosition = function() {
};
/**
 * 启用窗口最大化功能。需要设置最大化后信息窗口里的内容，该接口才生效。
 *
 *
 *
 */
BMap.InfoWindow.prototype.enableMaximize = function() {
};
/**
 * 禁用窗口最大化功能。
 *
 *
 *
 */
BMap.InfoWindow.prototype.disableMaximize = function() {
};
/**
 * 返回信息窗口的打开状态。
 *
 *
 * @return {BMap.Boolean}
 *
 */
BMap.InfoWindow.prototype.isOpen = function() {
};
/**
 * 信息窗口最大化时所显示内容，支持HTML内容。
 *
 *
 * @param {BMap.String}
 *            content
 *
 */
BMap.InfoWindow.prototype.setMaxContent = function(content) {
};
/**
 * 最大化信息窗口(自 1.1 新增)
 *
 *
 * @since 1.1
 */
BMap.InfoWindow.prototype.maximize = function() {
};
/**
 * 还原信息窗口(自 1.1 新增)
 *
 *
 * @since 1.1
 */
BMap.InfoWindow.prototype.restore = function() {
};
/**
 * 开启打开信息窗口时地图自动平移。 (自 1.1 新增)
 *
 *
 * @since 1.1
 */
BMap.InfoWindow.prototype.enableAutoPan = function() {
};
/**
 * 关闭打开信息窗口时地图自动平移。 (自 1.1 新增)
 *
 *
 * @since 1.1
 */
BMap.InfoWindow.prototype.disableAutoPan = function() {
};
/**
 * 开启点击地图时关闭信息窗口。 (自 1.1 新增)
 *
 *
 * @since 1.1
 */
BMap.InfoWindow.prototype.enableCloseOnClick = function() {
};
/**
 * 关闭点击地图时关闭信息窗口。 (自 1.1 新增)
 *
 *
 * @since 1.1
 */
BMap.InfoWindow.prototype.disableCloseOnClick = function() {
};
/**
 * 添加事件监听函数
 *
 *
 * @param {BMap.String}
 *            event
 * @param {Function}
 *            handler
 *
 */
BMap.InfoWindow.prototype.addEventListener = function(event, handler) {
};
/**
 * 移除事件监听函数
 *
 *
 * @param {BMap.String}
 *            event
 * @param {Function}
 *            handler
 *
 */
BMap.InfoWindow.prototype.removeEventListener = function(event, handler) {
};
/**
 * 此类表示Polyline构造函数的可选参数。它没有构造函数，但可通过对象字面量形式表示。
 *
 *
 */
BMap.PolylineOptions = function() {
};
/**
 *
 * 折线颜色
 *
 * @type {BMap.String}
 *
 */
BMap.PolylineOptions.prototype.strokeColor = "";
/**
 *
 * 折线的宽度，以像素为单位。
 *
 * @type {Number}
 *
 */
BMap.PolylineOptions.prototype.strokeWeight = 0;
/**
 *
 * 折线的透明度，取值范围0 - 1。
 *
 * @type {Number}
 *
 */
BMap.PolylineOptions.prototype.strokeOpacity = 0;
/**
 *
 * 折线的样式，solid或dashed。
 *
 * @type {BMap.String}
 *
 */
BMap.PolylineOptions.prototype.strokeStyle = "";
/**
 *
 * 是否在调用map.clearOverlays清除此覆盖物，默认为true。 (自 1.1 新增)
 *
 * @type {BMap.Boolean}
 * @since 1.1
 */
BMap.PolylineOptions.prototype.enableMassClear = false;
/**
 *
 * 是否启用线编辑，默认为false。 (自 1.1 新增)
 *
 * @type {BMap.Boolean}
 * @since 1.1
 */
BMap.PolylineOptions.prototype.enableEditing = false;
/**
 *
 * 是否响应点击事件，默认为true。 (自 1.2 新增)
 *
 * @type {BMap.Boolean}
 * @since 1.2
 */
BMap.PolylineOptions.prototype.enableClicking = false;
/**
 * 此类表示地图上的一个热区。(自 1.2 新增)
 *
 * <p>
 * 创建Hotspot对象实例。(自 1.2 新增)
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.Hotspot
 * @param {BMap.Point}
 *            position
 * @param {BMap.HotspotOptions}
 *            [options]
 * @since 1.2
 */
BMap.Hotspot = function(position, options) {
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 获取热区位置坐标。(自 1.2 新增)
 *
 *
 * @return {BMap.Point}
 * @since 1.2
 */
BMap.Hotspot.prototype.getPosition = function() {
};
/**
 * 设置热区位置坐标。(自 1.2 新增)
 *
 *
 * @param {BMap.Point}
 *            position
 * @return {BMap.None}
 * @since 1.2
 */
BMap.Hotspot.prototype.setPosition = function(position) {
};
/**
 * 获取热区提示文本。(自 1.2 新增)
 *
 *
 * @return {BMap.String}
 * @since 1.2
 */
BMap.Hotspot.prototype.getText = function() {
};
/**
 * 设置热区提示文本。(自 1.2 新增)
 *
 *
 * @param {BMap.String}
 *            text
 * @return {BMap.None}
 * @since 1.2
 */
BMap.Hotspot.prototype.setText = function(text) {
};
/**
 * 获取热区对应的用户数据。(自 1.2 新增)
 *
 *
 * @return {BMap.Mix}
 * @since 1.2
 */
BMap.Hotspot.prototype.getUserData = function() {
};
/**
 * 设置热区对应的用户数据。(自 1.2 新增)
 *
 *
 * @param {BMap.Mix}
 *            data
 * @return {BMap.None}
 * @since 1.2
 */
BMap.Hotspot.prototype.setUserData = function(data) {
};
/**
 * 此类表示Marker构造函数的可选参数。它没有构造函数，但可通过对象字面量形式表示。
 *
 *
 */
BMap.MarkerOptions = function() {
};
/**
 *
 * 标注的位置偏移值。
 *
 * @type {BMap.Size}
 *
 */
BMap.MarkerOptions.prototype.offset = null;
/**
 *
 * 标注所用的图标对象。
 *
 * @type {BMap.Icon}
 *
 */
BMap.MarkerOptions.prototype.icon = null;
/**
 *
 * 是否在调用map.clearOverlays清除此覆盖物，默认为true。 (自 1.1 新增)
 *
 * @type {BMap.Boolean}
 * @since 1.1
 */
BMap.MarkerOptions.prototype.enableMassClear = false;
/**
 *
 * 是否启用拖拽，默认为false。 (自 1.1 新增)
 *
 * @type {BMap.Boolean}
 * @since 1.1
 */
BMap.MarkerOptions.prototype.enableDragging = false;
/**
 *
 * 是否响应点击事件。默认为true。 (自 1.2 新增)
 *
 * @type {BMap.Boolean}
 * @since 1.2
 */
BMap.MarkerOptions.prototype.enableClicking = false;
/**
 *
 * 拖拽标注时，标注是否开启离开地图表面效果。默认为false。 (自 1.2 新增)
 *
 * @type {BMap.Boolean}
 * @since 1.2
 */
BMap.MarkerOptions.prototype.raiseOnDrag = false;
/**
 *
 * 拖拽标注时的鼠标指针样式。此属性值需遵循CSS的cursor属性规范。 (自 1.2 新增)
 *
 * @type {BMap.String}
 * @since 1.2
 */
BMap.MarkerOptions.prototype.draggingCursor = "";
/**
 *
 * 阴影图标。 (自 1.2 新增)
 *
 * @type {BMap.Icon}
 * @since 1.2
 */
BMap.MarkerOptions.prototype.shadow = null;
/**
 *
 * 鼠标移到marker上的显示内容。
 *
 * @type {BMap.String}
 *
 */
BMap.MarkerOptions.prototype.title = "";
/**
 * 此类表示InfoWindow构造函数的可选参数，它没有构造函数，但可通过对象字面量形式表示。
 *
 *
 */
BMap.InfoWindowOptions = function() {
};
/**
 *
 * 信息窗宽度，单位像素。取值范围：0, 220 - 730。如果您指定宽度为0，则信息窗口的宽度将按照其内容自动调整。
 *
 * @type {Number}
 *
 */
BMap.InfoWindowOptions.prototype.width = 0;
/**
 *
 * 信息窗高度，单位像素。取值范围：0, 60 - 650。如果您指定高度为0，则信息窗口的高度将按照其内容自动调整。
 *
 * @type {Number}
 *
 */
BMap.InfoWindowOptions.prototype.height = 0;
/**
 *
 * 信息窗最大化时的宽度，单位像素。取值范围：220 - 730。
 *
 * @type {Number}
 *
 */
BMap.InfoWindowOptions.prototype.maxWidth = 0;
/**
 *
 * 信息窗位置偏移值。默认情况下在地图上打开的信息窗底端的尖角将指向其地理坐标，在标注上打开的信息窗底端尖角的位置取决于标注所用图标的infoWindowOffset属性值，您可以为信息窗添加偏移量来改变默认位置。
 *
 * @type {BMap.Size}
 *
 */
BMap.InfoWindowOptions.prototype.offset = null;
/**
 *
 * 信息窗标题文字，支持HTML内容。
 *
 * @type {BMap.String}
 *
 */
BMap.InfoWindowOptions.prototype.title = "";
/**
 *
 * 是否开启信息窗口打开时地图自动移动（默认开启）。(自 1.1 新增)
 *
 * @type {BMap.Boolean}
 * @since 1.1
 */
BMap.InfoWindowOptions.prototype.enableAutoPan = false;
/**
 *
 * 是否开启点击地图关闭信息窗口（默认开启）(自 1.1 新增)
 *
 * @type {BMap.Boolean}
 * @since 1.1
 */
BMap.InfoWindowOptions.prototype.enableCloseOnClick = false;
/**
 *
 * 是否在信息窗里显示短信发送按钮（默认开启）（自1.5新增）。
 *
 * @type {BMap.Boolean}
 *
 */
BMap.InfoWindowOptions.prototype.enableMessage = false;
/**
 *
 * 自定义部分的短信内容，可选项。完整的短信内容包括：自定义部分+位置链接，不设置时，显示默认短信内容。短信内容最长为140个字。（自1.5新增）
 *
 * @type {BMap.String}
 *
 */
BMap.InfoWindowOptions.prototype.message = "";
/**
 * 此类表示一个多边形覆盖物。
 *
 * <p>
 * 创建多边形覆盖物
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.Polygon
 * @param {BMap.Point[]}
 *            points
 * @param {BMap.PolygonOptions}
 *            [opts]
 *
 */
BMap.Polygon = function(points, opts) {
    /**
     * @event click 点击多边形后会触发此事件。
     * @param {eventtype}
     * @param {target}
     * @param {point}
     * @param {pixel}
     *
     */
    /**
     * @event dblclick 双击多边形后会触发此事件。
     * @param {eventtype}
     * @param {target}
     * @param {point}
     * @param {pixel}
     *
     */
    /**
     * @event mousedown 鼠标在多边形上按下触发此事件。
     * @param {eventtype}
     * @param {target}
     * @param {point}
     * @param {pixel}
     *
     */
    /**
     * @event mouseup 鼠标在多边形释放触发此事件。
     * @param {eventtype}
     * @param {target}
     * @param {point}
     * @param {pixel}
     *
     */
    /**
     * @event mouseout 鼠标离开多边形时触发此事件。
     * @param {eventtype}
     * @param {target}
     * @param {point}
     * @param {pixel}
     *
     */
    /**
     * @event mouseover 当鼠标进入多边形区域时会触发此事件。
     * @param {eventtype}
     * @param {target}
     * @param {point}
     * @param {pixel}
     *
     */
    /**
     * @event remove 移除多边形时触发。
     * @param {eventtype}
     * @param {target}
     *
     */
    /**
     * @event lineupdate 覆盖物的属性发生变化时触发。(自 1.1 新增)
     * @param {eventtype}
     * @param {target}
     * @since 1.1
     */
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 设置多边型的点数组。(自 1.2 废弃)
 *
 *
 * @param {BMap.Point[]}
 *            points
 * @deprecated 1.2
 */
BMap.Polygon.prototype.setPoints = function(points) {
};
/**
 * 返回多边型的点数组。(自 1.2 废弃)
 *
 *
 * @return {BMap.Point[]}
 * @deprecated 1.2
 */
BMap.Polygon.prototype.getPoints = function() {
};
/**
 * 设置多边型的点数组（自1.2新增）
 *
 *
 * @param {BMap.Point[]}
 *            path
 *
 */
BMap.Polygon.prototype.setPath = function(path) {
};
/**
 * 返回多边型的点数组（自1.2新增）
 *
 *
 * @return {BMap.Point[]}
 *
 */
BMap.Polygon.prototype.getPath = function() {
};
/**
 * 设置多边型的边线颜色，参数为合法的CSS颜色值。
 *
 *
 * @param {BMap.String}
 *            color
 *
 */
BMap.Polygon.prototype.setStrokeColor = function(color) {
};
/**
 * 返回多边型的边线颜色。
 *
 *
 * @return {BMap.String}
 *
 */
BMap.Polygon.prototype.getStrokeColor = function() {
};
/**
 * 设置多边形的填充颜色，参数为合法的CSS颜色值。当参数为空字符串时，折线覆盖物将没有填充效果。
 *
 *
 * @param {BMap.String}
 *            color
 *
 */
BMap.Polygon.prototype.setFillColor = function(color) {
};
/**
 * 返回多边形的填充颜色。
 *
 *
 * @return {BMap.String}
 *
 */
BMap.Polygon.prototype.getFillcolor = function() {
};
/**
 * 设置多边形的边线透明度，取值范围0 - 1。
 *
 *
 * @param {Number}
 *            opacity
 *
 */
BMap.Polygon.prototype.setStrokeOpacity = function(opacity) {
};
/**
 * 返回多边形的边线透明度。
 *
 *
 * @return {Number}
 *
 */
BMap.Polygon.prototype.getStrokeOpacity = function() {
};
/**
 * 设置多边形的填充透明度，取值范围0 - 1。
 *
 *
 * @param {Number}
 *            opacity
 *
 */
BMap.Polygon.prototype.setFillOpacity = function(opacity) {
};
/**
 * 返回多边形的填充透明度。
 *
 *
 * @return {Number}
 *
 */
BMap.Polygon.prototype.getFillOpacity = function() {
};
/**
 * 设置多边形边线的宽度，取值为大于等于1的整数。
 *
 *
 * @param {Number}
 *            weight
 *
 */
BMap.Polygon.prototype.setStrokeWeight = function(weight) {
};
/**
 * 返回多边形边线的宽度。
 *
 *
 * @return {Number}
 *
 */
BMap.Polygon.prototype.getStrokeWeight = function() {
};
/**
 * 设置多边形边线样式为实线或虚线，取值solid或dashed。
 *
 *
 * @param {BMap.String}
 *            style
 *
 */
BMap.Polygon.prototype.setStrokeStyle = function(style) {
};
/**
 * 返回多边形边线样式。
 *
 *
 * @return {BMap.String}
 *
 */
BMap.Polygon.prototype.getStrokeStyle = function() {
};
/**
 * 返回覆盖物的地理区域范围。(自 1.1 新增)
 *
 *
 * @return {BMap.Bounds}
 * @since 1.1
 */
BMap.Polygon.prototype.getBounds = function() {
};
/**
 * 开启编辑功能(自 1.1 新增)
 *
 *
 * @since 1.1
 */
BMap.Polygon.prototype.enableEditing = function() {
};
/**
 * 关闭编辑功能(自 1.1 新增)
 *
 *
 * @since 1.1
 */
BMap.Polygon.prototype.disableEditing = function() {
};
/**
 * 允许覆盖物在map.clearOverlays方法中被清除(自 1.1 新增)
 *
 *
 * @since 1.1
 */
BMap.Polygon.prototype.enableMassClear = function() {
};
/**
 * 禁止覆盖物在map.clearOverlays方法中被清除(自 1.1 新增)
 *
 *
 * @since 1.1
 */
BMap.Polygon.prototype.disableMassClear = function() {
};
/**
 * 修改指定位置的坐标。Number从0开始计数。例如setPointAt(2, point2a)代表将折线的第3个点，坐标设为point2a。(自 1.2
 * 废弃)
 *
 *
 * @param {Number}
 *            index
 * @param {BMap.Point}
 *            point
 * @deprecated 1.2
 */
BMap.Polygon.prototype.setPointAt = function(index, point) {
};
/**
 * 修改指定位置的坐标。索引index从0开始计数。例如setPointAt(2, point)代表将折线的第3个点的坐标设为point(自 1.2 新增)
 *
 *
 * @param {Number}
 *            index
 * @param {BMap.Point}
 *            point
 * @since 1.2
 */
BMap.Polygon.prototype.setPositionAt = function(index, point) {
};
/**
 * 返回覆盖物所在的map对象。（自1.2新增)
 *
 *
 * @return {BMap.Map}
 *
 */
BMap.Polygon.prototype.getMap = function() {
};
/**
 * 添加事件监听函数
 *
 *
 * @param {BMap.String}
 *            event
 * @param {Function}
 *            handler
 *
 */
BMap.Polygon.prototype.addEventListener = function(event, handler) {
};
/**
 * 移除事件监听函数
 *
 *
 * @param {BMap.String}
 *            event
 * @param {Function}
 *            handler
 *
 */
BMap.Polygon.prototype.removeEventListener = function(event, handler) {
};
/**
 * 此类是addHotspot方法的可选参数，没有构造函数，通过对象字面量形式表示。(自 1.2 新增)
 *
 *
 */
BMap.HotspotOptions = function() {
};
/**
 *
 * 当鼠标移至某一热区上时出现的文字提示。(自 1.2 新增)
 *
 * @type {BMap.String}
 * @since 1.2
 */
BMap.HotspotOptions.prototype.text = "";
/**
 *
 * 热区响应区域距中心点的扩展偏移值。数组的四个数值分别表示上、右、下、左距离中心点的扩展偏移量。默认偏移量为[5, 5, 5, 5]。(自 1.2 新增)
 *
 * @type {BMap.Array}
 * @since 1.2
 */
BMap.HotspotOptions.prototype.offsets = null;
/**
 *
 * 由用户填入的自定义数据。(自 1.2 新增)
 *
 * @type {BMap.Mix}
 * @since 1.2
 */
BMap.HotspotOptions.prototype.userData = null;
/**
 *
 * 热区生效的最小级别。(自 1.2 新增)
 *
 * @type {Number}
 * @since 1.2
 */
BMap.HotspotOptions.prototype.minZoom = 0;
/**
 *
 * 热区生效的最大级别。(自 1.2 新增)
 *
 * @type {Number}
 * @since 1.2
 */
BMap.HotspotOptions.prototype.maxZoom = 0;
/**
 *
 * 坠落动画。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_ANIMATION_DROP = 0;
/**
 *
 * 跳动动画。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_ANIMATION_BOUNCE = 0;
/**
 * 此类表示地图上的文本标注。
 *
 * <p>
 * 创建一个文本标注实例。point参数指定了文本标注所在的地理位置。
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.Label
 * @param {BMap.String}
 *            content
 * @param {BMap.LabelOptions}
 *            [opts]
 *
 */
BMap.Label = function(content, opts) {
    /**
     * @event click 点击文本标注后会触发此事件。
     * @param {eventtype}
     * @param {target}
     *
     */
    /**
     * @event dblclick 双击文本标注后会触发此事件。
     * @param {eventtype}
     * @param {target}
     *
     */
    /**
     * @event mousedown 鼠标在文本标注上按下触发此事件。
     * @param {eventtype}
     * @param {target}
     *
     */
    /**
     * @event mouseup 鼠标在文本标注释放触发此事件。
     * @param {eventtype}
     * @param {target}
     *
     */
    /**
     * @event mouseout 鼠标离开文本标注时触发此事件。
     * @param {eventtype}
     * @param {target}
     *
     */
    /**
     * @event mouseover 当鼠标进入文本标注区域时会触发此事件。
     * @param {eventtype}
     * @param {target}
     *
     */
    /**
     * @event remove 移除文本标注时触发。
     * @param {eventtype}
     * @param {target}
     *
     */
    /**
     * @event rightclick 右键点击标注时触发此事件。(自 1.1 新增)
     * @param {eventtype}
     * @param {target}
     * @since 1.1
     */
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 设置文本标注样式，该样式将作用于文本标注的容器元素上。其中styles为JavaScript对象常量，比如： setStyle({ color?:
 * "red", fontSize?: "12px" })
 * 注意：如果css的属性名中包含连字符，需要将连字符去掉并将其后的字母进行大写处理，例如：背景色属性要写成：backgroundColor。
 *
 *
 * @param {Object}
 *            styles
 *
 */
BMap.Label.prototype.setStyle = function(styles) {
};
/**
 * 设置文本标注的内容。支持HTML。
 *
 *
 * @param {BMap.String}
 *            content
 *
 */
BMap.Label.prototype.setContent = function(content) {
};
/**
 * 设置文本标注坐标。仅当通过Map.addOverlay()方法添加的文本标注有效。(自 1.2 废弃)
 *
 *
 * @param {BMap.Point}
 *            point
 * @deprecated 1.2
 */
BMap.Label.prototype.setPoint = function(point) {
};
/**
 * 返回文本标注坐标。(自 1.2 废弃)
 *
 *
 * @return {BMap.Point}
 * @deprecated 1.2
 */
BMap.Label.prototype.getPoint = function() {
};
/**
 * 设置文本标注坐标。仅当通过Map.addOverlay()方法添加的文本标注有效。(自 1.2 新增)
 *
 *
 * @param {BMap.Point}
 *            position
 * @since 1.2
 */
BMap.Label.prototype.setPosition = function(position) {
};
/**
 * 获取Marker的地理坐标.(自 1.2 新增)
 *
 *
 * @return {BMap.Point}
 * @since 1.2
 */
BMap.Label.prototype.getPosition = function() {
};
/**
 * 设置文本标注的偏移值。
 *
 *
 * @param {BMap.Size}
 *            offset
 *
 */
BMap.Label.prototype.setOffset = function(offset) {
};
/**
 * 返回文本标注的偏移值。
 *
 *
 * @return {BMap.Size}
 *
 */
BMap.Label.prototype.getOffset = function() {
};
/**
 * 设置文本标注的标题，当鼠标移至标注上时显示此标题。
 *
 *
 * @param {BMap.String}
 *            title
 *
 */
BMap.Label.prototype.setTitle = function(title) {
};
/**
 * 返回文本标注的标题。
 *
 *
 * @return {BMap.String}
 *
 */
BMap.Label.prototype.getTitle = function() {
};
/**
 * 允许覆盖物在map.clearOverlays方法中被清除。(自 1.1 新增)
 *
 *
 * @since 1.1
 */
BMap.Label.prototype.enableMassClear = function() {
};
/**
 * 禁止覆盖物在map.clearOverlays方法中被清除。(自 1.1 新增)
 *
 *
 * @since 1.1
 */
BMap.Label.prototype.disableMassClear = function() {
};
/**
 * 设置覆盖物的zIndex。(自 1.1 新增)
 *
 *
 * @param {Number}
 *            zIndex
 * @since 1.1
 */
BMap.Label.prototype.setZIndex = function(zIndex) {
};
/**
 * 设置地理坐标（自1.2新增）
 *
 *
 * @param {BMap.Point}
 *            position
 *
 */
BMap.Label.prototype.setPosition = function(position) {
};
/**
 * 返回地理坐标（自1.2新增）
 *
 *
 * @return {BMap.Point}
 *
 */
BMap.Label.prototype.getPosition = function() {
};
/**
 * 返回覆盖物所在的map对象。（自1.2新增)
 *
 *
 * @return {BMap.Map}
 *
 */
BMap.Label.prototype.getMap = function() {
};
/**
 * 添加事件监听函数
 *
 *
 * @param {BMap.String}
 *            event
 * @param {Function}
 *            handler
 *
 */
BMap.Label.prototype.addEventListener = function(event, handler) {
};
/**
 * 移除事件监听函数
 *
 *
 * @param {BMap.String}
 *            event
 * @param {Function}
 *            handler
 *
 */
BMap.Label.prototype.removeEventListener = function(event, handler) {
};
/**
 * 此类表示Polygon构造函数的可选参数。它没有构造函数，但可通过对象字面量形式表示。
 *
 *
 */
BMap.PolygonOptions = function() {
};
/**
 *
 * 边线颜色。
 *
 * @type {BMap.String}
 *
 */
BMap.PolygonOptions.prototype.strokeColor = "";
/**
 *
 * 填充颜色。当参数为空时，折线覆盖物将没有填充效果。
 *
 * @type {BMap.String}
 *
 */
BMap.PolygonOptions.prototype.fillColor = "";
/**
 *
 * 边线的宽度，以像素为单位。
 *
 * @type {Number}
 *
 */
BMap.PolygonOptions.prototype.strokeWeight = 0;
/**
 *
 * 边线透明度，取值范围0 - 1。
 *
 * @type {Number}
 *
 */
BMap.PolygonOptions.prototype.strokeOpacity = 0;
/**
 *
 * 填充的透明度，取值范围0 - 1。
 *
 * @type {Number}
 *
 */
BMap.PolygonOptions.prototype.fillOpacity = 0;
/**
 *
 * 边线的样式，solid或dashed。
 *
 * @type {BMap.String}
 *
 */
BMap.PolygonOptions.prototype.strokeStyle = "";
/**
 *
 * 是否在调用map.clearOverlays清除此覆盖物，默认为true。 (自 1.1 新增)
 *
 * @type {BMap.Boolean}
 * @since 1.1
 */
BMap.PolygonOptions.prototype.enableMassClear = false;
/**
 *
 * 是否启用线编辑，默认为false。 (自 1.1 新增)
 *
 * @type {BMap.Boolean}
 * @since 1.1
 */
BMap.PolygonOptions.prototype.enableEditing = false;
/**
 *
 * 是否响应点击事件，默认为true。 (自 1.2 新增)
 *
 * @type {BMap.Boolean}
 * @since 1.2
 */
BMap.PolygonOptions.prototype.enableClicking = false;
/**
 * 此类表示地图上所有覆盖物的容器集合，没有构造函数，通过对象字面量形式表示。通过Map的getPanes方法可获得该对象实例。
 *
 *
 */
BMap.MapPanes = function() {
};
/**
 *
 * 信息窗口所在的容器。
 *
 * @type {BMap.HTMLElement}
 *
 */
BMap.MapPanes.prototype.floatPane = null;
/**
 *
 * 标注点击区域所在的容器。
 *
 * @type {BMap.HTMLElement}
 *
 */
BMap.MapPanes.prototype.markerMouseTarget = null;
/**
 *
 * 信息窗口阴影所在的容器。
 *
 * @type {BMap.HTMLElement}
 *
 */
BMap.MapPanes.prototype.floatShadow = null;
/**
 *
 * 文本标注所在的容器。
 *
 * @type {BMap.HTMLElement}
 *
 */
BMap.MapPanes.prototype.labelPane = null;
/**
 *
 * 标注图标所在的容器。
 *
 * @type {BMap.HTMLElement}
 *
 */
BMap.MapPanes.prototype.markerPane = null;
/**
 *
 * 标注阴影所在的容器。
 *
 * @type {BMap.HTMLElement}
 *
 */
BMap.MapPanes.prototype.markerShadow = null;
/**
 *
 * 折线、多边形等矢量图形所在的容器。
 *
 * @type {BMap.HTMLElement}
 *
 */
BMap.MapPanes.prototype.mapPane = null;
/**
 * 此类表示标注覆盖物所使用的图标。
 *
 * <p>
 * 以给定的图像地址和大小创建图标对象实例。
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.Icon
 * @param {BMap.String}
 *            url
 * @param {BMap.Size}
 *            size
 * @param {BMap.IconOptions}
 *            [opts]
 *
 */
BMap.Icon = function(url, size, opts) {
};
/**
 *
 * 图标的定位点相对于图标左上角的偏移值。 (自1.2 废弃)请使用anchor
 *
 * @type {BMap.Size}
 * @deprecated 1.2
 */
BMap.Icon.prototype.offset = null;
/**
 *
 * 图标的定位点相对于图标左上角的偏移值。 (自1.2 新增)
 *
 * @type {BMap.Size}
 * @since 1.2
 */
BMap.Icon.prototype.anchor = null;
/**
 *
 * 图标可视区域的大小。
 *
 * @type {BMap.Size}
 *
 */
BMap.Icon.prototype.size = null;
/**
 *
 * 图标所用的图片相对于可视区域的偏移值，此功能的作用等同于CSS中的background-position属性。
 *
 * @type {BMap.Size}
 *
 */
BMap.Icon.prototype.imageOffset = null;
/**
 *
 * 图标所用的图片的大小，此功能的作用等同于CSS中的background-size属性。可用于实现高清屏的高清效果。 (自1.4 新增)
 *
 * @type {BMap.Size}
 * @since 1.4
 */
BMap.Icon.prototype.imageSize = null;
/**
 *
 * 图标所用图像资源的位置。
 *
 * @type {BMap.String}
 *
 */
BMap.Icon.prototype.imageUrl = "";
/**
 *
 * 信息窗口开启位置相对于图标左上角的偏移值。 (自1.2 废弃)
 *
 * @type {BMap.Size}
 * @deprecated 1.2
 */
BMap.Icon.prototype.infoWindowOffset = null;
/**
 *
 * 信息窗口开启位置相对于图标左上角的偏移值。 (自1.2 新增)
 *
 * @type {BMap.Size}
 * @since 1.2
 */
BMap.Icon.prototype.infoWindowAnchor = null;
/**
 *
 * 设置icon打印图片的url，该打印图片只针对IE6有效，解决IE6使用PNG滤镜导致的错位问题。如果您的icon没有使用PNG格式图片或者没有使用CSS
 * Sprites技术，则可忽略此配置。 (自1.1 新增)
 *
 * @type {BMap.String}
 * @since 1.1
 */
BMap.Icon.prototype.printImageUrl = "";
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 设置图片资源的地址。
 *
 *
 * @param {BMap.String}
 *            imageUrl
 *
 */
BMap.Icon.prototype.setImageUrl = function(imageUrl) {
};
/**
 * 设置图标可视区域的大小。
 *
 *
 * @param {BMap.Size}
 *            size
 *
 */
BMap.Icon.prototype.setSize = function(size) {
};
/**
 * 设置图标定位点相对于其左上角的偏移值。 (自 1.2 废弃)
 *
 *
 * @param {BMap.Size}
 *            offset
 * @deprecated 1.2
 */
BMap.Icon.prototype.setOffset = function(offset) {
};
/**
 * 设置图标的大小。 (自 1.4 新增)
 *
 *
 * @param {BMap.Size}
 *            offset
 * @since 1.4
 */
BMap.Icon.prototype.setImageSize = function(offset) {
};
/**
 * 设置图标定位点相对于其左上角的偏移值。 (自 1.2 新增)
 *
 *
 * @param {BMap.Size}
 *            anchor
 * @since 1.2
 */
BMap.Icon.prototype.setAnchor = function(anchor) {
};
/**
 * 设置图片相对于可视区域的偏移值。
 *
 *
 * @param {BMap.Size}
 *            offset
 *
 */
BMap.Icon.prototype.setImageOffset = function(offset) {
};
/**
 * 设置信息窗口开启位置相对于图标左上角的偏移值。 (自 1.2 废弃)
 *
 *
 * @param {BMap.Size}
 *            offset
 * @deprecated 1.2
 */
BMap.Icon.prototype.setInfoWindowOffset = function(offset) {
};
/**
 * 设置信息窗口开启位置相对于图标左上角的偏移值。 (自 1.2 新增)
 *
 *
 * @param {BMap.Size}
 *            anchor
 * @since 1.2
 */
BMap.Icon.prototype.setInfoWindowAnchor = function(anchor) {
};
/**
 * 设置icon的打印图片，该打印图片只针对IE6有效，解决IE6使用PNG滤镜导致的错位问题。如果您的icon没有使用PNG格式图片或者没有使用CSS
 * Sprites技术，则可忽略此配置。 (自 1.1 新增)
 *
 *
 * @param {BMap.String}
 *            url
 * @since 1.1
 */
BMap.Icon.prototype.setPrintImageUrl = function(url) {
};
/**
 * 此类表示Lable构造函数的可选参数。它没有构造函数，但可通过对象字面量形式表示。
 *
 *
 */
BMap.LabelOptions = function() {
};
/**
 *
 * 文本标注的位置偏移值。
 *
 * @type {BMap.Size}
 *
 */
BMap.LabelOptions.prototype.offset = null;
/**
 *
 * 文本标注的坐标点。(自 1.2 废弃)
 *
 * @type {BMap.Point}
 * @deprecated 1.2
 */
BMap.LabelOptions.prototype.point = null;
/**
 *
 * 文本标注的地理位置。（自1.2新增）
 *
 * @type {BMap.Point}
 *
 */
BMap.LabelOptions.prototype.position = null;
/**
 *
 * 是否在调用map.clearOverlays清除此覆盖物，默认为true.(自 1.1 新增)
 *
 * @type {BMap.Boolean}
 * @since 1.1
 */
BMap.LabelOptions.prototype.enableMassClear = false;
/**
 * 此类表示地图上的圆覆盖物。
 *
 * <p>
 * 创建圆覆盖物。(自 1.1 新增)
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.Circle
 * @param {BMap.Point}
 *            center
 * @param {Number}
 *            radius
 * @param {BMap.CircleOptions}
 *            [opts]
 *
 */
BMap.Circle = function(center, radius, opts) {
    /**
     * @event click 鼠标点击圆形后会触发此事件。
     * @param {eventtype}
     * @param {target}
     * @param {point}
     * @param {pixel}
     *
     */
    /**
     * @event dblclick 鼠标双击圆形后会触发此事件。
     * @param {eventtype}
     * @param {target}
     * @param {point}
     * @param {pixel}
     *
     */
    /**
     * @event mousedown 鼠标在圆形上按下触发此事件。
     * @param {eventtype}
     * @param {target}
     * @param {point}
     * @param {pixel}
     *
     */
    /**
     * @event mouseup 鼠标在圆形释放触发此事件。
     * @param {eventtype}
     * @param {target}
     * @param {point}
     * @param {pixel}
     *
     */
    /**
     * @event mouseout 鼠标离开圆形时触发此事件。
     * @param {eventtype}
     * @param {target}
     * @param {point}
     * @param {pixel}
     *
     */
    /**
     * @event mouseover 当鼠标进入圆形区域时会触发此事件。
     * @param {eventtype}
     * @param {target}
     * @param {point}
     * @param {pixel}
     *
     */
    /**
     * @event remove 移除圆形时触发此事件。
     * @param {eventtype}
     * @param {target}
     *
     */
    /**
     * @event lineupdate 圆形覆盖物的属性发生变化时触发此事件。
     * @param {eventtype}
     * @param {target}
     *
     */
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 设置圆形的中心点坐标。
 *
 *
 * @param {BMap.Point}
 *            center
 *
 */
BMap.Circle.prototype.setCenter = function(center) {
};
/**
 * 返回圆形的中心点坐标。
 *
 *
 * @return {BMap.Point}
 *
 */
BMap.Circle.prototype.getCenter = function() {
};
/**
 * 设置圆形的半径，单位为米。
 *
 *
 * @param {Number}
 *            radius
 *
 */
BMap.Circle.prototype.setRadius = function(radius) {
};
/**
 * 返回圆形的半径，单位为米。
 *
 *
 * @return {Number}
 *
 */
BMap.Circle.prototype.getRadius = function() {
};
/**
 * 返回圆形的地理区域范围。
 *
 *
 * @return {BMap.Bounds}
 *
 */
BMap.Circle.prototype.getBounds = function() {
};
/**
 * 设置圆形的边线颜色，参数为合法的CSS颜色值。
 *
 *
 * @param {BMap.String}
 *            color
 *
 */
BMap.Circle.prototype.setStrokeColor = function(color) {
};
/**
 * 返回圆形的边线颜色。
 *
 *
 * @return {BMap.String}
 *
 */
BMap.Circle.prototype.getStrokeColor = function() {
};
/**
 * 设置圆形的填充颜色，参数为合法的CSS颜色值。当参数为空字符串时，圆形覆盖物将没有填充效果。
 *
 *
 * @param {BMap.String}
 *            color
 *
 */
BMap.Circle.prototype.setFillColor = function(color) {
};
/**
 * 返回圆形的填充颜色。
 *
 *
 * @return {BMap.String}
 *
 */
BMap.Circle.prototype.getFillColor = function() {
};
/**
 * 设置圆形的边线透明度，取值范围0 - 1。
 *
 *
 * @param {Number}
 *            opacity
 *
 */
BMap.Circle.prototype.setStrokeOpacity = function(opacity) {
};
/**
 * 返回圆形的边线透明度。
 *
 *
 * @return {Number}
 *
 */
BMap.Circle.prototype.getStrokeOpacity = function() {
};
/**
 * 设置圆形的填充透明度，取值范围0 - 1。
 *
 *
 * @param {Number}
 *            opacity
 *
 */
BMap.Circle.prototype.setFillOpacity = function(opacity) {
};
/**
 * 返回圆形的填充透明度。
 *
 *
 * @return {Number}
 *
 */
BMap.Circle.prototype.getFillOpacity = function() {
};
/**
 * 设置圆形边线的宽度，取值为大于等于1的整数。
 *
 *
 * @param {Number}
 *            weight
 *
 */
BMap.Circle.prototype.setStrokeWeight = function(weight) {
};
/**
 * 返回圆形边线的宽度。
 *
 *
 * @return {Number}
 *
 */
BMap.Circle.prototype.getStrokeWeight = function() {
};
/**
 * 设置圆形边线样式为实线或虚线，取值solid或dashed。
 *
 *
 * @param {BMap.String}
 *            style
 *
 */
BMap.Circle.prototype.setStrokeStyle = function(style) {
};
/**
 * 返回圆形边线样式。
 *
 *
 * @return {BMap.String}
 *
 */
BMap.Circle.prototype.getStrokeStyle = function() {
};
/**
 * 开启编辑功能
 *
 *
 *
 */
BMap.Circle.prototype.enableEditing = function() {
};
/**
 * 关闭编辑功能
 *
 *
 *
 */
BMap.Circle.prototype.disableEditing = function() {
};
/**
 * 允许覆盖物在map.clearOverlays方法中被清除
 *
 *
 *
 */
BMap.Circle.prototype.enableMassClear = function() {
};
/**
 * 禁止覆盖物在map.clearOverlays方法中被清除
 *
 *
 *
 */
BMap.Circle.prototype.disableMassClear = function() {
};
/**
 * 返回覆盖物所在的map对象。（自1.2新增)
 *
 *
 * @return {BMap.Map}
 *
 */
BMap.Circle.prototype.getMap = function() {
};
/**
 * 添加事件监听函数
 *
 *
 * @param {BMap.String}
 *            event
 * @param {Function}
 *            handler
 *
 */
BMap.Circle.prototype.addEventListener = function(event, handler) {
};
/**
 * 移除事件监听函数
 *
 *
 * @param {BMap.String}
 *            event
 * @param {Function}
 *            handler
 *
 */
BMap.Circle.prototype.removeEventListener = function(event, handler) {
};
/**
 * 此类表示标注工具。此工具用来让使用者在地图上标注一个位置，可以通过该工具提供的事件来获得标注的位置。
 *
 * <p>
 * 创建一个标注控件项。
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.PushpinTool
 * @param {BMap.Map}
 *            map
 * @param {BMap.PushpinToolOptions}
 *            [opts] *
 * @deprecated 1.2
 *
 */
BMap.PushpinTool = function(map, opts) {
    /**
     * @event markend 在用户每完成一次标注时触发此事件。
     * @param {eventtype}
     * @param {target}
     * @param {marker}
     *
     */
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 开启标注工具。返回值如果为false，则表明开启失败，可能有其他工具正处于开启状态。请先关闭其他工具再进行开启。
 *
 *
 * @return {BMap.Boolean}
 *
 */
BMap.PushpinTool.prototype.open = function() {
};
/**
 * 关闭标注工具。
 *
 *
 *
 */
BMap.PushpinTool.prototype.close = function() {
};
/**
 * 设置标注的图标。
 *
 *
 * @param {BMap.Icon}
 *            icon
 * @return {BMap.Icon}
 *
 */
BMap.PushpinTool.prototype.setIcon = function(icon) {
};
/**
 * 返回标注的图标。
 *
 *
 * @return {BMap.Icon}
 *
 */
BMap.PushpinTool.prototype.getIcon = function() {
};
/**
 * 设置标注的鼠标样式。
 *
 *
 * @param {BMap.String}
 *            cursor
 * @return {BMap.String}
 *
 */
BMap.PushpinTool.prototype.setCursor = function(cursor) {
};
/**
 * 返回标注的鼠标样式。
 *
 *
 * @return {BMap.String}
 *
 */
BMap.PushpinTool.prototype.getCursor = function() {
};
/**
 * 返回类型描述字符串。
 *
 *
 * @return {BMap.String}
 *
 */
BMap.PushpinTool.prototype.toString = function() {
};
/**
 * 此类表示PushpinTool构造函数的可选参数。它没有构造函数，但可通过对象字面量形式表示。
 *
 *
 */
BMap.PushpinToolOptions = function() {
};
/**
 *
 * 标注所使用的图标。
 *
 * @type {BMap.Icon}
 *
 */
BMap.PushpinToolOptions.prototype.icon = null;
/**
 *
 * 标注所使用的鼠标样式。
 *
 * @type {BMap.String}
 *
 */
BMap.PushpinToolOptions.prototype.cursor = "";
/**
 *
 * 跟随鼠标移动的说明文字，默认为空。
 *
 * @type {BMap.String}
 *
 */
BMap.PushpinToolOptions.prototype.followText = "";
/**
 * 此类表示测距工具，用来让用户在地图上绘制折线并测定折线的距离。
 *
 * <p>
 * 创建一个绘制测距工具实例。
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.DistanceTool
 * @param {BMap.Map}
 *            map *
 * @deprecated 1.2
 *
 */
BMap.DistanceTool = function(map) {
    /**
     * @event drawend 用户每次完成一次测距操作时触发此事件。
     * @param {eventtype}
     * @param {target}
     * @param {points}
     * @param {polylines}
     * @param {distance}
     *
     */
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 开启测距工具。返回值如果为false，则表明开启失败，可能有其他工具正处于开启状态。请先关闭其他工具再进行开启。
 *
 *
 * @return {BMap.Boolean}
 *
 */
BMap.DistanceTool.prototype.open = function() {
};
/**
 * 关闭测距工具。
 *
 *
 *
 */
BMap.DistanceTool.prototype.close = function() {
};
/**
 * 返回类型描述字符串。
 *
 *
 * @return {BMap.String}
 *
 */
BMap.DistanceTool.prototype.toString = function() {
};
/**
 * 此类表示区域缩放控件。通过用户绘制的矩形区域的大小来确定地图的放大或缩小级别。
 *
 * <p>
 * 创建一个区域缩放控件实例。
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.DragAndZoomTool
 * @param {BMap.Map}
 *            map
 * @param {BMap.DragAndZoomToolOptions}
 *            [opts] *
 * @deprecated 1.2
 *
 */
BMap.DragAndZoomTool = function(map, opts) {
    /**
     * @event drawend 用户每完成一次缩放动作时触发此事件。
     * @param {eventtype}
     * @param {target}
     * @param {bounds}
     *
     */
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 开启区域缩放工具。返回值如果为false，则表明开启失败，可能有其他工具正处于开启状态。请先关闭其他工具再进行开启。
 *
 *
 * @return {BMap.Boolean}
 *
 */
BMap.DragAndZoomTool.prototype.open = function() {
};
/**
 * 关闭区域缩放工具。
 *
 *
 *
 */
BMap.DragAndZoomTool.prototype.close = function() {
};
/**
 * 返回类型描述字符串。
 *
 *
 * @return {BMap.String}
 *
 */
BMap.DragAndZoomTool.prototype.toString = function() {
};
/**
 * 此类表示DragAndZoomTool构造函数的可选参数。它没有构造函数，但可通过对象字面量形式表示。
 *
 *
 */
BMap.DragAndZoomToolOptions = function() {
};
/**
 *
 * 表示用户绘制趋于结束后进行放大还是缩小操作，默认为放大。
 *
 * @type {enum ZoomType}
 *
 */
BMap.DragAndZoomToolOptions.prototype.zoomType = null;
/**
 *
 * 每次完成缩放操作后是否自动退出，默认不退出。
 *
 * @type {BMap.Boolean}
 *
 */
BMap.DragAndZoomToolOptions.prototype.autoClose = false;
/**
 *
 * 跟随鼠标移动的说明文字。
 *
 * @type {BMap.String}
 *
 */
BMap.DragAndZoomToolOptions.prototype.followText = "";
/**
 *
 * 区域放大。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_ZOOM_IN = 0;
/**
 *
 * 区域缩小。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_ZOOM_OUT = 0;
/**
 * 此类表示右键菜单。您可以在地图上添加自定义内容的右键菜单。
 *
 * <p>
 * 创建一个右键菜单实例。
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.ContextMenu
 *
 */
BMap.ContextMenu = function() {
    /**
     * @event open 右键菜单打开时触发，事件参数point和pixel分别表示菜单开启时的地理和像素坐标点。
     * @param {eventtype}
     * @param {target}
     * @param {point}
     * @param {pixel}
     *
     */
    /**
     * @event close 右键菜单关闭时触发，事件参数point和pixel分别表示菜单开启时的地理和像素坐标点。
     * @param {eventtype}
     * @param {target}
     * @param {point}
     * @param {pixel}
     *
     */
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 添加菜单项。
 *
 *
 * @param {BMap.MenuItem}
 *            item
 *
 */
BMap.ContextMenu.prototype.addItem = function(item) {
};
/**
 * 返回指定索引位置的菜单项，第一个菜单项的索引为0。
 *
 *
 * @param {Number}
 *            index
 * @return {BMap.MenuItem}
 *
 */
BMap.ContextMenu.prototype.getItem = function(index) {
};
/**
 * 移除菜单项。
 *
 *
 * @param {BMap.MenuItem}
 *            item
 *
 */
BMap.ContextMenu.prototype.removeItem = function(item) {
};
/**
 * 添加分隔符。
 *
 *
 *
 */
BMap.ContextMenu.prototype.addSeparator = function() {
};
/**
 * 移除指定索引位置的分隔符，第一个分隔符的索引为0。
 *
 *
 * @param {Number}
 *            index
 *
 */
BMap.ContextMenu.prototype.removeSeparator = function(index) {
};
/**
 * 此类表示一个菜单项
 *
 * <p>
 * 创建一个菜单项。当菜单项被点击时，系统将会以当前菜单弹出的地理坐标点作为参数调用回调函数callback。
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.MenuItem
 * @param {BMap.String}
 *            text
 * @param {Function}
 *            callback
 * @param {BMap.MenuItemOptions}
 *            [opts]
 *
 */
BMap.MenuItem = function(text, callback, opts) {
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 设置菜单项显示的文本。
 *
 *
 * @param {BMap.String}
 *            text
 *
 */
BMap.MenuItem.prototype.setText = function(text) {
};
/**
 * 启用菜单项。
 *
 *
 *
 */
BMap.MenuItem.prototype.enable = function() {
};
/**
 * 禁用菜单项。
 *
 *
 *
 */
BMap.MenuItem.prototype.disable = function() {
};
/**
 * 此类表示MenuItem构造函数的可选参数。它没有构造函数，但可通过对象字面量形式表示。
 *
 *
 */
BMap.MenuItemOptions = function() {
};
/**
 *
 * 指定此菜单项的宽度，菜单以最长的菜单项宽度为准。
 *
 * @type {Number}
 *
 */
BMap.MenuItemOptions.prototype.width = 0;
/**
 * 此类表示一种地图类型，您可以通过实例化此类自定义地图类型（自1.2新增）。
 *
 * <p>
 * 创建MapType对象实例。
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.MapType
 * @param {BMap.String}
 *            name
 * @param {BMap.TileLayer|TileLayer[]}
 *            layers
 * @param {BMap.MapTypeOptions}
 *            [options]
 *
 */
BMap.MapType = function(name, layers, options) {
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 返回地图类型名称。
 *
 *
 * @return {BMap.String}
 *
 */
BMap.MapType.prototype.getName = function() {
};
/**
 * 返回地图类型对应的图层。
 *
 *
 * @return {BMap.TileLayer}
 *
 */
BMap.MapType.prototype.getTileLayer = function() {
};
/**
 * 返回地图类型允许的最小级别。
 *
 *
 * @return {Number}
 *
 */
BMap.MapType.prototype.getMinZoom = function() {
};
/**
 * 返回地图类型允许的最大级别。
 *
 *
 * @return {Number}
 *
 */
BMap.MapType.prototype.getMaxZoom = function() {
};
/**
 * 返回地图类型所使用的投影实例。
 *
 *
 * @return {BMap.Projection}
 *
 */
BMap.MapType.prototype.getProjection = function() {
};
/**
 * 返回地图类型对应的前景色。
 *
 *
 * @return {BMap.String}
 *
 */
BMap.MapType.prototype.getTextColor = function() {
};
/**
 * 返回地图类型的提示说明，用于在地图类型控件中提示。
 *
 *
 * @return {BMap.String}
 *
 */
BMap.MapType.prototype.getTips = function() {
};
/**
 *
 * 此地图类型展示普通街道视图。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_NORMAL_MAP = 0;
/**
 *
 * 此地图类型展示透视图像视图。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_PERSPECTIVE_MAP = 0;
/**
 *
 * 此地图类型展示卫星视图。(自 1.2 新增)
 *
 * @constant
 * @type {Number}
 * @default
 * @since 1.2
 */
var BMAP_SATELLITE_MAP = 0;
/**
 *
 * 此地图类型展示卫星和路网的混合视图。(自 1.2 新增)
 *
 * @constant
 * @type {Number}
 * @default
 * @since 1.2
 */
var BMAP_HYBRID_MAP = 0;
/**
 * 此类是MapType构造函数的可选参数，不能实例化，通过对象字面量形式表示（自1.2新增）。
 *
 *
 */
BMap.MapTypeOptions = function() {
};
/**
 *
 * 该类型地图的最小级别。
 *
 * @type {Number}
 *
 */
BMap.MapTypeOptions.prototype.minZoom = 0;
/**
 *
 * 该类型地图的最大级别。
 *
 * @type {Number}
 *
 */
BMap.MapTypeOptions.prototype.maxZoom = 0;
/**
 *
 * 当没有图块时所显示的错误图片地址。默认为透明图。
 *
 * @type {BMap.String}
 *
 */
BMap.MapTypeOptions.prototype.errorImageUrl = "";
/**
 *
 * 地图类型对应的前景色。
 *
 * @type {Number}
 *
 */
BMap.MapTypeOptions.prototype.textColor = 0;
/**
 *
 * 提示说明信息，用于在地图类型控件中进行提示。
 *
 * @type {BMap.String}
 *
 */
BMap.MapTypeOptions.prototype.tips = "";
/**
 * 此类表示地图投影抽象基类，不可实例化，但可通过MapType的getProjection方法获得（自1.2新增）。
 *
 *
 */
BMap.Projection = function() {
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 抽象，根据球面坐标获得平面坐标。
 *
 *
 * @param {BMap.Point}
 *            lngLat
 * @return {BMap.Pixel}
 *
 */
BMap.Projection.prototype.lngLatToPoint = function(lngLat) {
};
/**
 * 抽象，根据平面坐标获得球面坐标。
 *
 *
 * @param {BMap.Pixel}
 *            point
 * @return {BMap.Point}
 *
 */
BMap.Projection.prototype.pointToLngLat = function(point) {
};
/**
 * 此类表示街道地图投影类，一般通过MapType的getProjection方法获得实例（自1.2新增）。
 *
 *
 */
BMap.MercatorProjection = function() {
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 根据球面坐标获得平面坐标。
 *
 *
 * @param {BMap.Point}
 *            lngLat
 * @return {BMap.Pixel}
 *
 */
BMap.MercatorProjection.prototype.lngLatToPoint = function(lngLat) {
};
/**
 * 根据平面坐标获得球面坐标。
 *
 *
 * @param {BMap.Pixel}
 *            point
 * @return {BMap.Point}
 *
 */
BMap.MercatorProjection.prototype.pointToLngLat = function(point) {
};
/**
 * 此类表示透视地图投影类，一般通过MapType的getProjection方法获得实例（自1.2新增）。
 *
 *
 */
BMap.PerspectiveProjection = function() {
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 根据球面坐标获得平面坐标。
 *
 *
 * @param {BMap.Point}
 *            lngLat
 * @return {BMap.Pixel}
 *
 */
BMap.PerspectiveProjection.prototype.lngLatToPoint = function(lngLat) {
};
/**
 * 根据平面坐标获得球面坐标。
 *
 *
 * @param {BMap.Pixel}
 *            point
 * @return {BMap.Point}
 *
 */
BMap.PerspectiveProjection.prototype.pointToLngLat = function(point) {
};
/**
 * 此类表示一个地图图层，您可以向地图中添加自定义图层。
 *
 * <p>
 * 创建一个地图图层实例。
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.TileLayer
 * @param {BMap.TileLayerOptions}
 *            opts
 *
 */
BMap.TileLayer = function(opts) {
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 抽象。向地图返回地图图块的网址，图块索引由tileCoord的x和y属性在指定的缩放级别zoom提供。如果您在TileLayerOptions中提供了tileUrlTemplate参数，则可不实现此接口。
 *
 *
 * @param {BMap.Pixel}
 *            tileCoord
 * @param {Number}
 *            zoom
 * @return {BMap.String}
 *
 */
BMap.TileLayer.prototype.getTileUrl = function(tileCoord, zoom) {
};
/**
 * 返回地图图层数据的版权对象。
 *
 *
 * @return {BMap.Copyright|Null}
 *
 */
BMap.TileLayer.prototype.getCopyright = function() {
};
/**
 * 如果图层所用的图片为PNG格式并且包含透明信息，则返回true。
 *
 *
 * @return {Number}
 *
 */
BMap.TileLayer.prototype.isTransparentPng = function() {
};
/**
 * 此类表示TileLayer构造函数的可选参数。
 *
 *
 */
BMap.TileLayerOptions = function() {
};
/**
 *
 * 是否使用了带有透明信息的PNG。由于IE6不支持PNG透明，因此需要特殊处理。
 *
 * @type {BMap.Boolean}
 *
 */
BMap.TileLayerOptions.prototype.transparentPng = false;
/**
 *
 * 指定图块网址模板，该模板可以针对每个图块请求而展开，以根据现有的图块坐标系引用唯一的图块。模板的格式应该为：http://yourhost/tile?x={X}&y={Y}&z={Z}.png
 * 其中X和Y分别指纬度和经度图块坐标，Z指缩放级别，比如： http://yourhost/tile?x=3&y=27&z=5.png
 * 如果您没有提供图块网址模板，您需要实现TileLayer.getTileUrl()抽象方法。
 *
 * @type {BMap.String}
 *
 */
BMap.TileLayerOptions.prototype.tileUrlTemplate = "";
/**
 *
 * 地图图层的版权信息。
 *
 * @type {BMap.Copyright}
 *
 */
BMap.TileLayerOptions.prototype.copyright = null;
/**
 *
 * 图层的zIndex。 (自 1.2 新增)
 *
 * @type {Number}
 * @since 1.2
 */
BMap.TileLayerOptions.prototype.zIndex = 0;
/**
 * 此类表示交通流量图层。
 *
 * <p>
 * 创建交通流量图层。参数：opts: TrafficLayerOptions，可选 options
 * 参数指定应作为对象常量传递。如果可选参数提供predictDate，则将显示预测流量。否则显示实时流量。(自 1.1 新增)
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.TrafficLayer
 * @param {BMap.TrafficLayerOptions}
 *            TrafficLayeropts
 *
 */
BMap.TrafficLayer = function(TrafficLayeropts) {
};
/**
 * 此类是TrafficLayer构造函数的可选参数，没有构造函数，不能实例化。
 *
 *
 */
BMap.TrafficLayerOptions = function() {
};
/**
 *
 * 预测日期。 (自 1.1 新增)
 *
 * @type {BMap.PredictDate}
 * @since 1.1
 */
BMap.TrafficLayerOptions.prototype.predictDate = null;
/**
 * 此类表示交通流量的预测日期，没有构造函数，通过对象字面量形式表示。
 *
 *
 */
BMap.PredictDate = function() {
};
/**
 *
 * 预测日期，取值1到7，表示周一到周日。
 *
 * @type {Number}
 *
 */
BMap.PredictDate.prototype.weekday = 0;
/**
 *
 * 预测小时数，取值0到23，表示当日的0点到23点。
 *
 * @type {Number}
 *
 */
BMap.PredictDate.prototype.hour = 0;
/**
 * CustomLayer是用户自定义底图层，现阶段主要为LBS云麻点功能展现服务。
 *
 * <p>
 * 创建自定义底图层。 databoxId ： 必选，数据集id CustomLayerOptions?: 可选参数，暂不开放
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.CustomLayer
 * @param {BMap.String}
 *            databoxId
 * @param {BMap.String}
 *            [CustomLayerOptions]
 *
 */
BMap.CustomLayer = function(databoxId, CustomLayerOptions) {
    /**
     * @event hotspotclick 点击热区触发。
     * @param {eventtype}
     * @param {target}
     * @param {custompoi}
     *
     */
};
/**
 * 此类表示点击麻点图返回的，没有构造函数，通过对象字面量形式表示。
 *
 *
 */
BMap.Custompoi = function() {
};
/**
 *
 * 返回数据的id
 *
 * @type {BMap.String}
 *
 */
BMap.Custompoi.prototype.poiId = "";
/**
 *
 * 数据集的id
 *
 * @type {BMap.String}
 *
 */
BMap.Custompoi.prototype.databoxId = "";
/**
 *
 * 结果的名称标题
 *
 * @type {BMap.String}
 *
 */
BMap.Custompoi.prototype.title = "";
/**
 *
 * 地址
 *
 * @type {BMap.String}
 *
 */
BMap.Custompoi.prototype.address = "";
/**
 *
 * 电话
 *
 * @type {BMap.String}
 *
 */
BMap.Custompoi.prototype.phoneNumber = "";
/**
 *
 * 邮政编码
 *
 * @type {BMap.String}
 *
 */
BMap.Custompoi.prototype.postcode = "";
/**
 *
 * 结果所在省的编码
 *
 * @type {Number}
 *
 */
BMap.Custompoi.prototype.provinceCode = 0;
/**
 *
 * 结果所在省的名称
 *
 * @type {BMap.String}
 *
 */
BMap.Custompoi.prototype.province = "";
/**
 *
 * 结果所在城市的编码
 *
 * @type {Number}
 *
 */
BMap.Custompoi.prototype.cityCode = 0;
/**
 *
 * 结果所在城市的名称
 *
 * @type {BMap.String}
 *
 */
BMap.Custompoi.prototype.city = "";
/**
 *
 * 结果所在区县的编码
 *
 * @type {Number}
 *
 */
BMap.Custompoi.prototype.districtCode = 0;
/**
 *
 * 结果所在区县的名称
 *
 * @type {BMap.String}
 *
 */
BMap.Custompoi.prototype.district = "";
/**
 *
 * 结果所在的地理位置
 *
 * @type {BMap.Point}
 *
 */
BMap.Custompoi.prototype.point = null;
/**
 *
 * 结果的筛选标签
 *
 * @type {BMap.Array <String[]}
 *
 */
BMap.Custompoi.prototype.tags = null;
/**
 *
 * 结果的类别id
 *
 * @type {Number}
 *
 */
BMap.Custompoi.prototype.typeId = 0;
/**
 *
 * 用户扩展数据，结构根据用户的自定义
 *
 * @type {BMap.Json}
 *
 */
BMap.Custompoi.prototype.extendedData = null;
/**
 * 用于位置检索、周边检索和范围检索。
 *
 * <p>
 * 创建一个搜索类实例，其中location表示检索区域，其类型可为地图实例、坐标点或城市名称的字符串。当参数为地图实例时，检索位置由当前地图中心点确定，且搜索结果的标注将自动加载到地图上，并支持调整地图视野层级；当参数为坐标时，检索位置由该点所在位置确定；当参数为城市名称时，检索会在该城市内进行。
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.LocalSearch
 * @param {BMap.Map|Point|String}
 *            location
 * @param {BMap.LocalSearchOptions}
 *            [opts]
 *
 */
BMap.LocalSearch = function(location, opts) {
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 根据检索词发起检索。当keyword为数组时将同时执行多关键字的查询，最多支持10个关键字，多关键字自 1.2
 * 版本支持。option:{forceLocal:Boolean, customData:CustomData}
 * forceLocal表示是否将搜索范围约束在当前城市，customData表示检索lbs云服务的数据
 *
 *
 * @param {BMap.String|String[]}
 *            keyword
 * @param {Object}
 *            [option]
 *
 */
BMap.LocalSearch.prototype.search = function(keyword, option) {
};
/**
 * 根据范围和检索词发起范围检索。当keyword为数组时将同时执行多关键字检索，最多支持10个关键字，多关键字自 1.2
 * 版本支持。option:{customData:CustomData} customData表示检索lbs云服务的数据
 *
 *
 * @param {BMap.String|String[]}
 *            keyword
 * @param {BMap.Bounds}
 *            bounds
 * @param {Object}
 *            [option]
 *
 */
BMap.LocalSearch.prototype.searchInBounds = function(keyword, bounds, option) {
};
/**
 * 根据中心点、半径与检索词发起周边检索。当keyword为数组时将同时执行多关键字的检索，最多支持10个关键字，多关键字自 1.2
 * 版本支持。当center为字符串时，半径参数将忽略。注意：Point类型的中心点自 1.1
 * 版本支持。option:{customData:CustomData} customData表示检索lbs云服务的数据
 *
 *
 * @param {BMap.String|String[]}
 *            keyword
 * @param {BMap.LocalResultPoi|String|Point}
 *            center
 * @param {Number}
 *            radius
 * @param {Object}
 *            [option]
 *
 */
BMap.LocalSearch.prototype.searchNearby = function(keyword, center, radius,
        option) {
};
/**
 * 返回最近一次检索的结果。如果是多关键字范围检索，则返回一个LocalResult的数组，数组中的结果顺序和范围检索中多关键字数组中顺序一致。注意多关键字查询自
 * 1.2 版本支持。
 *
 *
 * @return {BMap.LocalResult| LocalResult[]}
 *
 */
BMap.LocalSearch.prototype.getResults = function() {
};
/**
 * 清除最近一次检索的结果
 *
 *
 *
 */
BMap.LocalSearch.prototype.clearResults = function() {
};
/**
 * 检索特定页面的结果
 *
 *
 * @param {Number}
 *            page
 *
 */
BMap.LocalSearch.prototype.gotoPage = function(page) {
};
/**
 * 启用根据结果自动调整地图层级，当指定了搜索结果所展现的地图时有效。
 *
 *
 *
 */
BMap.LocalSearch.prototype.enableAutoViewport = function() {
};
/**
 * 禁用根据结果自动调整地图层级。
 *
 *
 *
 */
BMap.LocalSearch.prototype.disableAutoViewport = function() {
};
/**
 * 启用自动选择第一个检索结果。
 *
 *
 *
 */
BMap.LocalSearch.prototype.enableFirstResultSelection = function() {
};
/**
 * 禁用自动选择第一个检索结果。
 *
 *
 *
 */
BMap.LocalSearch.prototype.disableFirstResultSelection = function() {
};
/**
 * 设置检索范围，参数类型可以为地图实例、坐标点或字符串。例：setLocation("北京市")
 *
 *
 * @param {BMap.Map|Point|String}
 *            location
 *
 */
BMap.LocalSearch.prototype.setLocation = function(location) {
};
/**
 * 设置每页容量，取值范围：1 - 100，对于多关键字检索，每页容量表示每个关键字返回结果的数量（例如当用2个关键字检索时，实际结果数量范围为：2 -
 * 200）。此值只对下一次检索有效。
 *
 *
 *
 */
BMap.LocalSearch.prototype.setPageCapacity = function() {
};
/**
 * 返回每页容量，对于多关键字检索，返回每个关键字对应的页面容量。
 *
 *
 * @return {Number}
 *
 */
BMap.LocalSearch.prototype.getPageCapacity = function() {
};
/**
 * 设置检索结束后的回调函数。参数：results: LocalResult 或 Array<LocalResult>
 * 如果是多关键字检索，回调函数参数为LocalResult的数组，数组中的结果顺序和检索中多关键字数组中顺序一致。
 *
 *
 *
 */
BMap.LocalSearch.prototype.setSearchCompleteCallback = function() {
};
/**
 * 设置添加标注后的回调函数。参数： pois: Array<LocalResultPoi>，通过marker属性可得到其对应的标注。
 *
 *
 * @param {Function}
 *            callback
 *
 */
BMap.LocalSearch.prototype.setMarkersSetCallback = function(callback) {
};
/**
 * 设置标注气泡创建时的回调函数。参数： poi: LocalResultPoi，通过其marker属性可得到当前的标注。 html:
 * HTMLElement，气泡内的Dom元素
 *
 *
 * @param {Function}
 *            callback
 *
 */
BMap.LocalSearch.prototype.setInfoHtmlSetCallback = function(callback) {
};
/**
 * 设置结果列表创建后的回调函数。参数： container: HTMLElement，结果列表所用的HTML元素
 *
 *
 * @param {Function}
 *            callback
 *
 */
BMap.LocalSearch.prototype.setResultsHtmlSetCallback = function(callback) {
};
/**
 * 返回状态码。
 *
 *
 * @return {BMap.StatusCodes}
 *
 */
BMap.LocalSearch.prototype.getStatus = function() {
};
/**
 *
 * 公交车
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_LINE_TYPE_BUS = 0;
/**
 *
 * 地铁
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_LINE_TYPE_SUBWAY = 0;
/**
 *
 * 渡轮
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_LINE_TYPE_FERRY = 0;
/**
 * 此类表示路线导航的结果，没有构造函数，通过访问WalkingRoute.getResults()方法或WalkingRoute的onSearchComplete回调函数参数获得。
 *
 *
 */
BMap.WalkingRouteResult = function() {
};
/**
 *
 * 本次检索所在的城市
 *
 * @type {BMap.String}
 *
 */
BMap.WalkingRouteResult.prototype.city = "";
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 返回起点。
 *
 *
 * @return {BMap.LocalResultPoi}
 *
 */
BMap.WalkingRouteResult.prototype.getStart = function() {
};
/**
 * 返回终点。
 *
 *
 * @return {BMap.LocalResultPoi}
 *
 */
BMap.WalkingRouteResult.prototype.getEnd = function() {
};
/**
 * 返回方案个数。
 *
 *
 * @return {Number}
 *
 */
BMap.WalkingRouteResult.prototype.getNumPlans = function() {
};
/**
 * 返回索引指定的方案。索引0表示第一条方案。
 *
 *
 * @param {Number}
 *            i
 * @return {BMap.RoutePlan}
 *
 */
BMap.WalkingRouteResult.prototype.getPlan = function(i) {
};
/**
 * 公交路线搜索类。
 *
 * <p>
 * 创建公交线搜索类。其中location表示检索区域，其类型可为地图实例、坐标点或城市名称的字符串。当参数为地图实例时，检索位置由当前地图中心点确定；当参数为坐标时，检索位置由该点所在位置确定；当参数为城市名称时，检索会在该城市内进行。(自1.2新增)
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.BusLineSearch
 * @param {BMap.Map|Point|String}
 *            location
 * @param {BMap.BusLineSearchOptions}
 *            [options]
 *
 */
BMap.BusLineSearch = function(location, options) {
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 在用户配置的回调函数中返回公交列表结果，其类型为BusListResult(自 1.2 新增)
 *
 *
 * @param {BMap.String}
 *            keyword
 * @since 1.2
 */
BMap.BusLineSearch.prototype.getBusList = function(keyword) {
};
/**
 * 在用户配置的回调函数中返回该条线路的公交信息，其类型为BusLine类型(自 1.2 新增)
 *
 *
 * @param {BMap.BusListItem}
 *            busLstItem
 * @since 1.2
 */
BMap.BusLineSearch.prototype.getBusLine = function(busLstItem) {
};
/**
 * 清除本次公交线检索结果(自 1.2 新增)
 *
 *
 * @since 1.2
 */
BMap.BusLineSearch.prototype.clearResults = function() {
};
/**
 * 启用自动调整地图视野功能(自 1.2 新增)
 *
 *
 * @since 1.2
 */
BMap.BusLineSearch.prototype.enableAutoViewport = function() {
};
/**
 * 禁用自动调整地图视野功能(自 1.2 新增)
 *
 *
 * @since 1.2
 */
BMap.BusLineSearch.prototype.disableAutoViewport = function() {
};
/**
 * 设置检索范围，参数类型可以为地图实例、坐标点或字符串。例：setLocation("北京市")(自 1.2 新增)
 *
 *
 * @param {BMap.Map|Point|String}
 *            location
 * @since 1.2
 */
BMap.BusLineSearch.prototype.setLocation = function(location) {
};
/**
 * 返回状态码(自 1.2 新增)
 *
 *
 * @return {BMap.StatusCodes}
 * @since 1.2
 */
BMap.BusLineSearch.prototype.getStatus = function() {
};
/**
 * 返回类型说明(自 1.2 新增)
 *
 *
 * @return {BMap.String}
 * @since 1.2
 */
BMap.BusLineSearch.prototype.toString = function() {
};
/**
 * 设置公交列表查询后的回调函数。参数：rs: BusListResult类型(自 1.2 新增)
 *
 *
 * @param {Function}
 *            callback
 * @since 1.2
 */
BMap.BusLineSearch.prototype.setGetBusListCompleteCallback = function(callback) {
};
/**
 * 设置公交线路查询后的回调函数。参数：rs: BusLine类型(自 1.2 新增)
 *
 *
 * @param {Function}
 *            callback
 * @since 1.2
 */
BMap.BusLineSearch.prototype.setGetBusLineCompleteCallback = function(callback) {
};
/**
 * 公交列表结果页渲染后回调函数。参数：container: HTMLElement，结果列表所用的HTML元素(自 1.2 新增)
 *
 *
 * @param {Function}
 *            callback
 * @since 1.2
 */
BMap.BusLineSearch.prototype.setBusListHtmlSetCallback = function(callback) {
};
/**
 * 公交线路结果页渲染后回调函数。参数：container: HTMLElement，结果列表所用的HTML元素(自 1.2 新增)
 *
 *
 * @param {Function}
 *            callback
 * @since 1.2
 */
BMap.BusLineSearch.prototype.setBusLineHtmlSetCallback = function(callback) {
};
/**
 * 添加公交线时候回调函数。参数：ply:Polyline 公交线路几何对象(自 1.2 新增)
 *
 *
 * @param {Function}
 *            callback
 * @since 1.2
 */
BMap.BusLineSearch.prototype.setPolylinesSetCallback = function(callback) {
};
/**
 * 添加公交站点时候回调函数。参数：sts:Array<Marker>公交站坐标组成的Marker对象数组(自 1.2 新增)
 *
 *
 * @param {Function}
 *            callback
 * @since 1.2
 */
BMap.BusLineSearch.prototype.setMarkersSetCallback = function(callback) {
};
/**
 * 此类表示LocalSearch构造函数的可选参数。
 *
 *
 */
BMap.LocalSearchOptions = function() {
};
/**
 *
 * 结果呈现设置。
 *
 * @type {BMap.LocalRenderOptions}
 *
 */
BMap.LocalSearchOptions.prototype.renderOptions = null;
/**
 *
 * 标注添加完成后的回调函数。 参数： pois: Array<LocalResultPoi>，通过marker属性可得到其对应的标注。
 *
 * @type {Function}
 *
 */
BMap.LocalSearchOptions.prototype.onMarkersSet = null;
/**
 *
 * 标注气泡内容创建后的回调函数。 参数： poi: LocalResultPoi，通过其marker属性可得到当前的标注。 html:
 * HTMLElement，气泡内的Dom元素
 *
 * @type {Function}
 *
 */
BMap.LocalSearchOptions.prototype.onInfoHtmlSet = null;
/**
 *
 * 结果列表添加完成后的回调函数。 参数： container: HTMLElement，结果列表所用的HTML元素
 *
 * @type {Function}
 *
 */
BMap.LocalSearchOptions.prototype.onResultsHtmlSet = null;
/**
 *
 * 设置每页容量，取值范围：1 - 100，对于多关键字检索，容量表示每个关键字的数量，如果有2个关键字，则实际检索结果数量范围为：2 - 200。
 *
 * @type {Number}
 *
 */
BMap.LocalSearchOptions.prototype.pageCapacity = 0;
/**
 *
 * 检索完成后的回调函数。 参数：results: LocalResult或Array<LocalResult>
 * 如果是多关键字检索，回调函数参数返回一个LocalResult的数组，数组中的结果顺序和检索中多关键字数组中顺序一致。
 *
 * @type {Function}
 *
 */
BMap.LocalSearchOptions.prototype.onSearchComplete = null;
/**
 * 此类用于获取驾车路线规划方案。
 *
 * <p>
 * 创建一个驾车导航实例，其中location表示检索区域，类型可为地图实例、坐标点或城市名称的字符串。当参数为地图实例时，检索位置由地图当前的中心点确定；当参数为坐标时，检索位置由该点所在位置确定；当参数为城市名称时，检索会在该城市内进行。
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.DrivingRoute
 * @param {BMap.Map|Point|String}
 *            location
 * @param {BMap.DrivingRouteOptions}
 *            [opts]
 *
 */
BMap.DrivingRoute = function(location, opts) {
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 发起检索。 start: 起点，参数可以是关键字、坐标点（自1.1版本支持）和LocalSearchPoi实例。 end:
 * 终点，参数可以是关键字、坐标点（自1.1版本支持）或LocalSearchPoi实例。
 * option:{startCity:String,endCity:string}
 * startCity表示是驾车查询的起点城市，可以是城市名或者城市编码，endCity表示驾车查询的终点城市，可以是城市名或者城市编码。
 *
 *
 * @param {BMap.String|Point|LocalResultPoi}
 *            start
 * @param {BMap.String|Point|LocalResultPoi}
 *            end
 * @param {object}
 *            [options]
 *
 */
BMap.DrivingRoute.prototype.search = function(start, end, options) {
};
/**
 * 返回最近一次检索的结果
 *
 *
 * @return {BMap.DrivingRouteResult}
 *
 */
BMap.DrivingRoute.prototype.getResults = function() {
};
/**
 * 清除最近一次检索的结果
 *
 *
 *
 */
BMap.DrivingRoute.prototype.clearResults = function() {
};
/**
 * 启用自动调整地图层级，当指定了搜索结果所展现的地图时有效。
 *
 *
 *
 */
BMap.DrivingRoute.prototype.enableAutoViewport = function() {
};
/**
 * 禁用自动调整地图层级。
 *
 *
 *
 */
BMap.DrivingRoute.prototype.disableAutoViewport = function() {
};
/**
 * 设置检索范围，参数类型可以为地图实例、坐标点或字符串。例：setLocation("北京市")
 *
 *
 * @param {BMap.Map|Point|String}
 *            location
 *
 */
BMap.DrivingRoute.prototype.setLocation = function(location) {
};
/**
 * 设置路线规划策略，参数为策略常量
 *
 *
 * @param {BMap.DrivingPolicy}
 *            policy
 *
 */
BMap.DrivingRoute.prototype.setPolicy = function(policy) {
};
/**
 * 设置检索结束后的回调函数。 参数： results: DrivingRouteResult
 *
 *
 * @param {Function}
 *            callback
 *
 */
BMap.DrivingRoute.prototype.setSearchCompleteCallback = function(callback) {
};
/**
 * 设置添加标注后的回调函数。 参数： pois: Array<LocalResultPoi>，起点和目的地点数组，通过marker属性可得到其对应的标注
 *
 *
 * @param {Function}
 *            callback
 *
 */
BMap.DrivingRoute.prototype.setMarkersSetCallback = function(callback) {
};
/**
 * 设置气泡打开后的回调函数。 参数： poi: LocalResultPoi，通过marker属性可得到当前的标注。html:
 * HTMLElement，气泡内的DOM元素。
 *
 *
 * @param {Function}
 *            callback
 *
 */
BMap.DrivingRoute.prototype.setInfoHtmlSetCallback = function(callback) {
};
/**
 * 设置添加路线后的回调函数。 参数： routes: Array<Route>，驾车线路数组，通过Route.getPolyline()方法可得到对应的折线覆盖物。
 *
 *
 * @param {Function}
 *            callback
 *
 */
BMap.DrivingRoute.prototype.setPolylinesSetCallback = function(callback) {
};
/**
 * 设置结果列表创建后的回调函数。 参数： container: 结果列表所用的HTML元素。
 *
 *
 * @param {Function}
 *            callback
 *
 */
BMap.DrivingRoute.prototype.setResultsHtmlSetCallback = function(callback) {
};
/**
 * 返回状态码
 *
 *
 * @return {BMap.StatusCodes}
 *
 */
BMap.DrivingRoute.prototype.getStatus = function() {
};
/**
 * 返回类型说明
 *
 *
 * @return {BMap.String}
 *
 */
BMap.DrivingRoute.prototype.toString = function() {
};
/**
 * 类用于获取用户的地址解析。
 *
 * <p>
 * 创建一个地址解析器的实例
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.Geocoder
 *
 */
BMap.Geocoder = function() {
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 对指定的地址进行解析。如果地址定位成功，则以地址所在的坐标点Point为参数调用回调函数。否则，回调函数的参数为null。city为地址所在的城市名，例如“北京市”。
 *
 *
 * @param {BMap.String}
 *            address
 * @param {Function}
 *            callback
 * @param {BMap.String}
 *            city
 *
 */
BMap.Geocoder.prototype.getPoint = function(address, callback, city) {
};
/**
 * 对指定的坐标点进行反地址解析。如果解析成功，则回调函数的参数为GeocoderResult对象，否则回调函数的参数为null。(自 1.1 新增)
 *
 *
 * @param {BMap.Point}
 *            point
 * @param {function}
 *            callback
 * @param {BMap.LocationOptions}
 *            [options]
 * @since 1.1
 */
BMap.Geocoder.prototype.getLocation = function(point, callback, options) {
};
/**
 *
 *
 *
 */
BMap.BusLineSearchOptions = function() {
};
/**
 *
 * RenderOptions结果呈现设置。(自 1.2 新增)
 *
 * @type {BMap.RenderOptions}
 * @since 1.2
 */
BMap.BusLineSearchOptions.prototype.renderOptions = null;
/**
 *
 * 设置公交列表查询后的回调函数.参数：rs: BusListResult类型(自 1.2 新增)
 *
 * @type {Function}
 * @since 1.2
 */
BMap.BusLineSearchOptions.prototype.onGetBusListComplete = null;
/**
 *
 * 设置公交线路查询后的回调函数.参数：rs: BusLine类型(自 1.2 新增)
 *
 * @type {Function}
 * @since 1.2
 */
BMap.BusLineSearchOptions.prototype.onGetBusLineComplete = null;
/**
 *
 * 公交列表结果页渲染后回调函数.参数：container: HTMLElement，结果列表所用的HTML元素(自 1.2 新增)
 *
 * @type {Function}
 * @since 1.2
 */
BMap.BusLineSearchOptions.prototype.onBusListHtmlSet = null;
/**
 *
 * 公交线路结果页渲染后回调函数.参数：container: HTMLElement，结果列表所用的HTML元素(自 1.2 新增)
 *
 * @type {Function}
 * @since 1.2
 */
BMap.BusLineSearchOptions.prototype.onBusLineHtmlSet = null;
/**
 *
 * 添加公交线时候回调函数.参数：ply:Polyline 公交线路几何对象(自 1.2 新增)
 *
 * @type {Function}
 * @since 1.2
 */
BMap.BusLineSearchOptions.prototype.onPolylinesSet = null;
/**
 *
 * 添加公交站点时候回调函数.参数：sts:Array<Marker>公交站坐标组成的Marker对象数组(自 1.2 新增)
 *
 * @type {Function}
 * @since 1.2
 */
BMap.BusLineSearchOptions.prototype.onMarkersSet = null;
/**
 * 此类表示检索lbs云服务的数据。它没有构造函数，但可通过对象字面量形式表示。
 * 要检索lbs云服务的数据，需要在引用api的时候在参数后加上lbs云平台的key。 如<script
 * src="http://api.map.baidu.com/api?v=1.3&key=123456" type="text/javascript"></script>
 *
 *
 */
BMap.CustomData = function() {
};
/**
 *
 * 可在lbs云平台上查看自己的databoxId
 *
 * @type {Number}
 *
 */
BMap.CustomData.prototype.databoxId = 0;
/**
 * 此类表示DrivingRoute构造函数的可选参数。
 *
 *
 */
BMap.DrivingRouteOptions = function() {
};
/**
 *
 * 结果呈现设置。
 *
 * @type {BMap.RenderOptions}
 *
 */
BMap.DrivingRouteOptions.prototype.renderOptions = null;
/**
 *
 * 驾车策略
 *
 * @type {BMap.DrivingPolicy}
 *
 */
BMap.DrivingRouteOptions.prototype.policy = null;
/**
 *
 * 检索完成后的回调函数。参数： results: DrivingRouteResult
 *
 * @type {Function}
 *
 */
BMap.DrivingRouteOptions.prototype.onSearchComplete = null;
/**
 *
 * 标注添加完成后的回调函数。 参数： pois: Array<LocalResultPoi>，起点和目的地点数组，通过marker属性可得到其对应的标注。
 *
 * @type {Function}
 *
 */
BMap.DrivingRouteOptions.prototype.onMarkersSet = null;
/**
 *
 * 标注气泡内容创建后的回调函数。 参数： poi: LocalResultPoi，通过marker属性可得到当前的标注。html:
 * HTMLElement，气泡内的DOM元素。
 *
 * @type {Function}
 *
 */
BMap.DrivingRouteOptions.prototype.onInfoHtmlSet = null;
/**
 *
 * 折线添加完成后的回调函数。 参数： routes: Array<Route>，驾车线路数组，通过Route.getPolyline()方法可得到对应的折线覆盖物。
 *
 * @type {Function}
 *
 */
BMap.DrivingRouteOptions.prototype.onPolylinesSet = null;
/**
 *
 * 结果列表添加完成后的回调函数。 参数： container: 结果列表所用的HTML元素。
 *
 * @type {Function}
 *
 */
BMap.DrivingRouteOptions.prototype.onResultsHtmlSet = null;
/**
 * 此类表示Geocoder的地址解析结果。它在地址解析的回调函数的参数中返回，不可实例化。
 *
 *
 */
BMap.GeocoderResult = function() {
};
/**
 *
 * 坐标点。(自 1.1 新增)
 *
 * @type {BMap.Point}
 * @since 1.1
 */
BMap.GeocoderResult.prototype.point = null;
/**
 *
 * 地址描述。(自 1.1 新增)
 *
 * @type {BMap.String}
 * @since 1.1
 */
BMap.GeocoderResult.prototype.address = "";
/**
 *
 * 结构化的地址描述。(自 1.1 新增)
 *
 * @type {BMap.AddressComponent}
 * @since 1.1
 */
BMap.GeocoderResult.prototype.addressComponents = null;
/**
 *
 * 附近的POI点。(自 1.1 新增)
 *
 * @type {BMap.LocalResultPoi[]}
 * @since 1.1
 */
BMap.GeocoderResult.prototype.surroundingPois = null;
/**
 *
 * 商圈字段，代表此点所属的商圈 (自 1.1 新增)
 *
 * @type {BMap.String}
 * @since 1.1
 */
BMap.GeocoderResult.prototype.business = "";
/**
 *
 *
 *
 */
BMap.BusListResult = function() {
};
/**
 *
 * 本次检索关键字(自 1.2 新增)
 *
 * @type {BMap.String}
 * @since 1.2
 */
BMap.BusListResult.prototype.keyword = "";
/**
 *
 * 本次检索所在城市(自 1.2 新增)
 *
 * @type {BMap.String}
 * @since 1.2
 */
BMap.BusListResult.prototype.city = "";
/**
 *
 * 到百度地图查看url(自 1.2 新增)
 *
 * @type {BMap.String}
 * @since 1.2
 */
BMap.BusListResult.prototype.moreResultsUrl = "";
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 公交列表个数(自 1.2 新增)
 *
 *
 * @return {Number}
 * @since 1.2
 */
BMap.BusListResult.prototype.getNumBusList = function() {
};
/**
 * 获取某一个具体的公交列表中的对象。0表示上行，1表示下行。(自 1.2 新增)
 *
 *
 * @param {Number}
 *            i
 * @return {BMap.BusListItem}
 * @since 1.2
 */
BMap.BusListResult.prototype.getBusListItem = function(i) {
};
/**
 * 此类表示搜索结果呈现的配置。它没有构造函数，但可通过对象字面量形式表示。
 *
 *
 */
BMap.RenderOptions = function() {
};
/**
 *
 * 展现结果的地图实例。当指定此参数后，搜索结果的标注、线路等均会自动添加到此地图上。
 *
 * @type {BMap.Map}
 *
 */
BMap.RenderOptions.prototype.map = null;
/**
 *
 * 结果列表的HTML容器id或容器元素，提供此参数后，结果列表将在此容器中进行展示。此属性对LocalCity无效。
 *
 * @type {BMap.String|HTMLElement}
 *
 */
BMap.RenderOptions.prototype.panel = null;
/**
 *
 * 是否选择第一个检索结果。此属性仅对LocalSearch有效。
 *
 * @type {BMap.Boolean}
 *
 */
BMap.RenderOptions.prototype.selectFirstResult = false;
/**
 *
 * 检索结束后是否自动调整地图视野。此属性对LocalCity无效。
 *
 * @type {BMap.Boolean}
 *
 */
BMap.RenderOptions.prototype.autoViewport = false;
/**
 *
 * 驾车结果展现中点击列表后的展现策略。
 *
 * @type {BMap.HighlightModes}
 *
 */
BMap.RenderOptions.prototype.highlightMode = null;
/**
 *
 * 最少时间。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_DRIVING_POLICY_LEAST_TIME = 0;
/**
 *
 * 最短距离。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_DRIVING_POLICY_LEAST_DISTANCE = 0;
/**
 *
 * 避开高速。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_DRIVING_POLICY_AVOID_HIGHWAYS = 0;
/**
 * 此类表示地址解析结果的层次化地址信息，没有构造函数，通过对象字面量形式表示。
 *
 *
 */
BMap.AddressComponent = function() {
};
/**
 *
 * 门牌号码。
 *
 * @type {BMap.String}
 *
 */
BMap.AddressComponent.prototype.streetNumber = "";
/**
 *
 * 街道名称。
 *
 * @type {BMap.String}
 *
 */
BMap.AddressComponent.prototype.street = "";
/**
 *
 * 区县名称。
 *
 * @type {BMap.String}
 *
 */
BMap.AddressComponent.prototype.district = "";
/**
 *
 * 城市名称。
 *
 * @type {BMap.String}
 *
 */
BMap.AddressComponent.prototype.city = "";
/**
 *
 * 省份名称。
 *
 * @type {BMap.String}
 *
 */
BMap.AddressComponent.prototype.province = "";
/**
 * 表示公交线路结果的公交线，没有构造函数，通过检索回调函数获得。
 *
 *
 */
BMap.BusLine = function() {
};
/**
 *
 * 线路名称(自 1.2 新增)
 *
 * @type {BMap.String}
 * @since 1.2
 */
BMap.BusLine.prototype.name = "";
/**
 *
 * 首班车时间(自 1.2 新增)
 *
 * @type {BMap.String}
 * @since 1.2
 */
BMap.BusLine.prototype.startTime = "";
/**
 *
 * 末班车时间(自 1.2 新增)
 *
 * @type {BMap.String}
 * @since 1.2
 */
BMap.BusLine.prototype.endTime = "";
/**
 *
 * 公交线路所属公司(自 1.2 新增)
 *
 * @type {BMap.String}
 * @since 1.2
 */
BMap.BusLine.prototype.company = "";
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 获取公交站点个数(自 1.2 新增)
 *
 *
 * @return {Number}
 * @since 1.2
 */
BMap.BusLine.prototype.getNumBusStations = function() {
};
/**
 * 获取某一个具体的公交站对象(自 1.2 新增)
 *
 *
 * @param {Number}
 *            i
 * @return {BMap.BusStation}
 * @since 1.2
 */
BMap.BusLine.prototype.getBusStation = function(i) {
};
/**
 * 返回公交线地理坐标点数组。(自 1.2 新增)
 *
 *
 * @return {BMap.Point[]}
 * @since 1.2
 */
BMap.BusLine.prototype.getPath = function() {
};
/**
 * 获取公交线几何对象, 仅当结果自动添加到地图上时有效(自 1.2 新增)
 *
 *
 * @return {BMap.Polyline}
 * @since 1.2
 */
BMap.BusLine.prototype.getPolyline = function() {
};
/**
 * 类表示LocalSearch的检索结果，没有构造函数，通过LocalSearch.getResults()方法或LocalSearch的onSearchComplete回调函数的参数得到。
 *
 *
 */
BMap.LocalResult = function() {
};
/**
 *
 * 本次检索的关键词。
 *
 * @type {BMap.String}
 *
 */
BMap.LocalResult.prototype.keyword = "";
/**
 *
 * 周边检索的中心点（仅当周边检索时提供）。
 *
 * @type {BMap.LocalResultPoi}
 *
 */
BMap.LocalResult.prototype.center = null;
/**
 *
 * 周边检索的半径（仅当周边检索时提供）。
 *
 * @type {Number}
 *
 */
BMap.LocalResult.prototype.radius = 0;
/**
 *
 * 范围检索的地理区域（仅当范围检索时提供）。
 *
 * @type {BMap.Bounds}
 *
 */
BMap.LocalResult.prototype.bounds = null;
/**
 *
 * 本次检索所在的城市。
 *
 * @type {BMap.String}
 *
 */
BMap.LocalResult.prototype.city = "";
/**
 *
 * 更多结果的链接，到百度地图进行搜索。
 *
 * @type {BMap.String}
 *
 */
BMap.LocalResult.prototype.moreResultsUrl = "";
/**
 *
 * 本次检索所在的省份。 (自 1.2 新增)
 *
 * @type {BMap.String}
 * @since 1.2
 */
BMap.LocalResult.prototype.province = "";
/**
 *
 * 搜索建议列表。（当关键词是拼音或拼写错误时给出的搜索建议） (自 1.2 新增)
 *
 * @type {BMap.String[]}
 * @since 1.2
 */
BMap.LocalResult.prototype.suggestions = null;
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 返回索引指定的结果。索引0表示第1条结果
 *
 *
 * @param {Number}
 *            i
 * @return {BMap.LocalResultPoi}
 *
 */
BMap.LocalResult.prototype.getPoi = function(i) {
};
/**
 * 返回当前页的结果数
 *
 *
 * @return {Number}
 *
 */
BMap.LocalResult.prototype.getCurrentNumPois = function() {
};
/**
 * 返回总结果数
 *
 *
 * @return {Number}
 *
 */
BMap.LocalResult.prototype.getNumPois = function() {
};
/**
 * 返回总页数
 *
 *
 * @return {Number}
 *
 */
BMap.LocalResult.prototype.getNumPages = function() {
};
/**
 * 返回页数序号
 *
 *
 * @return {Number}
 *
 */
BMap.LocalResult.prototype.getPageIndex = function() {
};
/**
 * 返回城市列表。数组元素对象包含如下属性： city: String，城市名 numResults: Number，结果数
 *
 *
 * @return {BMap.Object[]}
 *
 */
BMap.LocalResult.prototype.getCityList = function() {
};
/**
 * 此类表示路线导航的结果，没有构造函数，通过DrivingRoute.getResults()方法或DrivingRoute的onSearchComplete回调函数参数获得。
 *
 *
 */
BMap.DrivingRouteResult = function() {
};
/**
 *
 * 驾车导航策略。
 *
 * @type {enum DrivingPolicy}
 *
 */
BMap.DrivingRouteResult.prototype.policy = null;
/**
 *
 * 本次检索所在的城市。
 *
 * @type {BMap.String}
 *
 */
BMap.DrivingRouteResult.prototype.city = "";
/**
 *
 * 更多结果的链接，到百度地图进行搜索。
 *
 * @type {BMap.String}
 *
 */
BMap.DrivingRouteResult.prototype.moreResultsUrl = "";
/**
 *
 * 出租车费用信息。注意，此属性在部分城市有效，无效时为null。(自 1.1 新增)
 *
 * @type {BMap.TaxiFare}
 * @since 1.1
 */
BMap.DrivingRouteResult.prototype.taxiFare = null;
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 返回起点。
 *
 *
 * @return {BMap.LocalResultPoi}
 *
 */
BMap.DrivingRouteResult.prototype.getStart = function() {
};
/**
 * 返回终点。
 *
 *
 * @return {BMap.LocalResultPoi}
 *
 */
BMap.DrivingRouteResult.prototype.getEnd = function() {
};
/**
 * 返回方案个数。
 *
 *
 * @return {Number}
 *
 */
BMap.DrivingRouteResult.prototype.getNumPlans = function() {
};
/**
 * 返回索引指定的方案。索引0表示第一条方案。
 *
 *
 * @param {Number}
 *            i
 * @return {BMap.RoutePlan}
 *
 */
BMap.DrivingRouteResult.prototype.getPlan = function(i) {
};
/**
 * 此类表示Geocoder的地址解析请求的可选参数。它不可实例化。
 *
 *
 */
BMap.LocationOptions = function() {
};
/**
 *
 * 附近POI所处于的最大半径，默认为100米。(自 1.1 新增)
 *
 * @type {Number}
 * @since 1.1
 */
BMap.LocationOptions.prototype.poiRadius = 0;
/**
 *
 * 返回的POI点个数，默认为10个。取值范围(自 1.1 新增)
 *
 * @type {Number}
 * @since 1.1
 */
BMap.LocationOptions.prototype.numPois = 0;
/**
 *
 *
 *
 */
BMap.BusListItem = function() {
};
/**
 *
 * 公交线名称(自 1.2 新增)
 *
 * @type {BMap.String}
 * @since 1.2
 */
BMap.BusListItem.prototype.name = "";
/**
 * 此类表示位置检索或路线规划的一个结果点，没有构造函数，可通过对象字面量形式表示。
 *
 *
 */
BMap.LocalResultPoi = function() {
};
/**
 *
 * 结果的名称标题。
 *
 * @type {BMap.String}
 *
 */
BMap.LocalResultPoi.prototype.title = "";
/**
 *
 * 该结果所在的地理位置。
 *
 * @type {BMap.Point}
 *
 */
BMap.LocalResultPoi.prototype.point = null;
/**
 *
 * 在百度地图中展示该结果点的详情信息链接。
 *
 * @type {BMap.String}
 *
 */
BMap.LocalResultPoi.prototype.url = "";
/**
 *
 * 地址（根据数据部分提供）。注：当结果点类型为公交站或地铁站时，地址信息为经过该站点的所有车次。
 *
 * @type {BMap.String}
 *
 */
BMap.LocalResultPoi.prototype.address = "";
/**
 *
 * 所在城市。
 *
 * @type {BMap.String}
 *
 */
BMap.LocalResultPoi.prototype.city = "";
/**
 *
 * 电话，根据数据部分提供。
 *
 * @type {BMap.String}
 *
 */
BMap.LocalResultPoi.prototype.phoneNumber = "";
/**
 *
 * 邮政编码，根据数据部分提供。
 *
 * @type {BMap.String}
 *
 */
BMap.LocalResultPoi.prototype.postcode = "";
/**
 *
 * 类型，根据数据部分提供。
 *
 * @type {enum PoiType}
 *
 */
BMap.LocalResultPoi.prototype.type = null;
/**
 *
 * 是否精确匹配。只适用LocalSearch的search方法检索的结果。 (自 1.2 新增)
 *
 * @type {BMap.Boolean}
 * @since 1.2
 */
BMap.LocalResultPoi.prototype.isAccurate = false;
/**
 *
 * 所在省份。 (自 1.2 新增)
 *
 * @type {BMap.String}
 * @since 1.2
 */
BMap.LocalResultPoi.prototype.province = "";
/**
 *
 * POI的标签，如商务大厦、餐馆等。目前只有LocalSearch的回调函数onSearchComplete(result)中的result和Geocoder.getLocation的回调函数的参数GeocoderResult.surroundingPois涉及的LocalResultPoi有tags字段。其他API涉及的LocalResultPoi没有该字段
 * (自 1.2 新增)
 *
 * @type {BMap.String[]}
 * @since 1.2
 */
BMap.LocalResultPoi.prototype.tags = null;
/**
 *
 * 在百度地图详情页面展示该结果点的链接。localsearch的结果中才有。 (自 1.5 新增)
 *
 * @type {BMap.String}
 * @since 1.5
 */
BMap.LocalResultPoi.prototype.detailUrl = "";
/**
 * 此类表示出租车费用信息，没有构造函数，通过对象字面量形式表示。
 *
 *
 */
BMap.TaxiFare = function() {
};
/**
 *
 * 白天费用。
 *
 * @type {BMap.TaxiFareDetail}
 *
 */
BMap.TaxiFare.prototype.day = null;
/**
 *
 * 夜间费用。注意，部分城市没有夜间费用，此时此属性为null，且同时白天费用表示全天费用。
 *
 * @type {BMap.TaxiFareDetail}
 *
 */
BMap.TaxiFare.prototype.night = null;
/**
 *
 * 出租车里程，单位为米。
 *
 * @type {Number}
 *
 */
BMap.TaxiFare.prototype.distance = 0;
/**
 *
 * 出租车备注信息。
 *
 * @type {BMap.String}
 *
 */
BMap.TaxiFare.prototype.remark = "";
/**
 * 此类用于获取用户所在的城市位置信息。(根据用户IP自动定位到城市)
 *
 * <p>
 * 创建一个获取本地城市位置的实例
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.LocalCity
 * @param {BMap.LocalCityOptions}
 *            opts
 *
 */
BMap.LocalCity = function(opts) {
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 当获取城市信息后，回调函数会被调用，其参数为类型为LocalCityResult对象。
 *
 *
 * @param {Function}
 *            callback
 *
 */
BMap.LocalCity.prototype.get = function(callback) {
};
/**
 *
 *
 *
 */
BMap.BusStation = function() {
};
/**
 *
 * 站点名称(自 1.2 新增)
 *
 * @type {BMap.String}
 * @since 1.2
 */
BMap.BusStation.prototype.name = "";
/**
 *
 * 站点坐标(自 1.2 新增)
 *
 * @type {BMap.Point}
 * @since 1.2
 */
BMap.BusStation.prototype.position = null;
/**
 *
 * 一般位置点
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_POI_TYPE_NORMAL = 0;
/**
 *
 * 公交车站位置点
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_POI_TYPE_BUSSTOP = 0;
/**
 *
 * 地铁车站位置点
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_POI_TYPE_SUBSTOP = 0;
/**
 * 此类表示出租车具体费用信息，没有构造函数，通过对象字面量形式表示。
 *
 *
 */
BMap.TaxiFareDetail = function() {
};
/**
 *
 * 出租车起步价。
 *
 * @type {Number}
 *
 */
BMap.TaxiFareDetail.prototype.initialFare = 0;
/**
 *
 * 出租车单价。
 *
 * @type {Number}
 *
 */
BMap.TaxiFareDetail.prototype.unitFare = 0;
/**
 *
 * 出租车费用总价。
 *
 * @type {Number}
 *
 */
BMap.TaxiFareDetail.prototype.totalFare = 0;
/**
 * 此类表示LocalCity的可选参数。它没有构造函数，但可通过对象字面量表示。
 *
 *
 */
BMap.LocalCityOptions = function() {
};
/**
 *
 * 结果呈现设置，当给定map参数时，改地图将自动将视野定位到当前城市。
 *
 * @type {BMap.RenderOptions}
 *
 */
BMap.LocalCityOptions.prototype.renderOptions = null;
/**
 * Autocomplete是结果提示、自动完成类。
 *
 * <p>
 * 创建自动完成的实例。
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.Autocomplete
 * @param {BMap.AutocompleteOptions}
 *            options
 *
 */
BMap.Autocomplete = function(options) {
    /**
     * @event onconfirm 回车选中某条记录后触发
     *
     * <pre>
     * item?: { index?: 1 //高亮的记录，所属返回结果的index ,value?: {}//结果数据，见AutocompleteResultPoi }
     * </pre>
     *
     * @param {type}
     * @param {target}
     * @param {item}
     *
     */
    /**
     * @event onhighlight 键盘或者鼠标移动，某条记录高亮之后，触发
     *
     * <pre>
     * fromitem: { //上一条记录的信息
     *  index?: 2 //高亮的记录，所属返回结果的index
     * ,value?: {}//结果数据，见AutocompleteResultPoi }
     * , toitem : {//当前记录的信息，与fromitem结构一致}
     * </pre>
     *
     * @param {type}
     * @param {target}
     * @param {fromitem}
     * @param {toitem}
     *
     */
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 显示提示列表
 *
 *
 *
 */
BMap.Autocomplete.prototype.show = function() {
};
/**
 * 隐藏提示列表
 *
 *
 *
 */
BMap.Autocomplete.prototype.hide = function() {
};
/**
 * 修改请求数据类型。types定义方法详见AutocompleteOptions.
 *
 *
 * @param {type
 *            <Array[]} types
 *
 */
BMap.Autocomplete.prototype.setTypes = function(types) {
};
/**
 * 设置检索区域
 *
 *
 * @param {BMap.String|Map|Point}
 *            location
 *
 */
BMap.Autocomplete.prototype.setLocation = function(location) {
};
/**
 * 发起某个关键字的提示请求，会引起onSearchComplete的回调
 *
 *
 * @param {string}
 *            keywords
 *
 */
BMap.Autocomplete.prototype.search = function(keywords) {
};
/**
 * 获取结果列表
 *
 *
 * @return {BMap.AutocompleteResult}
 *
 */
BMap.Autocomplete.prototype.getResults = function() {
};
/**
 * 设置绑定的input控件的值，且不会出现下拉列表。 (自 1.3 新增)
 *
 *
 * @param {string}
 *            keyword
 * @since 1.3
 */
BMap.Autocomplete.prototype.setInputValue = function(keyword) {
};
/**
 * 销毁自动完成对象。(自 1.3 新增)
 *
 *
 * @since 1.3
 */
BMap.Autocomplete.prototype.dispose = function() {
};
/**
 * 用于获取公交路线规划方案。
 *
 * <p>
 * 创建一个公交导航实例。location表示检索区域，类型可为地图实例、坐标点或城市名称的字符串。当参数为地图实例时，检索位置由当前地图中心点确定；当参数为坐标时，检索位置由该点所在位置确定；当参数为城市名称时，检索会在该城市内进行。
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.TransitRoute
 * @param {BMap.Map|Point|String}
 *            location
 * @param {BMap.TransitRouteOptions}
 *            [opts]
 *
 */
BMap.TransitRoute = function(location, opts) {
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 发起检索。 start: 起点，参数可以是关键字、坐标点（自1.1版本支持）或者LocalSearchPoi实例。 end:
 * 终点，参数可以是关键字、坐标点（自1.1版本支持）或者LocalSearchPoi实例。
 *
 *
 * @param {BMap.String|Point|LocalResultPoi}
 *            start
 * @param {BMap.String|Point|LocalResultPoi}
 *            end
 *
 */
BMap.TransitRoute.prototype.search = function(start, end) {
};
/**
 * 返回最近一次检索的结果
 *
 *
 * @return {BMap.TransitRouteResult}
 *
 */
BMap.TransitRoute.prototype.getResults = function() {
};
/**
 * 清除最近一次检索的结果
 *
 *
 *
 */
BMap.TransitRoute.prototype.clearResults = function() {
};
/**
 * 启用自动调整地图层级，当指定了搜索结果所展现的地图时有效。
 *
 *
 *
 */
BMap.TransitRoute.prototype.enableAutoViewport = function() {
};
/**
 * 禁用自动调整地图层级。
 *
 *
 *
 */
BMap.TransitRoute.prototype.disableAutoViewport = function() {
};
/**
 * 设置每页返回方案个数（1-5），默认为5
 *
 *
 * @param {Number}
 *            capacity
 *
 */
BMap.TransitRoute.prototype.setPageCapacity = function(capacity) {
};
/**
 * 设置检索范围，参数类型可以为地图实例、坐标点或字符串。例：setLocation("北京市")
 *
 *
 * @param {BMap.Map|Point|String}
 *            location
 *
 */
BMap.TransitRoute.prototype.setLocation = function(location) {
};
/**
 * 设置路线规划策略，参数为策略常量
 *
 *
 * @param {BMap.TransitPolicy}
 *            policy
 *
 */
BMap.TransitRoute.prototype.setPolicy = function(policy) {
};
/**
 * 设置检索结束后的回调函数。 参数： results: TransitRouteResult，公交导航结果
 *
 *
 * @param {Function}
 *            callback
 *
 */
BMap.TransitRoute.prototype.setSearchCompleteCallback = function(callback) {
};
/**
 * 设置添加标注后的回调函数。 参数： pois: Array<LocalResultPoi>，起点和目的地数组。 transfers: Array<LocalResultPoi>，公交车站数组。
 *
 *
 * @param {Function}
 *            callback
 *
 */
BMap.TransitRoute.prototype.setMarkersSetCallback = function(callback) {
};
/**
 * 设置气泡打开后的回调函数。 参数： poi: LocalResultPoi，表示当前气泡对应的点（可以是起点、终点或换乘车站） html:
 * HTMLElement，气泡内的DOM元素
 *
 *
 * @param {Function}
 *            callback
 *
 */
BMap.TransitRoute.prototype.setInfoHtmlSetCallback = function(callback) {
};
/**
 * 设置添加路线后的回调函数。 参数： lines: Array<Line>，公交线路数组。 routes: Array<Route>，步行线路数组，通过Route.getPolyline()方法可得到对应的折线覆盖物
 *
 *
 * @param {Function}
 *            callback
 *
 */
BMap.TransitRoute.prototype.setPolylinesSetCallback = function(callback) {
};
/**
 * 设置结果列表创建后的回调函数。 参数： container: 结果列表所用的HTML元素
 *
 *
 * @param {Function}
 *            callback
 *
 */
BMap.TransitRoute.prototype.setResultsHtmlSetCallback = function(callback) {
};
/**
 * 返回状态码
 *
 *
 * @return {BMap.StatusCodes}
 *
 */
BMap.TransitRoute.prototype.getStatus = function() {
};
/**
 * 返回类型说明
 *
 *
 * @return {BMap.String}
 *
 */
BMap.TransitRoute.prototype.toString = function() {
};
/**
 * 此类表示一条驾车或步行出行方案。它没有构造函数，可通过DrivingRouteResult.getPlan()方法或WalkingRouteResult类的getPlan()方法获得。
 *
 *
 */
BMap.RoutePlan = function() {
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 返回方案包含的线路的个数。
 *
 *
 * @return {Number}
 *
 */
BMap.RoutePlan.prototype.getNumRoutes = function() {
};
/**
 * 返回方案中索引指定的线路。索引0表示第一条线路。
 *
 *
 * @param {Number}
 *            i
 * @return {BMap.Route}
 *
 */
BMap.RoutePlan.prototype.getRoute = function(i) {
};
/**
 * 返回方案总距离。当format参数为true时，返回方案距离字符串（包含单位），当format为false时，仅返回数值（单位为米）信息。默认参数为true。
 *
 *
 * @param {BMap.Boolean}
 *            format
 * @return {BMap.String|Number}
 *
 */
BMap.RoutePlan.prototype.getDistance = function(format) {
};
/**
 * 返回方案总时间。当format参数为true时，返回描述时间的字符串（包含单位），当format为false时，仅返回数值（单位为秒）信息。默认参数为true。
 * (自 1.1 新增)
 *
 *
 * @param {BMap.Boolean}
 *            format
 * @return {BMap.String|Number}
 * @since 1.1
 */
BMap.RoutePlan.prototype.getDuration = function(format) {
};
/**
 * 获取通过拖拽方式产生的途径点。注意，这里的LocalResultPoi只有title和point属性。 (自 1.2 新增)
 *
 *
 * @return {BMap.LocalResultPoi[]}
 * @since 1.2
 */
BMap.RoutePlan.prototype.getDragPois = function() {
};
/**
 * 此类表示LocalCity的定位结果。
 *
 *
 */
BMap.LocalCityResult = function() {
};
/**
 *
 * 城市所在中心点。
 *
 * @type {BMap.LocalResultPoi}
 *
 */
BMap.LocalCityResult.prototype.center = null;
/**
 *
 * 展示当前城市的最佳地图级别，如果您在使用此对象时提供了map实例，则地图级别将根据您提供的地图大小进行调整。
 *
 * @type {Number}
 *
 */
BMap.LocalCityResult.prototype.level = 0;
/**
 *
 * 城市名称。
 *
 * @type {BMap.String}
 *
 */
BMap.LocalCityResult.prototype.name = "";
/**
 *
 *
 *
 */
BMap.AutocompleteOptions = function() {
};
/**
 *
 * 设定返回结果的所属范围。例如“北京市”。
 *
 * @type {BMap.String|Map|Point}
 *
 */
BMap.AutocompleteOptions.prototype.location = null;
/**
 *
 * 返回数据类型。两种设置方式，第一种为默认值（即设置值为空），将返回所有数据。如地图初始化为北京，在输入框中输入“小”，输入框下会出现包含“小”关键字的多种类型（如餐饮、地名等）的提示词条。第二种设置值为"city"，将返回省市区县乡镇街道地址类型数据。如地图初始化为北京，在输入框中输入“小”，输入框下会出现“小金县”的地点名称类的提示词条。
 *
 * @type {BMap.String[]}
 *
 */
BMap.AutocompleteOptions.prototype.types = null;
/**
 *
 * 在input框中输入字符后，发起列表检索，完成后的回调函数。 参数：AutocompleteResult
 *
 * @type {Function}
 *
 */
BMap.AutocompleteOptions.prototype.onSearchComplete = null;
/**
 *
 * 文本输入框元素或其id
 *
 * @type {BMap.String|HTMLElement}
 *
 */
BMap.AutocompleteOptions.prototype.input = null;
/**
 * 此类表示TransitRoute构造函数的可选参数。它没有构造函数，但可通过对象字面量形式表示。
 *
 *
 */
BMap.TransitRouteOptions = function() {
};
/**
 *
 * 搜索结果的呈现设置。
 *
 * @type {BMap.RenderOptions}
 *
 */
BMap.TransitRouteOptions.prototype.renderOptions = null;
/**
 *
 * 公交导航的策略参数。
 *
 * @type {BMap.TransitPolicy}
 *
 */
BMap.TransitRouteOptions.prototype.policy = null;
/**
 *
 * 返回方案的个数。
 *
 * @type {Number}
 *
 */
BMap.TransitRouteOptions.prototype.pageCapacity = 0;
/**
 *
 * 检索完成后的回调函数。参数：results: TransitRouteResult，公交导航结果
 *
 * @type {Function}
 *
 */
BMap.TransitRouteOptions.prototype.onSearchComplete = null;
/**
 *
 * 标注添加完成后的回调函数。参数：pois: Array<LocalResultPoi>，起点和目的地数组。transfers: Array<LocalResultPoi>，公交车站数组。
 *
 * @type {Function}
 *
 */
BMap.TransitRouteOptions.prototype.onMarkersSet = null;
/**
 *
 * 气泡内容创建后的回调函数。参数：poi: LocalResultPoi，表示当前气泡对应的点（可以是起点、终点或换乘车站）html:
 * HTMLElement，气泡内的DOM元素
 *
 * @type {Function}
 *
 */
BMap.TransitRouteOptions.prototype.onInfoHtmlSet = null;
/**
 *
 * 折线添加完成后的回调函数。参数：lines: Array<Line>，公交线路数组。routes: Array<Route>，步行线路数组，通过Route.getPolyline()方法可得到对应的折线覆盖物。
 *
 * @type {Function}
 *
 */
BMap.TransitRouteOptions.prototype.onPolylinesSet = null;
/**
 *
 * 结果列表添加完成后的回调函数。参数：container: 结果列表所用的HTML元素
 *
 * @type {Function}
 *
 */
BMap.TransitRouteOptions.prototype.onResultsHtmlSet = null;
/**
 * 此类表示一条驾车或步行路线。
 *
 *
 */
BMap.Route = function() {
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 返回路线包含的关键点个数。
 *
 *
 * @return {Number}
 *
 */
BMap.Route.prototype.getNumSteps = function() {
};
/**
 * 返回索引指定的关键点，驾车和步行适用。索引0表示第一个关键点。
 *
 *
 * @param {Number}
 *            i
 * @return {BMap.Step}
 *
 */
BMap.Route.prototype.getStep = function(i) {
};
/**
 * 返回路线距离，当format为false时仅返回数值。
 *
 *
 * @param {BMap.Boolean}
 *            format
 * @return {BMap.String|Number}
 *
 */
BMap.Route.prototype.getDistance = function(format) {
};
/**
 * 返回本路线在方案中的索引位置。
 *
 *
 * @return {Number}
 *
 */
BMap.Route.prototype.getIndex = function() {
};
/**
 * 返回路线对应的覆盖物，仅当结果自动添加到地图上时有效。
 *
 *
 * @return {BMap.Polyline}
 *
 */
BMap.Route.prototype.getPolyline = function() {
};
/**
 * 返回路线的地理坐标点数组。（自 1.2 废弃）
 *
 *
 * @return {BMap.Point[]}
 *
 */
BMap.Route.prototype.getPoints = function() {
};
/**
 * 返回路线的地理坐标点数组。（自 1.2 新增）
 *
 *
 * @return {BMap.Point[]}
 *
 */
BMap.Route.prototype.getPath = function() {
};
/**
 * 返回路线类型，可区分是驾车还是步行线路。
 *
 *
 * @return {BMap.RouteTypes}
 *
 */
BMap.Route.prototype.getRouteType = function() {
};
/**
 * 此类表示交通流量控件，它继承Control类，包含该类的所有方法。该控件的停靠位置常量仅支持BMAP_CONTROL_ANCHOR_TOP_RIGHT，但可修改其偏移位置（自
 * 1.2 废弃）。
 *
 * <p>
 * 创建一个交通流量的控件实例，该实例继承自Control，支持此类的所有方法。
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.TrafficControl
 *
 */
BMap.TrafficControl = function() {
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 设置路况面板的水平和竖直偏移位置。面板与按钮停靠位置一致，始终位于地图区域的右上角。
 *
 *
 * @param {BMap.Size}
 *            offset
 *
 */
BMap.TrafficControl.prototype.setPanelOffset = function(offset) {
};
/**
 * 显示实时路况。
 *
 *
 *
 */
BMap.TrafficControl.prototype.show = function() {
};
/**
 * 隐藏实时路况。
 *
 *
 *
 */
BMap.TrafficControl.prototype.hide = function() {
};
/**
 *
 *
 *
 */
BMap.AutocompleteResultPoi = function() {
};
/**
 *
 * 省名
 *
 * @type {BMap.String}
 *
 */
BMap.AutocompleteResultPoi.prototype.province = "";
/**
 *
 * 城市名
 *
 * @type {BMap.String}
 *
 */
BMap.AutocompleteResultPoi.prototype.City = "";
/**
 *
 * 区县名称
 *
 * @type {BMap.String}
 *
 */
BMap.AutocompleteResultPoi.prototype.district = "";
/**
 *
 * 街道名称
 *
 * @type {BMap.String}
 *
 */
BMap.AutocompleteResultPoi.prototype.street = "";
/**
 *
 * 门牌号码
 *
 * @type {BMap.String}
 *
 */
BMap.AutocompleteResultPoi.prototype.streetNumber = "";
/**
 *
 * 商户名
 *
 * @type {BMap.String}
 *
 */
BMap.AutocompleteResultPoi.prototype.business = "";
/**
 *
 * 最少时间。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_TRANSIT_POLICY_LEAST_TIME = 0;
/**
 *
 * 最少换乘。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_TRANSIT_POLICY_LEAST_TRANSFER = 0;
/**
 *
 * 最少步行。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_TRANSIT_POLICY_LEAST_WALKING = 0;
/**
 *
 * 不乘地铁。 (自 1.2 新增)
 *
 * @constant
 * @type {Number}
 * @default
 * @since 1.2
 */
var BMAP_TRANSIT_POLICY_AVOID_SUBWAYS = 0;
/**
 *
 * 驾车线路
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_ROUTE_TYPE_DRIVING = 0;
/**
 *
 * 步行线路
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_ROUTE_TYPE_WALKING = 0;
/**
 * 返回用户当前的位置。此方法利用浏览器的geolocation接口获取用户当前位置，不支持的浏览器将无法获取。(自 1.1 新增)
 *
 * <p>
 * 创建Geolocation对象实例。
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.Geolocation
 * @since 1.1
 */
BMap.Geolocation = function() {
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 返回用户当前位置。当定位成功时，回调函数的参数为GeolocationResult对象，否则为null。
 *
 *
 * @param {function}
 *            callback
 * @param {BMap.PositionOptions}
 *            [options]
 *
 */
BMap.Geolocation.prototype.getCurrentPosition = function(callback, options) {
};
/**
 * 返回状态码，当定位成功后，状态码为：BMAP_STATUS_SUCCESS，如果为其他状态码表示不能获取您当前的位置。
 *
 *
 * @return {BMap.StatusCode}
 *
 */
BMap.Geolocation.prototype.getStatus = function() {
};
/**
 *
 *
 *
 */
BMap.AutocompleteResult = function() {
};
/**
 *
 * 检索关键字
 *
 * @type {BMap.String}
 *
 */
BMap.AutocompleteResult.prototype.keyword = "";
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 结果数组
 *
 *
 * @param {Number}
 *            i
 * @return {BMap.AutocompleteResultPoi}
 *
 */
BMap.AutocompleteResult.prototype.getPoi = function(i) {
};
/**
 * 结果总数
 *
 *
 * @return {Number}
 *
 */
BMap.AutocompleteResult.prototype.getNumPois = function() {
};
/**
 * 此类表示路线导航的结果，没有构造函数，通过访问TransitRoute.getResults()方法或TransitRoute的onSearchComplete回调函数参数获得。
 *
 *
 */
BMap.TransitRouteResult = function() {
};
/**
 *
 * 公交导航策略
 *
 * @type {BMap.TransitPolices}
 *
 */
BMap.TransitRouteResult.prototype.policy = null;
/**
 *
 * 本次检索所在的城市
 *
 * @type {BMap.String}
 *
 */
BMap.TransitRouteResult.prototype.city = "";
/**
 *
 * 更多结果的链接，到百度地图进行搜索
 *
 * @type {BMap.String}
 *
 */
BMap.TransitRouteResult.prototype.moreResultsUrl = "";
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 返回起点
 *
 *
 * @return {BMap.LocalResultPoi}
 *
 */
BMap.TransitRouteResult.prototype.getStart = function() {
};
/**
 * 返回终点
 *
 *
 * @return {BMap.LocalResultPoi}
 *
 */
BMap.TransitRouteResult.prototype.getEnd = function() {
};
/**
 * 返回方案个数
 *
 *
 * @return {Number}
 *
 */
BMap.TransitRouteResult.prototype.getNumPlans = function() {
};
/**
 * 返回索引指定的方案。索引0表示第一条方案。
 *
 *
 * @param {Number}
 *            i
 * @return {BMap.TransitRoutePlan}
 *
 */
BMap.TransitRouteResult.prototype.getPlan = function(i) {
};
/**
 * 此类表示驾车或步行路线中的一个关键点。它没有构造函数，通过Route.getStep()方法获得。
 *
 *
 */
BMap.Step = function() {
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 返回关键点地理坐标。
 *
 *
 * @return {BMap.Point}
 *
 */
BMap.Step.prototype.getPoint = function() {
};
/**
 * 返回关键点地理坐标。（自1.2新增）
 *
 *
 * @return {BMap.Point}
 *
 */
BMap.Step.prototype.getPosition = function() {
};
/**
 * 返回本关键点在路线中的位置索引
 *
 *
 * @return {Number}
 *
 */
BMap.Step.prototype.getIndex = function() {
};
/**
 * 返回关键点描述文本，默认包含HTML标签。当includeHtml为false时，描述文本不包含HTML标签。
 *
 *
 * @param {BMap.Boolean}
 *            includeHtml
 * @return {BMap.String}
 *
 */
BMap.Step.prototype.getDescription = function(includeHtml) {
};
/**
 * 返回到下一个关键点的距离，当format为false时仅返回数值（单位为米）。
 *
 *
 * @param {BMap.Boolean}
 *            format
 * @return {BMap.String|Number}
 *
 */
BMap.Step.prototype.getDistance = function(format) {
};
/**
 * 此类作为Geolocation的getCurrentPosition方法的回调函数参数，不可实例化(自 1.1 新增)。
 *
 *
 */
BMap.GeolocationResult = function() {
};
/**
 *
 * 坐标点。
 *
 * @type {BMap.Point}
 *
 */
BMap.GeolocationResult.prototype.point = null;
/**
 *
 * 定位精确程度，单位为米。
 *
 * @type {Number}
 *
 */
BMap.GeolocationResult.prototype.accuracy = 0;
/**
 * 此类表示一个行政区域的边界。
 *
 * <p>
 * 创建行政区域搜索的对象实例。
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.Boundary
 *
 */
BMap.Boundary = function() {
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 返回行政区域的边界。 name: 查询省、直辖市、地级市、或县的名称。
 * callback:执行查询后，数据返回到客户端的回调函数，数据以回调函数的参数形式返回。返回结果是一个数组，数据格式如下： arr[0] = "x1,
 * y1; x2, y2; x3, y3; ..." arr[1] = "x1, y1; x2, y2; x3, y3; ..." arr[2] = "x1,
 * y1; x2, y2; ..." … 否则回调函数的参数为null。(自1.3自增)
 *
 *
 * @param {BMap.String}
 *            name
 * @param {function}
 *            callback
 * @since 1.3
 */
BMap.Boundary.prototype.get = function(name, callback) {
};
/**
 * 此类表示一条公交出行方案。没有构造函数，通过TransitRouteResult.getPlan()方法获得。
 *
 *
 */
BMap.TransitRoutePlan = function() {
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 返回方案包含的公交线路段数。
 *
 *
 * @return {Number}
 *
 */
BMap.TransitRoutePlan.prototype.getNumLines = function() {
};
/**
 * 返回方案包含的某条公交线路。
 *
 *
 * @param {Number}
 *            i
 * @return {BMap.Line}
 *
 */
BMap.TransitRoutePlan.prototype.getLine = function(i) {
};
/**
 * 返回方案包含的步行线路段数。
 *
 *
 * @return {Number}
 *
 */
BMap.TransitRoutePlan.prototype.getNumRoutes = function() {
};
/**
 * 返回方案包含的某条步行线路。
 *
 *
 * @param {Number}
 *            i
 * @return {BMap.Route}
 *
 */
BMap.TransitRoutePlan.prototype.getRoute = function(i) {
};
/**
 * 返回方案总距离。当format参数为true时，返回方案距离字符串（包含单位），当format为false时，仅返回数值（单位为米）信息。默认参数为true。
 *
 *
 * @param {BMap.Boolean}
 *            format
 * @return {BMap.String|Number}
 *
 */
BMap.TransitRoutePlan.prototype.getDistance = function(format) {
};
/**
 * 返回方案总时间。当format参数为true时，返回描述时间的字符串（包含单位），当format为false时，仅返回数值（单位为秒）信息。默认参数为true。(自
 * 1.1 新增)
 *
 *
 * @param {BMap.Boolean}
 *            format
 * @return {BMap.String|Number}
 * @since 1.1
 */
BMap.TransitRoutePlan.prototype.getDuration = function(format) {
};
/**
 * 返回方案描述文本，默认包含HTML标签。当includeHtml为false时，方案描述不包含HTML标签。
 *
 *
 * @param {BMap.Boolean}
 *            includeHtml
 * @return {BMap.String}
 *
 */
BMap.TransitRoutePlan.prototype.getDescription = function(includeHtml) {
};
/**
 * 用于获取步行路线规划方案。
 *
 * <p>
 * 创建一个步行导航实例。location表示检索区域，类型可为地图实例、坐标点或城市名称的字符串。当参数为地图实例时，检索位置由当前地图中心点确定；当参数为坐标时，检索位置由该点所在位置确定；当参数为城市名称时，检索会在该城市内进行。
 * </p>
 *
 * @author baidu inc.
 *
 * @class BMap.WalkingRoute
 * @param {BMap.Map|Point|String}
 *            location
 * @param {BMap.WalkingRouteOptions}
 *            [opts]
 *
 */
BMap.WalkingRoute = function(location, opts) {
};
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 发起检索。 start: 起点，参数可以是关键字、坐标点（自1.1版本支持）或者LocalSearchPoi实例。 end:
 * 终点，参数可以是关键字、坐标点（自1.1版本支持）或者LocalSearchPoi实例。
 *
 *
 * @param {BMap.String|Point|LocalResultPoi}
 *            start
 * @param {BMap.String|Point|LocalResultPoi}
 *            end
 *
 */
BMap.WalkingRoute.prototype.search = function(start, end) {
};
/**
 * 返回最近一次检索的结果
 *
 *
 * @return {BMap.WalkingRouteResult}
 *
 */
BMap.WalkingRoute.prototype.getResults = function() {
};
/**
 * 清除最近一次检索的结果
 *
 *
 *
 */
BMap.WalkingRoute.prototype.clearResults = function() {
};
/**
 * 启用自动调整地图层级，当指定了搜索结果所展现的地图时有效。
 *
 *
 *
 */
BMap.WalkingRoute.prototype.enableAutoViewport = function() {
};
/**
 * 禁用自动调整地图层级。
 *
 *
 *
 */
BMap.WalkingRoute.prototype.disableAutoViewport = function() {
};
/**
 * 设置检索范围，参数类型可以为地图实例、坐标点或字符串。例：setLocation("北京市")
 *
 *
 * @param {BMap.Map|Point|String}
 *            location
 *
 */
BMap.WalkingRoute.prototype.setLocation = function(location) {
};
/**
 * 设置检索结束后的回调函数。 参数： results: WalkingRouteResult。
 *
 *
 * @param {Function}
 *            callback
 *
 */
BMap.WalkingRoute.prototype.setSearchCompleteCallback = function(callback) {
};
/**
 * 设置添加标注后的回调函数。 参数： pois: Array<LocalResultPoi>，起点和目的地点数组。通过marker属性可得到其对应的标注。
 *
 *
 * @param {Function}
 *            callback
 *
 */
BMap.WalkingRoute.prototype.setMarkersSetCallback = function(callback) {
};
/**
 * 设置气泡打开后的回调函数。 参数： poi: LocalResultPoi，通过其marker属性可得到当前的标注。 html:
 * HTMLElement，气泡内的DOM元素。
 *
 *
 * @param {Function}
 *            callback
 *
 */
BMap.WalkingRoute.prototype.setInfoHtmlSetCallback = function(callback) {
};
/**
 * 设置添加路线后的回调函数。 参数： routes: Array<Route>，步行线路数组，通过Route.getPolyline()方法可得到对应的折线覆盖物。
 *
 *
 * @param {Function}
 *            callback
 *
 */
BMap.WalkingRoute.prototype.setPolylinesSetCallback = function(callback) {
};
/**
 * 设置结果列表创建后的回调函数。 参数： container: 结果列表所用的HTML元素
 *
 *
 * @param {Function}
 *            callback
 *
 */
BMap.WalkingRoute.prototype.setResultsHtmlSetCallback = function(callback) {
};
/**
 * 返回状态码
 *
 *
 * @return {BMap.StatusCodes}
 *
 */
BMap.WalkingRoute.prototype.getStatus = function() {
};
/**
 * 返回类型说明
 *
 *
 * @return {BMap.String}
 *
 */
BMap.WalkingRoute.prototype.toString = function() {
};
/**
 * 此类作为getCurrentPosition的可选参数，不能实例化(自 1.1 新增)。
 *
 *
 */
BMap.PositionOptions = function() {
};
/**
 *
 * 要求浏览器获取最佳结果。
 *
 * @type {BMap.Boolean}
 *
 */
BMap.PositionOptions.prototype.enableHighAccuracy = false;
/**
 *
 * 超时时间。
 *
 * @type {Number}
 *
 */
BMap.PositionOptions.prototype.timeout = 0;
/**
 *
 * 允许返回指定时间内的缓存结果。如果此值为0，则浏览器将立即获取新定位结果。
 *
 * @type {Number}
 *
 */
BMap.PositionOptions.prototype.maximumAge = 0;
/**
 * 此类表示一条公交线路。没有构造函数，通过TransitRoutePlan.getLine()方法得到。
 *
 *
 */
BMap.Line = function() {
};
/**
 *
 * 公交线路全称
 *
 * @type {BMap.String}
 *
 */
BMap.Line.prototype.title = "";
/**
 *
 * 公交线路类型
 *
 * @type {enum LineType}
 *
 */
BMap.Line.prototype.type = null;
/*
 * ================================================================================
 *
 * 方法
 *
 * ================================================================================
 */
/**
 * 返回公交线路途径的车站个数。
 *
 *
 * @return {Number}
 *
 */
BMap.Line.prototype.getNumViaStops = function() {
};
/**
 * 返回上车站。
 *
 *
 * @return {BMap.LocalResultPoi}
 *
 */
BMap.Line.prototype.getGetOnStop = function() {
};
/**
 * 返回下车站。
 *
 *
 * @return {BMap.LocalResultPoi}
 *
 */
BMap.Line.prototype.getGetOffStop = function() {
};
/**
 * 返回线路对应的地理坐标点数组，在公交方案中，地理坐标只给出方案涉及的部分。（自1.2废弃）
 *
 *
 * @return {BMap.Point[]}
 *
 */
BMap.Line.prototype.getPoints = function() {
};
/**
 * 返回线路对应的地理坐标点数组，在公交方案中，地理坐标只给出方案涉及的部分。（自1.2新增）
 *
 *
 * @return {BMap.Point[]}
 *
 */
BMap.Line.prototype.getPath = function() {
};
/**
 * 返回公交线路对应的折线覆盖物。
 *
 *
 * @return {BMap.Polyline}
 *
 */
BMap.Line.prototype.getPolyline = function() {
};
/**
 * 当format为true时，返回本段公交线路的距离字符串（包含单位），当format为false时仅返回数值（单位为米）。默认参数为true。
 *
 *
 * @param {BMap.Boolean}
 *            format
 * @return {BMap.String|Number}
 *
 */
BMap.Line.prototype.getDistance = function(format) {
};
/**
 * 此类表示WalkingRoute构造函数的可选参数。
 *
 *
 */
BMap.WalkingRouteOptions = function() {
};
/**
 *
 * 搜索结果呈现设置。
 *
 * @type {BMap.RenderOptions}
 *
 */
BMap.WalkingRouteOptions.prototype.renderOptions = null;
/**
 *
 * 检索完成后的回调函数。 参数： results: WalkingRouteResult
 *
 * @type {Function}
 *
 */
BMap.WalkingRouteOptions.prototype.onSearchComplete = null;
/**
 *
 * 标注添加完成后的回调函数。 参数： pois: Array<LocalResultPoi>，起点和目的地点数组，。通过marker属性可得到其对应的标注。
 *
 * @type {Function}
 *
 */
BMap.WalkingRouteOptions.prototype.onMarkersSet = null;
/**
 *
 * 折线添加完成后的回调函数。 参数： routes: Array<Route>，步行线路数组，通过Route.getPolyline()方法可得到对应的折线覆盖物。
 *
 * @type {Function}
 *
 */
BMap.WalkingRouteOptions.prototype.onPolylinesSet = null;
/**
 *
 * 标注气泡内容创建后的回调函数。 参数： poi: LocalResultPoi，通过其marker属性可得到当前的标注。 html:
 * HTMLElement，气泡内的DOM元素。
 *
 * @type {Function}
 *
 */
BMap.WalkingRouteOptions.prototype.onInfoHtmlSet = null;
/**
 *
 * 结果列表添加完成后的回调函数。 参数： container: 结果列表所用的HTML元素
 *
 * @type {Function}
 *
 */
BMap.WalkingRouteOptions.prototype.onResultsHtmlSet = null;
/**
 *
 * 驾车结果展现中点击列表后的展现点步骤。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_HIGHLIGHT_STEP = 0;
/**
 *
 * 驾车结果展现中点击列表后的展现路段。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_HIGHLIGHT_ROUTE = 0;
/**
 *
 * 检索成功。对应数值“0”。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_STATUS_SUCCESS = 0;
/**
 *
 * 城市列表。对应数值“1”。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_STATUS_CITY_LIST = 0;
/**
 *
 * 位置结果未知。对应数值“2”。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_STATUS_UNKNOWN_LOCATION = 0;
/**
 *
 * 导航结果未知。对应数值“3”。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_STATUS_UNKNOWN_ROUTE = 0;
/**
 *
 * 非法密钥。对应数值“4”。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_STATUS_INVALID_KEY = 0;
/**
 *
 * 非法请求。对应数值“5”。
 *
 * @constant
 * @type {Number}
 * @default
 *
 */
var BMAP_STATUS_INVALID_REQUEST = 0;
/**
 *
 * 没有权限。对应数值“6”。(自 1.1 新增)
 *
 * @constant
 * @type {Number}
 * @default
 * @since 1.1
 */
var BMAP_STATUS_PERMISSION_DENIED = 0;
/**
 *
 * 服务不可用。对应数值“7”。(自 1.1 新增)
 *
 * @constant
 * @type {Number}
 * @default
 * @since 1.1
 */
var BMAP_STATUS_SERVICE_UNAVAILABLE = 0;
/**
 *
 * 超时。对应数值“8”。(自 1.1 新增)
 *
 * @constant
 * @type {Number}
 * @default
 * @since 1.1
 */
var BMAP_STATUS_TIMEOUT = 0;
