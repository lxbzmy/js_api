/**
 * History.js gracefully supports the HTML5 History/State APIs (pushState,
 * replaceState, onPopState) in all browsers. Including continued support for
 * data, titles, replaceState. Supports jQuery, MooTools and Prototype. For
 * HTML5 browsers this means that you can modify the URL directly, without
 * needing to use hashes anymore. For HTML4 browsers it wi��
 * http://browserstate.github.com/history.js/demo/
 *
 * @author balupton
 * @link https://github.com/browserstate/history.js
 * @class History
 *
 */
History = {};

/**
 *
 * @type Object
 */
History.Adapter = {};
/**
 * Bind state change listener to a function.
 * <p>
 * History.Adapter.bind(window,'statechange',function(){ var State =
 * History.getState(); });
 *
 * @param {Window}
 *            dom,commonly window
 * @param {String}
 *            event type name,commonly is 'statechange'
 * @param {Function}
 *            fn
 */
History.Adapter.bind = function(dom, event, fn) {

};
/**
 * Put a new state step in History stack.
 *
 * @param {Object}
 *            param javascript object assosicate with this state.
 * @param {String}
 *            title Broswer' title to change.
 * @param {String}
 *            suid SUIDs (State Unique Identifiers) commonly a query string
 *            begin with ?
 */
History.pushState = function(param, title, suid) {

};
/**
 * Replace a history step.
 *
 * @param {Object}
 *            param javascript object assosicate with this state.
 * @param {String}
 *            title Broswer' title to change.
 * @param {String}
 *            suid SUIDs (State Unique Identifiers) commonly a query string
 *            begin with ?
 */
History.replaceState = function(param, title, suid) {

};

History.StateObject = function() {
	/**
	 * @type Object data first argument passed in pushState.
	 */
	this.data = {};
	/**
	 * @type String second argument passed in pushState.
	 */
	this.title = "";
	/**
	 * @type String full url begin with http://
	 */
	this.url = "";
  /**
   * <p>eg.
   * http://localhost:8080/abc/?foo=bar
   * @type String clean url without suid
   */
  this.cleanUrl="";
  /**
   * <p>eg.
   * /abc/?foo=bar&_suid=138415894116405008025567512959
   * @type String url without domain,and with _suid param
   */
  this.hash = "";

  /**
   * <p>eg.
   * http://localhost:8080/abc/?foo=bar&_suid=138415894116405008025567512959
   * @type String uril with full schema and _suid
   */
  this.hashedUrl = "";

  /**
   * @type Boolean
   */
  this.normalized = true;


};
/**
 * Go back,popup current state.
 */
History.back = function() {
};

/**
 * Go to specifiy history step
 *
 * @param {Number}
 *            step
 */
History.go = function(step) {
};

/**
 * Get current State.
 *
 * @return {History.StateObject}
 */
History.getState = function() {

};