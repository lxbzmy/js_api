About
===========

A set of javascript library API document.used in eclipse or other IDE for code hint.

eclipse IDE 已知问题
-----------------

* 如果添加上@method同时没有@param的话，jsdt识别不了@return.
* spket不能提示构造函数的参数表列。

api(s)
--------------

|           file        |       summary          |
|-----------------------------------|------------------------|
|   com.baidu.BMap.js               |  baidu.com map js api  |
|   github.browserstate.History.js  |  html5+4 history api   |