/**
 * OpenAjax Widget prototype document.
 *
 * @class
 * @constructor
 */
OpenAjax.Widget = function() {
    /**
     * OPTIONAL FUNCTION
     *
     * This event signals that the widget has finished loading and that all
     * resources specified by <library> and <require> elements are available.
     *
     * This is NOT the same as the browser's window or page onload event.
     *
     * The callback function has no arguments.
     */
    this.onLoad = function() {

    }

    /**
     * OPTIONAL FUNCTION
     *
     * This event signals that the page is about to unload the widget. This
     * notification is intended to allow the widget to store any transient data
     * such that it can be recovered upon reloading.
     *
     * This is NOT the same as the browser's window or page onunload event.
     *
     * The callback function has no arguments.
     */
    this.onUnload = function() {
    };

    /**
     * OPTIONAL FUNCTION
     *
     * If the onChange callback exists, it is invoked after ANY property value
     * changes. This allows a developer to have a single callback for any
     * property change events.
     *
     * @param {Object}
     *            event The event payload is an object that has the following
     *            properties:
     *
     * @param {String}
     *            event.property Property's name (same as 'name' attribute on
     *            <property> element)
     * @param {String}
     *            event.oldValue Any JSON-serializable JavaScript value (e.g.,
     *            String or Object)
     * @param {String}
     *            event.newValue Any JSON-serializable JavaScript value (e.g.,
     *            String or Object)
     * @param {Boolean}
     *            event.self 'true' if this widget changed the property value by
     *            calling setPropertyValue(). 'false' otherwise.
     */
    this.onChange = function(event) {
    };

    /**
     * OPTIONAL FUNCTION
     *
     * This event signals that the mode for the widget has changed.
     *
     * @param {Object}
     *            event The event payload is an object that has the following
     *            properties:
     *
     * @param {String}
     *            event.oldMode "view"|"edit"|"help"|<QName>
     * @param {String} event.newMode
     *      "view"|"edit"|"help"|<QName>
     * @param {String} event.renderedBy
     *      "host" | "widget"
     */
    this.onModeChanged = function(event) {
    };

    /**
     * OPTIONAL FUNCTION
     *
     * This event signals that the widget has just been resized by the host.
     *
     * @param {Object}
     *            event The event payload is an object that has the following
     *            properties:
     *
     * @param {Number}
     *            event.oldWidth Previous width in pixels.
     * @param {Number}
     *            event.oldHeight Previous height in pixels.
     * @param {Number}
     *            event.newWidth New width in pixels.
     * @param {Number}
     *            event.newHeight New height in pixels.
     */
    this.onSizeChanged = function(event) {
    };
    /**
     * OPTIONAL FUNCTION
     *
     * This event signals that the container considers the widget's presentation
     * to be stale and requires a refresh.
     *
     * The callback function has no arguments.
     */
    this.onRefresh = function() {
    };

    /**
     * @type {OpenAjax.Widget.OpenAjaxAPI}
     */
    this.OpenAjax = new OpenAjax.Widget.OpenAjaxAPI();

}

OpenAjax.Widget.OpenAjaxAPI = function() {

    /**
     * Returns the ID string corresponding to this widget (i.e., the value of
     * the __WID__ substitution variable).
     *
     * @returns {String} ID string corresponding to this widget
     *
     * @throws {OpenAjax.widget.Error.Inactive}
     *             If widget is not currently active.
     */
    this.getId = function() {
    };
    /**
     * Return the maximum size in pixels that the run-time environment will
     * allow this widget to grow.
     *
     * @returns {Object} The return value is an object with a width and/or a
     *          height property representing the current available dimensions of
     *          the widget.
     *
     * @throws {OpenAjax.widget.Error.Inactive}
     *             If widget is not currently active.
     */
    this.getAvailableSize = function() {
    };
    /**
     * Request the current width and height of the element that immediately
     * contains the widget.
     *
     * @returns {Object} The return value is an object with a width and a height
     *          property representing the current dimensions of the widget.
     *
     * @throws {OpenAjax.widget.Error.Inactive}
     *             If widget is not currently active.
     */
    this.getSize = function() {
    };

    /**
     * Request that the run-time environment update the width and height of the
     * element that contains the widget.
     *
     * @param {Object}
     *            dimensions This parameter is an object with width and/or
     *            height properties. For example: { width: 500, height: 400 }
     *            requests that the height be reset to 500px and that the width
     *            be reset to 400px. { height: 400 } requests that the height be
     *            reset to 400px and that the width be left alone. { width: 500 }
     *            requests that the width be reset to 500px and that the height
     *            be left alone.
     *
     * @throws {OpenAjax.widget.Error.Inactive}
     *             If widget is not currently active.
     * @throws {OpenAjax.widget.Error.BadParameters}
     *             Incorrect function parameters.
     */
    this.requestSizeChange = function(dimensions) {
    };
    /**
     * Returns the widget's current mode.
     *
     * @returns {String} mode The widget's current mode
     *
     * @throws {OpenAjax.widget.Error.Inactive}
     *             If widget is not currently active.
     */
    this.getMode = function() {
    };

    /**
     * Request transition to a specified mode. The widget container might not
     * honor this request. If the request is honored, then the onModeChanged
     * event will notify the widget of the change (if the widget has a handler
     * for that event).
     *
     * @param {String}
     *            mode The name of the mode to which the widget is requesting
     *            transition
     *
     * @throws {OpenAjax.widget.Error.Inactive}
     *             If widget is not currently active.
     * @throws {OpenAjax.widget.Error.BadParameters}
     *             Incorrect function parameters.
     */
    this.requestModeChange = function(mode) {
    };
    /**
     * Returns the current value of the requested property name.
     *
     * This function returns immediately.
     *
     * @param {String}
     *            name The name of the property to retrieve
     *
     * @returns {*} The current value of the property named name. The value must
     *          be serializable as JSON.
     *
     * @throws {OpenAjax.widget.Error.Inactive}
     *             If widget is not currently active.
     */
    this.getPropertyValue = function(name) {
    };
    /**
     * Request that the value of the requested property be changed. This
     * function may cause the updated value of the property to be published.
     * This function returns immediately.
     *
     * @param {String}
     *            name This is the name of the property to set
     * @param {*}
     *            value This is the value to which the property is to be set.
     *            The value must be serializeable as JSON.
     *
     * @throws {OpenAjax.widget.Error.Inactive}
     *             If widget is not currently active.
     * @throws {OpenAjax.widget.Error.BadParameters}
     *             Invalid params. e.g., value is not JSON-serializable
     */
    this.setPropertyValue = function(name, value) {
    };
    /**
     * Return array containing the names of all of the widget's properties, in
     * arbitrary order.
     *
     * @returns An array containing the names of all of the widget's properties
     * @type {String[]}
     *
     * @throws {OpenAjax.widget.Error.Inactive}
     *             If widget is not currently active.
     */
    this.getPropertyNames = function() {
    };
    /**
     * Returns the localized version of the given string.
     *
     * @param {String}
     *            key The lookup key for the localization string. This parameter
     *            must be an exact string match with the 'name' attribute on a
     *            <msg> element within a message bundle file.
     *
     * @returns {String} The localized version of the string.
     *
     * @throws {OpenAjax.widget.Error.Inactive}
     *             If widget is not currently active.
     * @throws {OpenAjax.widget.Error.NotFound}
     *             Requested message not found.
     */
    this.getMsg = function(key) {
    };
    /**
     * Returns a revised URI string that can be used to access a resource. Takes
     * into account the base URLs for the current run-time environment and proxy
     * server URI rewriting.
     *
     * @param {String}
     *            uri URI to be proxied. Can be either an absolute or relative
     *            URI.
     *
     * @returns {String} An absolute URI by which the browser can access the
     *          given resource.
     *
     * @throws {OpenAjax.widget.Error.Inactive}
     *             If widget is not currently active.
     * @throws {OpenAjax.widget.Error.BadParameters}
     *             Invalid params
     */
    this.rewriteURI = function(uri) {
    };
    /**
     * @type {new OpenAjax.hub.HubClient}
     */
    this.hub = new OpenAjax.hub.HubClient({});
};

OpenAjax.widget = {};
/**
 * Global function that returns a widget instance whose ID is wid
 *
 * @param {Object}
 *            wid
 *
 * @returns a widget instance, or null if none can be found
 * @type {Object}
 *
 * @throws {OpenAjax.widget.Error.Inactive}
 *             If widget is not currently active.
 * @throws {OpenAjax.widget.Error.BadParameters}
 *             Invalid params. e.g., value is not JSON-serializable
 */
OpenAjax.widget.byId = function(wid) {
}

OpenAjax.widget.Error = {
    // The widget is not active at this time
    Inactive : "OpenAjax.widget.Error.Inactive",
    // Either a required argument is missing or an invalid argument was provided
    BadParameters : "OpenAjax.widget.Error.BadParameters",
    // The requested resource could not be found
    NotFound : "OpenAjax.widget.Error.NotFound"
};