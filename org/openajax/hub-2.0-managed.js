/**
 * @class
 */
OpenAjax = {};

OpenAjax.hub = {};

/**
 * Registers an Ajax library with the OpenAjax Hub.
 * <p>
 * example:<code> OpenAjax.hub.registerLibrary("OpenAjax","http://openajax.org/hub","1.0",{};);</code>
 * </p>
 * <p>
 * The <code>prefix</code> parameter MUST be compatible with the prefix value
 * within an xmlns attribute per the W3C's <a
 * href="http://www.w3.org/TR/REC-xml-names/" class="external text"
 * title="http://www.w3.org/TR/REC-xml-names/" rel="nofollow">Namespaces in XML</a>
 * Recommendation, such as the value <code>foo</code> in the following
 * namespace declaration:
 * <code>xmlns:foo="<a href="http://example.com" class="external free" title="http://example.com" rel="nofollow">http://example.com</a>"</code>.
 * </p>
 * <p>
 * The <code>namespaceURI</code> MUST be compatible with the W3C's <a
 * href="http://www.w3.org/TR/REC-xml-names/" class="external text"
 * title="http://www.w3.org/TR/REC-xml-names/" rel="nofollow">Namespaces in XML</a>
 * Recommendation.
 * </p>
 * <p>
 * The <code>version</code> parameter MUST be of the form of #[.#]*, such as
 * "1", "1.1", "2.0" or "2.20.2", followed by an optional arbitrary string
 * (e.g., "2.20.2Beta" or "1.1 Build 543"). The leading numeric part of the
 * version string allows script to determine which version of a particular Ajax
 * library has been loaded, using equal/greater-than/less-than comparisons. The
 * optional trailing arbitrary string must begin with a character other than a
 * numeric character (i.e., 0-9) or period character. The optional trailing
 * string must be stripped before invoking version comparison logic.
 * </p>
 * <p>
 * The first (non-negative) integer token of the leading numeric part SHOULD
 * reflect the library's major release number. If present, the second
 * (non-negative) integer token of the leading numeric part SHOULD reflect the
 * minor release number. If present, the third (non-negative) integer token of
 * the leading numeric part SHOULD reflect the (next level down) minor release
 * ("minor-minor" release number). It is acceptable to include other numeric
 * values (4th, 5th, etc.) although probably not useful. To illustrate, with the
 * value "1.20.3 beta 7", the major release is "1", the minor release is "20",
 * and the minor-minor release is "3".
 * </p>
 * <p>
 * In version comparison logic, first extract the leading numeric part of the
 * version string; then tokenize the numeric part. Now do a numeric comparison
 * of major version tokens. If equal, then compare minor versions. If equal,
 * then compare minor-minor versions. Etc. Missing tokens are equivalent to
 * zero.
 * </p>
 * <p>
 * Subsequent releases of a given toolkit MUST have version numbers that are
 * greater than or equal to previous releases. Although not required, it is
 * recommended that each release have a higher version number than previous
 * releases. One technique to accomplish this is to use the build number for the
 * 3rd token (the "minor-minor" release number), such as "1.3.2065", where the
 * major version is "1", the minor version is "3", and the build number is
 * "2065".
 * </p>
 * <p>
 * This function has no return value.
 * </p>
 * <p>
 * It is not an error to re-register a library. If
 * <code>OpenAjax.hub.registerLibrary</code> is called on a library that is
 * already registered, then the new registration MUST replace the previous
 * registration.
 * </p>
 * <p>
 * Immediately after the library has been registered (or re-registered), the Hub
 * implementation MUST invoke <code>OpenAjax.hub.publish()</code>, where the
 * event name is <i>org.openajax.hub.registerLibrary</i> and <i>publisherData</i>
 * is a reference to the <code>OpenAjax.hub.libraries[prefix]</code> object
 * for the given library.
 * </p>
 *
 * @see http://www.openajax.org/member/wiki/OpenAjax_Hub_2.0_Specification_Libraries
 * @param {String}
 *            prefix A unique string that identifies the library being
 *            registered.
 * @param {String}
 *            namespaceURI A namespace URI string suitable for use with xmlns
 *            attribute, such as xmlns:foo="http://example.com"
 * @param {String}
 *            version Version number.
 * @param {*}
 *            [extraData] An optional arbitrary Object holding extra information
 *            about the library.
 */
OpenAjax.hub.registerLibrary = function(prefix, namespaceURI, version,
        extraData) {
};

/**
 * Unregisters an Ajax library with the OpenAjax Hub.
 *
 * <p>
 * This function has no return value.
 * </p>
 * <p>
 * It is not an error to unregister a library that is not currently registered.
 * If <code>OpenAjax.hub.unregisterLibrary</code> is called on a library which
 * is not currently registered, then the unregister request MUST result in no
 * action and MUST NOT generate an exception.
 * </p>
 * <p>
 * Immediately before the Hub implementation attempts to unregister the given
 * library, even if the specified library is not currently registered, the Hub
 * implementation MUST invoke <code>OpenAjax.hub.publish()</code>, where the
 * event name is <i>org.openajax.hub.unregisterLibrary</i> and <i>publisherData</i>
 * is a reference to the <code>OpenAjax.hub.libraries[prefix]</code> object
 * for the given library (or null if the given library is not currently
 * registered).
 * </p>
 *
 * @param {String}
 *            prefix A unique string that identifies the library being
 *            registered.
 */
OpenAjax.hub.unregisterLibrary = function(prefix) {
};

/**
 * <p>
 * This variable holds the list of libraries that have registered themselves
 * with the OpenAjax Hub.
 * </p>
 * <p>
 * In order to achieve best execution time performance and smallest download
 * size, OpenAjax Hub implementations MAY use
 * <code>OpenAjax.hub.libraries</code> to store its own internal information
 * about registered libraries. Because of this, OpenAjax-conformant libraries
 * MUST treat <code>OpenAjax.hub.libraries</code> as a read-only variable and
 * thus MUST NOT make any changes to this object or its contents.
 * </p>
 * <p>
 * <code>OpenAjax.hub.libraries</code> is a JavaScript associative array,
 * where <code>OpenAjax.hub.libraries</code> contains a
 * <code>&lt;prefix&gt;</code> property for each library that has registered
 * itself via <code>OpenAjax.hub.registerLibrary()</code>. For example, if
 * the FooLib library has prefix <code>foo</code> and has registered itself
 * via <code>OpenAjax.hub.registerLibrary()</code>, then there will be an
 * <code>OpenAjax.hub.libraries.foo</code> property. The
 * <code>OpenAjax.hub.libraries.foo</code> property MUST point to an object
 * that in turn will have four properties, 'prefix', 'namespaceURI', 'version',
 * and 'extraData', which will hold the corresponding values that were passed to
 * <code>OpenAjax.hub.registerLibrary()</code>. Therefore, the 'version' for
 * FooLib can be found at: <code>OpenAjax.hub.libraries.foo.version</code>
 * </p>
 * <p>
 * Developers should be aware that the content of
 * <code>OpenAjax.hub.libraries</code> is dynamic. It is possible, for
 * example, that an Ajax library might unregister itself (and perhaps
 * re-register itself) as the application runs. Therefore, applications SHOULD
 * NOT retain persistent pointers to the internal contents of
 * <code>OpenAjax.hub.libraries</code> because those pointers might become out
 * of sync with the current state of the Hub; instead, applications SHOULD
 * access the internal values via a reference to
 * <code>OpenAjax.hub.libraries</code> each time before using those values.
 * </p>
 *
 * @type Object
 */
OpenAjax.hub.libraries = {};

/**
 * Creates a subscription on the specified topic name.
 * <p>
 * This function returns an Object (a "subscription") that is unique for this
 * particular subscription. To unsubscribe, return this Object to
 * <code>OpenAjax.hub.unsubscribe(...)</code>. This subscription object MUST
 * be treated as an opaque, read-only value by applications and libraries that
 * are using the Hub. Thus, the object MUST NOT be modified or deleted.
 * </p>
 * <p>
 * Topic names are strings composed of sequences of tokens separated by the "."
 * character (period character - 0x2E). They MAY include wildcard characters.
 * For detailed rules and guidelines on topic names, see <a
 * href="http://www.openajax.org/member/wiki/OpenAjax_Hub_2.0_Specification_Topic_Names">Topic
 * Names chapter</a>.
 * </p>
 * <p>
 * The callback function will receive the following parameters (see
 * <tt>OpenAjax.hub.publish()</tt> for description of <tt>publisherData</tt>):
 * </p>
 * <p>
 * <tt>&nbsp;&nbsp;function(name, publisherData, subscriberData){ ... };</tt>
 * </p>
 * <p>
 * The callback function does not return a value. The callback function SHOULD
 * NOT throw exceptions.
 * </p>
 * <h4> <span class="mw-headline"> Hub 1.0 feature no longer available in Hub
 * 2.0 </span></h4>
 * <p>
 * Hub 1.0 included a 5th parameter, 'filter', to
 * <code>OpenAjax.hub.subscribe</code>. This parameter has been removed from
 * the OpenAjax Hub with version 2.0. Below is the description of the
 * (now-removed) 'filter' parameter.
 * </p>
 * <dl>
 * <dd>
 * <dl>
 * <dt> filter </dt>
 * <dd> Optional, can be null. A function that returns true or false to either
 * match or deny a match of the published event. The filter function allows for
 * the definition of an intermediate function that is invoked before the
 * callback function. Filter functions return a boolean. If the filter function
 * returns false, then the given callback function is not invoked. The filter
 * callback function takes the same parameters as the data callback function.
 * </dd>
 * </dl>
 * </dd>
 * </dl>
 * <p>
 * (Note that although this convenience feature have been removed from the
 * OpenAjax Hub 2.0 Specification, it continues to be supported within the open
 * source reference implementation provided by OpenAjax Alliance within a
 * special "Unmanaged Hub" release configuration, which is fully compatible with
 * the Hub 1.0 Specification.)
 * </p>
 *
 * @link http://www.openajax.org/member/wiki/OpenAjax_Hub_2.0_Specification_Unmanaged_Hub
 * @param {String}
 *            name The name of the topic to which you want to subscribe.
 * @param {Function|String}
 *            refOrName A function object reference or the name of a function
 *            that is invoked whenever an event is published on the topic.
 * @param {Object}
 *            [scope] An Object in which to execute refOrName when handling the
 *            event. If null, window object is used. The scope parameter will be
 *            the "this" object when the callback is invoked.
 * @param {*}
 *            [subscriberData] Client application provides this data, which is
 *            handed back to the client application in the subscriberData
 *            parameter of the callback function.
 * @returns {String} A String Object (a "subscription") that is unique for this
 *          particular subscription.
 *
 */
OpenAjax.hub.subscribe = function(name, refOrName, scope, subscriberData) {
};

/**
 * Removes a subscription to an event.
 *
 * @param {String}
 *            subscription The return value from a previous call to
 *            OpenAjax.hub.subscribe().
 */
OpenAjax.hub.unsubscribe = function(subscription) {
};

/**
 * Publishes (broadcasts) an event.
 * <p>
 * The <code>name</code> parameter MUST NOT contain wildcard tokens. For
 * detailed rules and guidelines on topic names, see <a
 * href="http://www.openajax.org/member/wiki/OpenAjax_Hub_2.0_Specification_Topic_Names">Topic
 * Names chapter</a>.
 * </p>
 *
 * @param {String}
 *            name The name of the topic that is being published.
 * @param {*}
 *            [publisherData] (Optional) An arbitrary Object holding extra
 *            information that will be passed as an argument to the handler
 *            function.
 * @type {Object};
 */
OpenAjax.hub.publish = function(name, publisherData) {
};

/**
 * OpenAjax.hub.Error holds the list of errors thrown by the Managed Hub APIs.
 *
 * @type Object
 */
OpenAjax.hub.Error = {
    // Either a required argument is missing or an invalid argument was provided
    BadParameters : "OpenAjax.hub.Error.BadParameters",
    // The specified hub has been disconnected and cannot perform the requested
    // operation:
    Disconnected : "OpenAjax.hub.Error.Disconnected",
    // Container with specified ID already exists:
    Duplicate : "OpenAjax.hub.Error.Duplicate",
    // The specified ManagedHub has no such Container (or it has been removed)
    NoContainer : "OpenAjax.hub.Error.NoContainer",
    // The specified ManagedHub or Container has no such subscription
    NoSubscription : "OpenAjax.hub.Error.NoSubscription",
    // Permission denied by manager's security policy
    NotAllowed : "OpenAjax.hub.Error.NotAllowed",
    // Wrong communications protocol identifier provided by Container or
    // HubClient
    WrongProtocol : "OpenAjax.hub.Error.WrongProtocol"
};
;

/**
 * OpenAjax.hub.SecurityAlert holds the list of standard codes used when
 * attempted security violations are detected. Unlike Errors, these codes are
 * not thrown as exceptions but rather passed into the SecurityAlertHandler
 * function registered with the Hub instance.
 *
 * @type Object
 */
OpenAjax.hub.SecurityAlert = {
    // Container did not load (possible frame phishing attack)
    LoadTimeout : "OpenAjax.hub.SecurityAlert.LoadTimeout",
    // Hub suspects a frame phishing attack against the specified container
    FramePhish : "OpenAjax.hub.SecurityAlert.FramePhish",
    // Hub detected a message forgery that purports to come to a specified
    // container
    ForgedMsg : "OpenAjax.hub.SecurityAlert.ForgedMsg"
};
;

/**
 * onSecurityAlert callback for ManagedHub, Container and HubClient Invoked
 * within the scope defined by Hub.getScope(), when a security alert is raised.
 * Supplied to ManagedHub, Container and HubClient constructor via the
 * onSecurityAlert parameter
 *
 * @param {Object}
 *            source The Container or HubClient instance that raised the
 *            security alert
 * @param {OpenAjax.hub.SecurityAlert}
 *            alertType The type of alert
 */
OpenAjax.hub.onSecurityAlert = function(source, alertType) {
};

/**
 * The scope of the log function is the same as that of the object with which it
 * is associated. If the log function is associated with a Container, then in
 * the log function, the JavaScript "this" keyword refers to the
 * params.Container.scope that was passed into the Container constructor.
 *
 * @param {String}
 *            messageString The message to be logged
 */
OpenAjax.hub.log = function(messagestring) {
};

/**
 * An onComplete callback function is invoked when an asynchronous API call,
 * such as HubClient.connect() or HubClient.subscribe(), completes. When the
 * callback is invoked, the JavaScript "this" keyword refers to the object
 * specified in the scope parameter of the asynchronous API call.
 *
 * @callback OpenAjax.hub~onComplete
 * @param {*}
 *            item Item on which the completed operation was invoked
 * @param {Boolean}
 *            success If operation succeeded (item is active) then true, else
 *            false
 * @param {OpenAjax.hub.Error}
 *            errCode If success != true, then contains a string error code,
 *            else is undefined
 */
OpenAjax.hub.onComplete = function(item, success, errCode) {
};

/**
 * The OpenAjax.hub.Hub interface is a base interface that is implemented on the
 * manager side by OpenAjax.hub.ManagedHub. On the client side,
 * OpenAjax.hub.HubClient extends OpenAjax.hub.Hub.
 *
 * @class
 */
OpenAjax.hub.Hub = function() {
};
/**
 * Subscribe to a topic.
 *
 * @param {String}
 *            topic A valid topic string. MAY include wildcards.
 * @param {Function}
 *            onData Callback function that is invoked whenever an event is
 *            published on the topic
 * @param {Object}
 *            [scope] When onData callback or onComplete callback is invoked,
 *            the JavaScript "this" keyword refers to this scope object. If no
 *            scope is provided, default is window.
 * @param {Function}
 *            [onComplete] Invoked to tell the client application whether the
 *            subscribe operation succeeded or failed.
 * @param {*}
 *            [subscriberData] Client application provides this data, which is
 *            handed back to the client application in the subscriberData
 *            parameter of the onData callback function.
 *
 * @returns {String} subscriptionID Identifier representing the subscription.
 *          This identifier is an arbitrary ID string that is unique within this
 *          Hub instance
 *
 *
 * @throws {OpenAjax.hub.Error.Disconnected};
 *             if this Hub instance is not in CONNECTED state
 * @throws {OpenAjax.hub.Error.BadParameters};
 *             if the topic is invalid (e.g. contains an empty token)
 */
OpenAjax.hub.Hub.prototype.subscribe = function(topic, onData, scope,
        onComplete, subscriberData) {
};
/**
 * onData callback functions
 *
 * @param {String}
 *            topic The topic on which the event was published
 * @param {*}
 *            data The event "payload" data. Can be any JSON-serializable value.
 * @param {*}
 *            subscriberData The 'subscriberData' value that the caller supplied
 *            to the subscribe() call
 */
OpenAjax.hub.onData = function(topic, data, subscriberData) {
};

/**
 * Publish an event on a topic
 *
 * @param {String}
 *            topic A valid topic string. MUST NOT include wildcards.
 * @param {*}
 *            data Valid publishable data. To be portable across different
 *            Container implementations, this value should be serializable as
 *            JSON.
 *
 * @throws {OpenAjax.hub.Error.Disconnected};
 *             if this Hub instance is not in CONNECTED state
 * @throws {OpenAjax.hub.Error.BadParameters};
 *             if the topic cannot be published (e.g. contains wildcards or
 *             empty tokens) or if the data cannot be published (e.g. cannot be
 *             serialized as JSON)
 */
OpenAjax.hub.Hub.prototype.publish = function(topic, data) {
};

/**
 * Unsubscribe from a subscription
 *
 * @param {String}
 *            subscriptionID A subscriptionID returned by Hub.subscribe()
 * @param {Function}
 *            [onComplete] Callback function invoked when unsubscribe completes
 * @param {Object}
 *            [scope] When onComplete callback function is invoked, the
 *            JavaScript "this" keyword refers to this scope object. If no scope
 *            is provided, default is window.
 *
 * @throws {OpenAjax.hub.Error.Disconnected};
 *             if this Hub instance is not in CONNECTED state
 * @throws {OpenAjax.hub.Error.NoSubscription};
 *             if no such subscription is found
 */
OpenAjax.hub.Hub.prototype.unsubscribe = function(subscriptionID, onComplete,
        scope) {
};

/**
 * Return true if this Hub instance is in the Connected state. Else returns
 * false.
 *
 * This function can be called even if the Hub is not in a CONNECTED state.
 *
 * @returns {Boolean} Boolean
 *
 */
OpenAjax.hub.Hub.prototype.isConnected = function() {
};

/**
 * Returns the scope associated with this Hub instance and which will be used
 * with callback functions.
 *
 * This function can be called even if the Hub is not in a CONNECTED state.
 *
 * @returns {Object} scope object
 *
 */
OpenAjax.hub.Hub.prototype.getScope = function() {
};

/**
 * Returns the subscriberData parameter that was provided when Hub.subscribe was
 * called.
 *
 * @param {String}
 *            subscriberID The subscriberID of a subscription
 *
 * @returns {*} subscriberData
 *
 *
 * @throws {OpenAjax.hub.Error.Disconnected};
 *             if this Hub instance is not in CONNECTED state
 * @throws {OpenAjax.hub.Error.NoSubscription};
 *             if there is no such subscription
 */
OpenAjax.hub.Hub.prototype.getSubscriberData = function(subscriptionID) {
};

/**
 * Returns the scope associated with a specified subscription. This scope will
 * be used when invoking the 'onData' callback supplied to Hub.subscribe().
 *
 * @param {String}
 *            subscriberID The subscriberID of a subscription
 *
 * @returns {*} scope
 *
 *
 * @throws {OpenAjax.hub.Error.Disconnected};
 *             if this Hub instance is not in CONNECTED state
 * @throws {OpenAjax.hub.Error.NoSubscription};
 *             if there is no such subscription
 */
OpenAjax.hub.Hub.prototype.getSubscriberScope = function(subscriberID) {
};

/**
 * Returns the params object associated with this Hub instance.
 *
 * @returns {Object} params The params object associated with this Hub instance
 *
 */
OpenAjax.hub.Hub.prototype.getParameters = function() {
};

/**
 * Create a new ManagedHub instance.
 * <p>
 * The class OpenAjax.hub.ManagedHub implements OpenAjax.hub.Hub and provides
 * manager-side APIs to the Managed Hub. Most ManagedHub functions are
 * synchronous; in particular, functions that support onComplete callbacks
 * invoke these callbacks before returning control to the caller.
 *
 *
 * @constructor
 *
 * This constructor automatically sets the ManagedHub's state to CONNECTED.
 *
 * @param {Object}
 *            params Parameters used to instantiate the ManagedHub. Once the
 *            constructor is called, the params object belongs exclusively to
 *            the ManagedHub. The caller MUST not modify it.
 *
 * The params object may contain the following properties:
 *
 * @param {Function}
 *            params.onPublish Callback function that is invoked whenever a data
 *            value published by a Container is about to be delivered to some
 *            (possibly the same) Container. This callback function implements a
 *            security policy; it returns true if the delivery of the data is
 *            permitted and false if permission is denied.
 * @param {Function}
 *            params.onSubscribe Called whenever a Container tries to subscribe
 *            on behalf of its client. This callback function implements a
 *            security policy; it returns true if the subscription is permitted
 *            and false if permission is denied.
 * @param {Function}
 *            [params.onUnsubscribe] Called whenever a Container unsubscribes on
 *            behalf of its client. Unlike the other callbacks, onUnsubscribe is
 *            intended only for informative purposes, and is not used to
 *            implement a security policy.
 * @param {Object}
 *            [params.scope] Whenever one of the ManagedHub's callback functions
 *            is called, references to the JavaScript "this" keyword in the
 *            callback function refer to this scope object If no scope is
 *            provided, default is window.
 * @param {Function}
 *            [params.log] Optional logger function. Would be used to log to
 *            console.log or equivalent.
 *
 * @throws {OpenAjax.hub.Error.BadParameters};
 *             if any of the required parameters are missing
 */
OpenAjax.hub.ManagedHub = function(params) {
};

/* extends Hub */
OpenAjax.hub.ManagedHub.prototype = new OpenAjax.hub.Hub();

/**
 * onPublish callback for ManagedHub Invoked within the scope defined by
 * ManagedHub.getScope(), when a ManagedHub client publishes an event via
 * publishForClient and the published data is to be delivered to a second client
 * Supplied to ManagedHub constructor via the params.onPublish parameter
 *
 * @param {String}
 *            topic The topic to which the first container is publishing
 * @param {*}
 *            data The data that the first container is publishing
 * @param {OpenAjax.hub.Container}
 *            pcont The container that is publishing (if the publisher is the
 *            Manager Application, pcont is null)
 * @param {OpenAjax.hub.Container}
 *            scont The container to which the data is about to be delivered (if
 *            this is the Manager Application, scont is null)
 *
 * @returns {boolean} true if the delivery of the data is allowed, else false
 *
 */
OpenAjax.hub.onPublish = function(topic, data, pcont, scont) {
};

/**
 * onSubscribe callback for ManagedHub Invoked within the scope defined by
 * ManagedHub.getScope(), when a ManagedHub client subscribes via
 * subscribeForClient. Supplied to ManagedHub constructor via the
 * params.onSubscribe parameter
 *
 * @param {String}
 *            topic The topic to which the container is subscribing
 * @param {OpenAjax.hub.Container}
 *            container The container that is subscribing
 *
 * @returns {boolean} true if the subscribe is allowed, else false
 *
 */
OpenAjax.hub.onSubscribe = function(topic, container) {
};

/**
 * onUnsubscribe callback for ManagedHub Invoked within the scope defined by
 * ManagedHub.getScope(), when a ManagedHub client unsubscribes via
 * unsubscribeForClient. Supplied to ManagedHub constructor via the
 * params.onUnsubscribe parameter.
 *
 * @param {String}
 *            topic The topic to which the container is unsubscribing
 * @param {OpenAjax.hub.Container}
 *            container The container that is unsubscribing
 */
OpenAjax.hub.onUnsubscribe = function(topic, container) {
};

/**
 * Subscribe to a topic on behalf of a Container. Called only by Container
 * implementations, NOT by manager applications.
 *
 * This function: 1. Checks with the ManagedHub's onSubscribe security policy to
 * determine whether this Container is allowed to subscribe to this topic. 2. If
 * the subscribe operation is permitted, subscribes to the topic and returns the
 * ManagedHub's subscription ID for this subscription. 3. If the subscribe
 * operation is not permitted, throws OpenAjax.hub.Error.NotAllowed.
 *
 * When data is published on the topic, the ManagedHub's onPublish security
 * policy will be invoked to ensure that this Container is permitted to receive
 * the published data. If the Container is allowed to receive the data, then the
 * Container's sendToClient function will be invoked.
 *
 * When a Container needs to create a subscription on behalf of its client, the
 * Container MUST use this function to create the subscription.
 *
 * @param {OpenAjax.hub.Container}
 *            container A Container
 * @param {String}
 *            topic A valid topic
 * @param {String}
 *            containerSubID Arbitrary string ID that the Container uses to
 *            represent the subscription. Must be unique within the context of
 *            the Container
 *
 * @returns {String} managerSubID Arbitrary string ID that this ManagedHub uses
 *          to represent the subscription. Will be unique within the context of
 *          this ManagedHub
 *
 *
 * @throws {OpenAjax.hub.Error.Disconnected};
 *             if this.isConnected() returns false
 * @throws {OpenAjax.hub.Error.NotAllowed};
 *             if subscription request is denied by the onSubscribe security
 *             policy
 * @throws {OpenAjax.hub.Error.BadParameters};
 *             if one of the parameters, e.g. the topic, is invalid
 */
OpenAjax.hub.ManagedHub.prototype.subscribeForClient = function(container,
        topic, containerSubID) {
};

/**
 * Unsubscribe from a subscription on behalf of a Container. Called only by
 * Container implementations, NOT by manager application code.
 *
 * This function: 1. Destroys the specified subscription 2. Calls the
 * ManagedHub's onUnsubscribe callback function
 *
 * This function can be called even if the ManagedHub is not in a CONNECTED
 * state.
 *
 * @param {OpenAjax.hub.Container}
 *            container container instance that is unsubscribing
 * @param {String}
 *            managerSubID opaque ID of a subscription, returned by previous
 *            call to subscribeForClient()
 *
 * @throws {OpenAjax.hub.Error.NoSubscription};
 *             if subscriptionID does not refer to a valid subscription
 */
OpenAjax.hub.ManagedHub.prototype.unsubscribeForClient = function(container,
        managerSubID) {
};

/**
 * Publish data on a topic on behalf of a Container. Called only by Container
 * implementations, NOT by manager application code.
 *
 * @param {OpenAjax.hub.Container}
 *            container Container on whose behalf data should be published
 * @param {String}
 *            topic Valid topic string. Must NOT contain wildcards.
 * @param {*}
 *            data Valid publishable data. To be portable across different
 *            Container implementations, this value SHOULD be serializable as
 *            JSON.
 *
 * @throws {OpenAjax.hub.Error.Disconnected};
 *             if this.isConnected() returns false
 * @throws {OpenAjax.hub.Error.BadParameters};
 *             if one of the parameters, e.g. the topic, is invalid
 */
OpenAjax.hub.ManagedHub.prototype.publishForClient = function(container, topic,
        data) {
};

/**
 * Destroy this ManagedHub
 *
 * 1. Sets state to DISCONNECTED. All subsequent attempts to add containers,
 * publish or subscribe will throw the Disconnected error. We will continue to
 * allow "cleanup" operations such as removeContainer and unsubscribe, as well
 * as read-only operations such as isConnected 2. Remove all Containers
 * associated with this ManagedHub
 */
OpenAjax.hub.ManagedHub.prototype.disconnect = function() {
};

/**
 * Get a container belonging to this ManagedHub by its clientID, or null if this
 * ManagedHub has no such container
 *
 * This function can be called even if the ManagedHub is not in a CONNECTED
 * state.
 *
 * @param {String}
 *            containerId Arbitrary string ID associated with the container
 *
 * @returns {OpenAjax.hub.Container} container associated with given ID
 *
 */
OpenAjax.hub.ManagedHub.prototype.getContainer = function(containerId) {
};
/**
 * Returns an array listing all containers belonging to this ManagedHub. The
 * order of the Containers in this array is arbitrary.
 *
 * This function can be called even if the ManagedHub is not in a CONNECTED
 * state.
 *
 * @returns {OpenAjax.hub.Container[]} container array
 *
 */
OpenAjax.hub.ManagedHub.prototype.listContainers = function() {
};
/**
 * Add a container to this ManagedHub.
 *
 * This function should only be called by a Container constructor.
 *
 * @param {OpenAjax.hub.Container}
 *            container A Container to be added to this ManagedHub
 *
 * @throws {OpenAjax.hub.Error.Duplicate};
 *             if there is already a Container in this ManagedHub whose clientId
 *             is the same as that of container
 * @throws {OpenAjax.hub.Error.Disconnected};
 *             if this.isConnected() returns false
 */
OpenAjax.hub.ManagedHub.prototype.addContainer = function(container) {
};
/**
 * Remove a container from this ManagedHub immediately
 *
 * This function can be called even if the ManagedHub is not in a CONNECTED
 * state.
 *
 * @param {OpenAjax.hub.Container}
 *            container A Container to be removed from this ManagedHub
 *
 * @throws {OpenAjax.hub.Error.NoContainer};
 *             if no such container is found
 */
OpenAjax.hub.ManagedHub.prototype.removeContainer = function(container) {
};

/**
 *
 * Container,OpenAjax.hub.Container is an interface that MUST be implemented by
 * all containers.
 * <p>
 * All versions of the OpenAjax Hub MUST implement two types of Containers:
 * <ul>
 * <li> OpenAjax.hub.IframeContainer
 * <li>OpenAjax.hub.InlineContainer
 * <p>
 * These, and any custom containers that might be added to the OpenAjax Hub, are
 * subclasses of OpenAjax.hub.Container.
 *
 * @constructor
 *
 * Container represents an instance of a manager-side object that contains and
 * communicates with a single client of the hub. The container might be an
 * inline container, an iframe FIM container, or an iframe PostMessage
 * container, or it might be an instance of some other implementation.
 *
 * @param {OpenAjax.hub.ManagedHub}
 *            hub Managed Hub instance
 * @param {String}
 *            clientID A string ID that identifies a particular client of a
 *            Managed Hub. Unique within the context of the ManagedHub.
 * @param {Object}
 *            params Parameters used to instantiate the Container. Once the
 *            constructor is called, the params object belongs exclusively to
 *            the Container. The caller MUST not modify it. Implementations of
 *            Container may specify additional properties for the params object,
 *            besides those identified below. The following params properties
 *            MUST be supported by all Container implementations:
 * @param {Function}
 *            params.Container.onSecurityAlert Called when an attempted security
 *            breach is thwarted. Function is defined as follows:
 *            function(container, securityAlert)
 * @param {Function}
 *            [params.Container.onConnect] Called when the client connects to
 *            the Managed Hub. Function is defined as follows:
 *            function(container)
 * @param {Function}
 *            [params.Container.onDisconnect] Called when the client disconnects
 *            from the Managed Hub. Function is defined as follows:
 *            function(container)
 * @param {Object}
 *            [params.Container.scope] Whenever one of the Container's callback
 *            functions is called, references to "this" in the callback will
 *            refer to the scope object. If no scope is provided, default is
 *            window.
 * @param {Function}
 *            [params.Container.log] Optional logger function. Would be used to
 *            log to console.log or equivalent.
 *
 * @throws {OpenAjax.hub.Error.BadParameters};
 *             if required params are not present or null
 * @throws {OpenAjax.hub.Error.Duplicate};
 *             if a Container with this clientID already exists in the given
 *             Managed Hub
 * @throws {OpenAjax.hub.Error.Disconnected};
 *             if ManagedHub is not connected
 */
OpenAjax.hub.Container = function(hub, clientID, params) {
};

/**
 * onConnect callback for Container Invoked within the scope defined by
 * params.Container.scope, when the specified Container's client connects to the
 * ManagedHub. Supplied to Container constructor via the
 * params.Container.onConnect parameter
 *
 * @param {OpenAjax.hub.Container}
 *            container The Container that detected the connect
 */
OpenAjax.hub.ContainerCallbacks.onConnect = function(container) {
};

/**
 * onDisconnect callback for Container Invoked within the scope defined by
 * params.Container.scope, when the specified Container's client disconnects
 * from the ManagedHub. Supplied to Container constructor via the
 * params.Container.onDisconnect parameter
 *
 * @param {OpenAjax.hub.Container}
 *            container The Container that detected the disconnect
 */
OpenAjax.hub.ContainerCallbacks.onDisconnect = function(container) {
};
/**
 * Send a message to the client inside this container. This function MUST only
 * be called by ManagedHub.
 *
 * @param {String}
 *            topic The topic name for the published message
 * @param {*}
 *            data The payload. Can be any JSON-serializable value.
 * @param {String}
 *            containerSubscriptionId Container's ID for a subscription, from
 *            previous call to subscribeForClient()
 */
OpenAjax.hub.Container.prototype.sendToClient = function(topic, data,
        containerSubscriptionId) {
};
/**
 * Shut down a container. remove does all of the following: - disconnects
 * container from HubClient - unsubscribes from all of its existing
 * subscriptions in the ManagedHub
 *
 * This function is only called by ManagedHub.removeContainer Calling this
 * function does NOT cause the container's onDisconnect callback to be invoked.
 */
OpenAjax.hub.Container.prototype.remove = function() {
};
/**
 * Returns true if the given client is connected to the managed hub. Else
 * returns false.
 *
 * @returns {Boolean} true if the client is connected to the managed hub
 */
OpenAjax.hub.Container.prototype.isConnected = function() {
};
/**
 * Returns the clientID passed in when this Container was instantiated.
 *
 * @returns {String} The clientID
 *
 */
OpenAjax.hub.Container.prototype.getClientID = function() {
};
/**
 * If DISCONNECTED: Returns null If CONNECTED: Returns the origin associated
 * with the window containing the HubClient associated with this Container
 * instance. The origin has the format
 *
 * [protocol]://[host]
 *
 * where:
 *
 * [protocol] is "http" or "https" [host] is the hostname of the partner page.
 *
 * @returns {String} Partner's origin
 *
 */
OpenAjax.hub.Container.prototype.getPartnerOrigin = function() {
};
/**
 * Returns the params object associated with this Container instance.
 *
 * @returns {Object} params The params object associated with this Container
 *          instance
 *
 */
OpenAjax.hub.Container.prototype.getParameters = function() {
};
/**
 * Returns the ManagedHub to which this Container belongs.
 *
 * @return {OpenAjax.hub.ManagedHub} ManagedHub The ManagedHub object associated
 *         with this Container instance
 */
OpenAjax.hub.Container.prototype.getHub = function() {
};
/**
 * Create a new Iframe Container.
 *
 * @constructor
 *
 * IframeContainer implements the Container interface to provide a container
 * that isolates client components into secure sandboxes by leveraging the
 * isolation features provided by browser iframes.
 *
 * @param {OpenAjax.hub.ManagedHub}
 *            hub Managed Hub instance to which this Container belongs
 * @param {String}
 *            clientID A string ID that identifies a particular client of a
 *            Managed Hub. Unique within the context of the ManagedHub.
 * @param {Object}
 *            params Parameters used to instantiate the IframeContainer. Once
 *            the constructor is called, the params object belongs exclusively
 *            to the IframeContainer. The caller MUST not modify it. The
 *            following are the pre-defined properties on params:
 * @param {Function}
 *            params.Container.onSecurityAlert Called when an attempted security
 *            breach is thwarted. Function is defined as follows:
 *            function(container, securityAlert)
 * @param {Function}
 *            [params.Container.onConnect] Called when the client connects to
 *            the Managed Hub. Function is defined as follows:
 *            function(container)
 * @param {Function}
 *            [params.Container.onDisconnect] Called when the client disconnects
 *            from the Managed Hub. Function is defined as follows:
 *            function(container)
 * @param {Object}
 *            [params.Container.scope] Whenever one of the Container's callback
 *            functions is called, references to "this" in the callback will
 *            refer to the scope object. If no scope is provided, default is
 *            window.
 * @param {Function}
 *            [params.Container.log] Optional logger function. Would be used to
 *            log to console.log or equivalent.
 * @param {Object}
 *            params.IframeContainer.parent DOM element that is to be parent of
 *            iframe
 * @param {String}
 *            params.IframeContainer.uri Initial Iframe URI (Container will add
 *            parameters to this URI)
 * @param {String}
 *            params.IframeContainer.tunnelURI URI of the tunnel iframe. Must be
 *            from the same origin as the page which instantiates the
 *            IframeContainer.
 * @param {Object}
 *            [params.IframeContainer.iframeAttrs] Attributes to add to IFRAME
 *            DOM entity. For example: { style: { width: "100%", height: "100%"
 *            };, className: "some_class" };
 * @param {Number}
 *            [params.IframeContainer.timeout] Load timeout in milliseconds. If
 *            not specified, defaults to 15000. If the client at
 *            params.IframeContainer.uri does not establish a connection with
 *            this container in the given time, the onSecurityAlert callback is
 *            called with a LoadTimeout error code.
 * @param {Function}
 *            [params.IframeContainer.seed] A function that returns a string
 *            that will be used to seed the pseudo-random number generator,
 *            which is used to create the security tokens. An implementation of
 *            IframeContainer may choose to ignore this value.
 * @param {Number}
 *            [params.IframeContainer.tokenLength] Length of the security tokens
 *            used when transmitting messages. If not specified, defaults to 6.
 *            An implementation of IframeContainer may choose to ignore this
 *            value.
 *
 * @throws {OpenAjax.hub.Error.BadParameters};
 *             if required params are not present or null
 * @throws {OpenAjax.hub.Error.Duplicate};
 *             if a Container with this clientID already exists in the given
 *             Managed Hub
 * @throws {OpenAjax.hub.Error.Disconnected};
 *             if hub is not connected
 */
OpenAjax.hub.IframeContainer = function(hub, clientID, params) {
};

OpenAjax.hub.IframeContainer.prototype = new OpenAjax.hub.Container();
/**
 * Get the iframe associated with this iframe container
 *
 * This function returns the iframe associated with an IframeContainer, allowing
 * the Manager Application to change its size, styles, scrollbars, etc.
 *
 * CAUTION: The iframe is owned exclusively by the IframeContainer. The Manager
 * Application MUST NOT destroy the iframe directly. Also, if the iframe is
 * hidden and disconnected, the Manager Application SHOULD NOT attempt to make
 * it visible. The Container SHOULD automatically hide the iframe when it is
 * disconnected; to make it visible would introduce security risks.
 *
 * @returns {HTMLIFrameElement} iframeElement
 *
 */
OpenAjax.hub.IframeContainer.prototype.getIframe = function() {
};
/**
 * Create a new Inline Container.
 * <p>
 * OpenAjax.hub.InlineContainer implements the OpenAjax.hub.Container interface
 * to provide a container that places components inline within the same browser
 * frame as the main mashup application. As such, this container does not
 * isolates client components into secure sandboxes.
 *
 * @constructor
 *
 * InlineContainer implements the Container interface to provide a container
 * that places components within the same browser frame as the main mashup
 * application. As such, this container does not isolate client components into
 * secure sandboxes.
 *
 * @param {OpenAjax.hub.ManagedHub}
 *            hub Managed Hub instance to which this Container belongs
 * @param {String}
 *            clientID A string ID that identifies a particular client of a
 *            Managed Hub. Unique within the context of the ManagedHub.
 * @param {Object}
 *            params Parameters used to instantiate the InlineContainer. Once
 *            the constructor is called, the params object belongs exclusively
 *            to the InlineContainer. The caller MUST not modify it. The
 *            following are the pre-defined properties on params:
 * @param {Function}
 *            params.Container.onSecurityAlert Called when an attempted security
 *            breach is thwarted. Function is defined as follows:
 *            function(container, securityAlert)
 * @param {Function}
 *            [params.Container.onConnect] Called when the client connects to
 *            the Managed Hub. Function is defined as follows:
 *            function(container)
 * @param {Function}
 *            [params.Container.onDisconnect] Called when the client disconnects
 *            from the Managed Hub. Function is defined as follows:
 *            function(container)
 * @param {Object}
 *            [params.Container.scope] Whenever one of the Container's callback
 *            functions is called, references to "this" in the callback will
 *            refer to the scope object. If no scope is provided, default is
 *            window.
 * @param {Function}
 *            [params.Container.log] Optional logger function. Would be used to
 *            log to console.log or equivalent.
 *
 * @throws {OpenAjax.hub.Error.BadParameters};
 *             if required params are not present or null
 * @throws {OpenAjax.hub.Error.Duplicate};
 *             if a Container with this clientID already exists in the given
 *             Managed Hub
 * @throws {OpenAjax.hub.Error.Disconnected};
 *             if ManagedHub is not connected
 */
OpenAjax.hub.InlineContainer = function(hub, clientID, params) {
};

OpenAjax.hub.InlineContainer.prototype = new OpenAjax.hub.Container();
/**
 * Create a new HubClient. All HubClient constructors MUST have this signature.
 * <p>
 * Interface OpenAjax.hub.HubClient extends OpenAjax.hub.Hub and provides
 * client-side APIs (i.e., component-side APIs) to interact with the Managed
 * Hub. Each HubClient implementation is specific to a particular implementation
 * of OpenAjax.hub.Container.
 *
 * @constructor
 *
 * @param {Object}
 *            params Parameters used to instantiate the HubClient. Once the
 *            constructor is called, the params object belongs to the HubClient.
 *            The caller MUST not modify it. Implementations of HubClient may
 *            specify additional properties for the params object, besides those
 *            identified below.
 *
 * @param {Function}
 *            params.HubClient.onSecurityAlert Called when an attempted security
 *            breach is thwarted
 * @param {Object}
 *            [params.HubClient.scope] Whenever one of the HubClient's callback
 *            functions is called, references to "this" in the callback will
 *            refer to the scope object. If not provided, the default is window.
 * @param {Function}
 *            [params.HubClient.log] Optional logger function. Would be used to
 *            log to console.log or equivalent.
 *
 * @throws {OpenAjax.hub.Error.BadParameters};
 *             if any of the required parameters is missing, or if a parameter
 *             value is invalid in some way.
 */
OpenAjax.hub.HubClient = function(params) {
};

OpenAjax.hub.HubClient.prototype = new OpenAjax.hub.Hub();
/**
 * Requests a connection to the ManagedHub, via the Container associated with
 * this HubClient.
 *
 * If the Container accepts the connection request, the HubClient's state is set
 * to CONNECTED and the HubClient invokes the onComplete callback function.
 *
 * If the Container refuses the connection request, the HubClient invokes the
 * onComplete callback function with an error code. The error code might, for
 * example, indicate that the Container is being destroyed.
 *
 * In most implementations, this function operates asynchronously, so the
 * onComplete callback function is the only reliable way to determine when this
 * function completes and whether it has succeeded or failed.
 *
 * A client application may call HubClient.disconnect and then call
 * HubClient.connect.
 *
 * @param {OpenAjax.hub~onComplete}
 *            [onComplete] Callback function to call when this operation
 *            completes.
 * @param {Object}
 *            [scope] When the onComplete function is invoked, the JavaScript
 *            "this" keyword refers to this scope object. If no scope is
 *            provided, default is window.
 *
 * @throws {OpenAjax.hub.Error.Duplicate};
 *             if the HubClient is already connected
 */
OpenAjax.hub.HubClient.prototype.connect = function(onComplete, scope) {
};
/**
 * Disconnect from the ManagedHub
 *
 * Disconnect immediately:
 *
 * 1. Sets the HubClient's state to DISCONNECTED. 2. Causes the HubClient to
 * send a Disconnect request to the associated Container. 3. Ensures that the
 * client application will receive no more onData or onComplete callbacks
 * associated with this connection, except for the disconnect function's own
 * onComplete callback. 4. Automatically destroys all of the HubClient's
 * subscriptions.
 *
 * In most implementations, this function operates asynchronously, so the
 * onComplete callback function is the only reliable way to determine when this
 * function completes and whether it has succeeded or failed.
 *
 * A client application is allowed to call HubClient.disconnect and then call
 * HubClient.connect.
 *
 * @param {Function}
 *            [onComplete] Callback function to call when this operation
 *            completes.
 * @param {Object}
 *            [scope] When the onComplete function is invoked, the JavaScript
 *            "this" keyword refers to the scope object. If no scope is
 *            provided, default is window.
 *
 * @throws {OpenAjax.hub.Error.Disconnected};
 *             if the HubClient is already disconnected
 */
OpenAjax.hub.HubClient.prototype.disconnect = function(onComplete, scope) {
};
/**
 * If DISCONNECTED: Returns null If CONNECTED: Returns the origin associated
 * with the window containing the Container associated with this HubClient
 * instance. The origin has the format
 *
 * [protocol]://[host]
 *
 * where:
 *
 * [protocol] is "http" or "https" [host] is the hostname of the partner page.
 *
 * @returns {String} Partner's origin
 *
 */
OpenAjax.hub.HubClient.prototype.getPartnerOrigin = function() {
};
/**
 * Returns the client ID of this HubClient
 *
 * @returns {String} clientID
 *
 */
OpenAjax.hub.HubClient.prototype.getClientID = function() {
};
/**
 * Create a new IframeHubClient.
 *
 * <p>
 * Class OpenAjax.hub.IframeHubClient implements OpenAjax.hub.HubClient for an
 * IframeContainer and provides client-side APIs (i.e., component-side APIs) to
 * interact with the Managed Hub..
 *
 * @constructor
 *
 * @param {Object}
 *            params Once the constructor is called, the params object belongs
 *            to the HubClient. The caller MUST not modify it. The following are
 *            the pre-defined properties on params:
 * @param {Function}
 *            params.HubClient.onSecurityAlert Called when an attempted security
 *            breach is thwarted
 * @param {Object}
 *            [params.HubClient.scope] Whenever one of the HubClient's callback
 *            functions is called, references to "this" in the callback will
 *            refer to the scope object. If not provided, the default is window.
 * @param {Function}
 *            [params.HubClient.log] Optional logger function. Would be used to
 *            log to console.log or equivalent.
 * @param {Function}
 *            [params.IframeHubClient.seed] A function that returns a string
 *            that will be used to seed the pseudo-random number generator,
 *            which is used to create the security tokens. An implementation of
 *            IframeHubClient may choose to ignore this value.
 * @param {Number}
 *            [params.IframeHubClient.tokenLength] Length of the security tokens
 *            used when transmitting messages. If not specified, defaults to 6.
 *            An implementation of IframeHubClient may choose to ignore this
 *            value.
 *
 * @throws {OpenAjax.hub.Error.BadParameters};
 *             if any of the required parameters is missing, or if a parameter
 *             value is invalid in some way.
 */
OpenAjax.hub.IframeHubClient = function(params) {
};
OpenAjax.hub.IframeHubClient.prototype = new OpenAjax.hub.HubClient()

/**
 * Create a new InlineHubClient.
 * <p>
 * Class OpenAjax.hub.InlineHubClient implements OpenAjax.hub.HubClient for an
 * InlineContainer and provides client-side APIs (i.e., component-side APIs) to
 * interact with the Managed Hub.
 * <p>
 * The following functions of InlineHubClient typically are SYNCHRONOUS:
 * <ul>
 * <li>connect
 * <li>disconnect
 * <li>subscribe
 * <li>unsubscribe
 * <p>
 * This means that for all of these functions, the associated onComplete
 * callbacks - and any other callbacks, e.g. Container.onSubscribe - typically
 * are invoked BEFORE these functions return. Developers are cautioned that in
 * most implementations of HubClient, onComplete is invoked after this function
 * returns.
 * <p>
 * In addition, InlineHubClient.publish typically is SYNCHRONOUS. This means
 * that the Container's onPublish and the publish function's onData callbacks
 * typically are invoked before the publish call returns. Developers are
 * cautioned that in most implementations of HubClient, onData is invoked after
 * this function returns.
 *
 * @constructor
 *
 * @param {Object}
 *            params Parameters used to instantiate the HubClient. Once the
 *            constructor is called, the params object belongs to the HubClient.
 *            The caller MUST not modify it. The following are the pre-defined
 *            properties on params:
 * @param {Function}
 *            params.HubClient.onSecurityAlert Called when an attempted security
 *            breach is thwarted
 * @param {Object}
 *            [params.HubClient.scope] Whenever one of the HubClient's callback
 *            functions is called, references to "this" in the callback will
 *            refer to the scope object. If not provided, the default is window.
 * @param {Function}
 *            [params.HubClient.log] Optional logger function. Would be used to
 *            log to console.log or equivalent.
 * @param {OpenAjax.hub.InlineContainer}
 *            params.InlineHubClient.container Specifies the InlineContainer to
 *            which this HubClient will connect
 *
 * @throws {OpenAjax.hub.Error.BadParameters};
 *             if any of the required parameters are missing
 */
OpenAjax.hub.InlineHubClient = function(params) {
};

OpenAjax.hub.InlineHubClient.prototype = new OpenAjax.hub.Hub();