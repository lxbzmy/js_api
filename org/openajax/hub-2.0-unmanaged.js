/**
 * @class
 */
OpenAjax = {};

OpenAjax.hub = {};

/**
 * Creates a subscription on the specified topic name.
 * <p>
 * This function returns an Object (a "subscription") that is unique for this
 * particular subscription. To unsubscribe, return this Object to
 * <code>OpenAjax.hub.unsubscribe(...)</code>. This subscription object MUST
 * be treated as an opaque, read-only value by applications and libraries that
 * are using the Hub. Thus, the object MUST NOT be modified or deleted.
 * </p>
 * <p>
 * Topic names are strings composed of sequences of tokens separated by the "."
 * character (period character - 0x2E). They MAY include wildcard characters.
 * For detailed rules and guidelines on topic names, see <a
 * href="http://www.openajax.org/member/wiki/OpenAjax_Hub_2.0_Specification_Topic_Names">Topic
 * Names chapter</a>.
 * </p>
 * <p>
 * The callback function will receive the following parameters (see
 * <tt>OpenAjax.hub.publish()</tt> for description of <tt>publisherData</tt>):
 * </p>
 * <p>
 * <tt>&nbsp;&nbsp;function(name, publisherData, subscriberData){ ... };</tt>
 * </p>
 * <p>
 * The callback function does not return a value. The callback function SHOULD
 * NOT throw exceptions.
 * </p>
 * <h4> <span class="mw-headline"> Hub 1.0 feature no longer available in Hub
 * 2.0 </span></h4>
 * <p>
 * Hub 1.0 included a 5th parameter, 'filter', to
 * <code>OpenAjax.hub.subscribe</code>. This parameter has been removed from
 * the OpenAjax Hub with version 2.0. Below is the description of the
 * (now-removed) 'filter' parameter.
 * </p>
 * <dl>
 * <dd>
 * <dl>
 * <dt> filter </dt>
 * <dd> Optional, can be null. A function that returns true or false to either
 * match or deny a match of the published event. The filter function allows for
 * the definition of an intermediate function that is invoked before the
 * callback function. Filter functions return a boolean. If the filter function
 * returns false, then the given callback function is not invoked. The filter
 * callback function takes the same parameters as the data callback function.
 * </dd>
 * </dl>
 * </dd>
 * </dl>
 * <p>
 * (Note that although this convenience feature have been removed from the
 * OpenAjax Hub 2.0 Specification, it continues to be supported within the open
 * source reference implementation provided by OpenAjax Alliance within a
 * special "Unmanaged Hub" release configuration, which is fully compatible with
 * the Hub 1.0 Specification.)
 * </p>
 *
 * @link http://www.openajax.org/member/wiki/OpenAjax_Hub_2.0_Specification_Unmanaged_Hub
 * @param {String};
 *            name The name of the topic to which you want to subscribe.
 * @param {Function|String};
 *            refOrName A function object reference or the name of a function
 *            that is invoked whenever an event is published on the topic.
 * @param {Object};
 *            [scope] An Object in which to execute refOrName when handling the
 *            event. If null, window object is used. The scope parameter will be
 *            the "this" object when the callback is invoked.
 * @param {*};
 *            [subscriberData] Client application provides this data, which is
 *            handed back to the client application in the subscriberData
 *            parameter of the callback function.
 * @returns A String Object (a "subscription") that is unique for this
 *          particular subscription.
 * @type {String};
 */
OpenAjax.hub.subscribe = function(name, refOrName, scope, subscriberData) {
};

/**
 * Removes a subscription to an event.
 *
 * @param {String};
 *            subscription The return value from a previous call to
 *            OpenAjax.hub.subscribe().
 */
OpenAjax.hub.unsubscribe = function(subscription) {
};

/**
 * Publishes (broadcasts) an event.
 * <p>
 * The <code>name</code> parameter MUST NOT contain wildcard tokens. For
 * detailed rules and guidelines on topic names, see <a
 * href="http://www.openajax.org/member/wiki/OpenAjax_Hub_2.0_Specification_Topic_Names">Topic
 * Names chapter</a>.
 * </p>
 *
 * @param {String};
 *            name The name of the topic that is being published.
 * @param {*};
 *            [publisherData] (Optional) An arbitrary Object holding extra
 *            information that will be passed as an argument to the handler
 *            function.
 * @type {Object};
 */
OpenAjax.hub.publish = function(name, publisherData) {
};
