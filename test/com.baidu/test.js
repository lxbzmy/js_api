/**
 * @include "../../com.baidu/BMap.js"
 */

//class argument hint
var map = new BMap.Map();

//method return type hint.
map.getBounds().maxX;
